export const womensFashionProductData = [
  {
    productDescription:
      "Women's Woven Silk Saree With Blouse Piece (4504_Color)",
    asin: "B09QX2Z55Y",
    countReview: 91,
    imgUrl: "https://m.media-amazon.com/images/I/71Xg1kk5+sL._AC_UL320_.jpg",
    price: 1745,
    retailPrice: 5999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B09QX2Z55Y/ref=sr_1_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Rani Pink",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B09QX2Z55Y/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Green",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1W7GQ1/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Magenta",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1VP998/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BFXKQ1WH/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1V82CY/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0B8ST3FGP/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Beach Upgrades Analog White Dial Women's Watch-NL9827PP07/NP9827PP07",
    asin: "B007R9WYKG",
    countReview: 1681,
    imgUrl: "https://m.media-amazon.com/images/I/61Gv2lvb17L._AC_UL320_.jpg",
    price: 1795,
    retailPrice: 2195,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Fastrack-Upgrades-Analog-Womens-Watch-NK9827PP07/dp/B007R9WYKG/ref=sr_1_omk_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Stylish Self-Design blouson fur Cotton Twill Jacket",
    asin: "B08RCHJ925",
    countReview: 48,
    imgUrl: "https://m.media-amazon.com/images/I/61UZTW7c4tL._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 1499,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08RCHJ925/ref=sr_1_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08RCHJ925/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Army Green 1",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B09RV6Q5YM/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cool Blue",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08RCJB4V5/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Pink",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08T7C7BD7/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08RCGRCVX/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/FUNDAY-FASHION-Stylish-Self-Design-blouson/dp/B08RCHTWQ3/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Lycre Tie-Dye Printed 3/4 Sleeve Top and Pajama Pants Regular Fit Night Suit Notched CollarLes Top and Pyjama Set Ladies Night Dress",
    asin: "B0B3VW2X34",
    countReview: 69,
    imgUrl: "https://m.media-amazon.com/images/I/41Y+CGvDpbL._AC_UL320_.jpg",
    price: 639.0,
    retailPrice: 1999,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Leriya-Fashion-Tie-Dye-Printed-CollarLes/dp/B0B3VW2X34/ref=sr_1_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Women Deva Fashion Sandals",
    asin: "B07LD9PKRG",
    countReview: 3249,
    imgUrl: "https://m.media-amazon.com/images/I/71BJUuO2ANL._AC_UL320_.jpg",
    price: 922.0,
    retailPrice: 999.0,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/BATA-Womens-Fashion-Sandals-5-6618912/dp/B07LD9PKRG/ref=sr_1_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Beige",
        dpUrl:
          "/BATA-Womens-Fashion-Sandals-5-6618912/dp/B07LD9PKRG/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/BATA-Womens-Fashion-Sandals-5-6618912/dp/B07X5J8X5C/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/BATA-Womens-Fashion-Sandals-5-6618912/dp/B07LDKQ9G3/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Latest pairs Earrings Combo Set Crystal Pearl Stud Earrings for Women",
    asin: "B084HHN8LD",
    countReview: 4681,
    imgUrl: "https://m.media-amazon.com/images/I/61xmb0cOh9L._AC_UL320_.jpg",
    price: 240.0,
    retailPrice: 1999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Earrings-cmb291_10868_10869/dp/B084HHN8LD/ref=sr_1_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "AAA Crystal 18k Rose Gold Stylish Jewellery for Women and Girls",
    asin: "B0779QYMVP",
    countReview: 1909,
    imgUrl: "https://m.media-amazon.com/images/I/71pVPpqzqwL._AC_UL320_.jpg",
    price: 210.0,
    retailPrice: 2999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Jewellery-9556b/dp/B0779QYMVP/ref=sr_1_7?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Long Chain Black Crystal Rose Flower Pendant Necklace for Women and Girls",
    asin: "B0768M3XRW",
    countReview: 2089,
    imgUrl: "https://m.media-amazon.com/images/I/612E8Vgbd9L._AC_UL320_.jpg",
    price: 250.0,
    retailPrice: 2499,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Jewellery-9279np/dp/B0768M3XRW/ref=sr_1_8?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Fashion Sandal, Faux Leather, Comfortable and Stylish Wedges| Soft Cushion Footbed For Women & Girls | For Casual Wear & Formal Wear Occasions 2 inches heel",
    asin: "B07MVHGJBL",
    countReview: 3497,
    imgUrl: "https://m.media-amazon.com/images/I/61fa2POB8yL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B07MVHGJBL/ref=sr_1_9?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Cream",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B07MVHGJBL/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B0828NR1TT/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B0828PB756/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B07MVLDYY2/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cherry",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B0828PMFHG/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Copper",
        dpUrl:
          "/TRASE-Avalon-Cream-Sandals-Women-5/dp/B082Z17CKT/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women Organza semi-stitched Lehenga choli (L8097_Green_Green_Free Size)",
    asin: "B09ZXL59X7",
    countReview: 39,
    imgUrl: "https://m.media-amazon.com/images/I/61xBBgL5QAL._AC_UL320_.jpg",
    price: 614.0,
    retailPrice: 2999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Basket-Organza-semi-stitched-L8097_Green_Green_Free/dp/B09ZXL59X7/ref=sr_1_10?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Women's Organza Printed & Jari Sequence Work Semi-Stitched Lehenga Choli",
    asin: "B09GXYHPDB",
    countReview: 467,
    imgUrl: "https://m.media-amazon.com/images/I/914yjFc6ZLL._AC_UL320_.jpg",
    price: 614.0,
    retailPrice: 2999,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Basket-Sequence-Semi-Stitched-L8097_Turquoise_Free/dp/B09GXYHPDB/ref=sr_1_11?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Latest Stylish Austrian Crystal Rose Gold Charm Bracelet for Women",
    asin: "B07RY8K8X9",
    countReview: 1844,
    imgUrl: "https://m.media-amazon.com/images/I/51rmMr2nSiL._AC_UL320_.jpg",
    price: 255.0,
    retailPrice: 1499,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Infinity-10672b/dp/B07RY8K8X9/ref=sr_1_12?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Fashion Sandals | Light weight, Comfortable & Trendy Flatform Sandals for Girls | Soft Footbed | Casual and Stylish Floaters for Walking, Working, All Day Wear",
    asin: "B0966CZZNS",
    countReview: 2934,
    imgUrl: "https://m.media-amazon.com/images/I/61Dxa4I8qmS._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1699,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B0966CZZNS/ref=sr_1_13?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black-Sea Green",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B0966CZZNS/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue-Pink",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B097K3L8TG/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White-Sea Green",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B098JXQH9J/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Fashion Jewellery Gold Plated Ear rings Combo of Earrings for Girls and Women",
    asin: "B0B5521TL3",
    countReview: 21,
    imgUrl: "https://m.media-amazon.com/images/I/61qGsfR+i1L._AC_UL320_.jpg",
    price: 327.0,
    retailPrice: 1599,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/YouBella-Fashion-Jewellery-Plated-Earrings/dp/B0B5521TL3/ref=sr_1_14?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [],
  },
  {
    productDescription:
      "Dresses for Women || Dresses for Women Dress || Dress for Women || Western Dresses for Women || Women Dress || Dresses (J-514-516)",
    asin: "B0BFR2L1TH",
    countReview: 12,
    imgUrl: "https://m.media-amazon.com/images/I/7178zfQwb5L._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 2999,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Dresses-Western-J-514-516-Maroon/dp/B0BFR2L1TH/ref=sr_1_15?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Maroon",
        dpUrl:
          "/Fashion-Dresses-Western-J-514-516-Maroon/dp/B0BFR2L1TH/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Fashion-Dresses-Western-J-514-516-Maroon/dp/B0BFR2V48V/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/Fashion-Dresses-Western-J-514-516-Maroon/dp/B0BFR25151/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Stylish Chiffon Floral Printed Saree with Blouse Piece",
    asin: "B0B6G8RFQ9",
    countReview: 3,
    imgUrl: "https://m.media-amazon.com/images/I/81E1ureT9DL._AC_UL320_.jpg",
    price: 495.0,
    retailPrice: 2799,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/MIRCHI-FASHION-Chiffon-Printed-36581-Khakhi/dp/B0B6G8RFQ9/ref=sr_1_16?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-16",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Khakhi, Pink, Yellow",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Printed-36581-Khakhi/dp/B0B6G8RFQ9/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Olive Green, Orange, Pink",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Printed-36581-Khakhi/dp/B0B6G7RHF3/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Dresses for Women || Dresses for Women Dress || Dress for Women || Western Dresses for Women || Women Dress || Dresses (J-520-521)",
    asin: "B0BFQZQKTW",
    countReview: 11,
    imgUrl: "https://m.media-amazon.com/images/I/61XdzIyV6hL._AC_UL320_.jpg",
    price: 589.0,
    retailPrice: 2999,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Fashion-Dresses-Women-Western-J-520-521/dp/B0BFQZQKTW/ref=sr_1_17?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-17",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Light Pink",
        dpUrl:
          "/Fashion-Dresses-Women-Western-J-520-521/dp/B0BFQZQKTW/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-17",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Fashion-Dresses-Women-Western-J-520-521/dp/B0BFR338ZW/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-17",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Latest Stylish Traditional Tibetan Pendant Necklace Jewellery Set for Women (13208s)",
    asin: "B0B15GYNS4",
    countReview: 114,
    imgUrl: "https://m.media-amazon.com/images/I/617BH0hUF1L._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Traditional-13208s/dp/B0B15GYNS4/ref=sr_1_18?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-18",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Latest Stylish 6-20 Pairs Combo Earrings for Women and Girls",
    asin: "B07JVVXFF4",
    countReview: 2222,
    imgUrl: "https://m.media-amazon.com/images/I/71WdysB5ozL._AC_UL320_.jpg",
    price: 187.0,
    retailPrice: 1999,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Earrings-9919er/dp/B07JVVXFF4/ref=sr_1_19?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-19",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women Embroidered Kurta Pant & Dupatta Set Wine",
    asin: "B09TFMHL9V",
    countReview: 59,
    imgUrl: "https://m.media-amazon.com/images/I/61uxssAELoL._AC_UL320_.jpg",
    price: 999.0,
    retailPrice: 2999,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Ziva-Fashion-Women-Embroidered-Dupatta/dp/B09TFMHL9V/ref=sr_1_20?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-20",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Western Dresses for Women | Short A-Line Dress for Girls | Maxi Dress for Women",
    asin: "B0BDVRRJ3H",
    countReview: 32,
    imgUrl: "https://m.media-amazon.com/images/I/713EWcF36KL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Western-Dresses-Multicolor/dp/B0BDVRRJ3H/ref=sr_1_21?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-21",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "18k Rose Gold Stylish Bracelet for Women and Girls",
    asin: "B07NQ1NMWF",
    countReview: 938,
    imgUrl: "https://m.media-amazon.com/images/I/51rQL1Fft+L._AC_UL320_.jpg",
    price: 260.0,
    retailPrice: 1999,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Bracelet-Girls/dp/B07NQ1NMWF/ref=sr_1_22?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-22",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "Western Dresses for Women |A-Line Knee-Length Dress | Indo Western Dress for Women| Short Dress",
    asin: "B0B86DQC7Z",
    countReview: 33,
    imgUrl: "https://m.media-amazon.com/images/I/61gFT+Fdv-L._AC_UL320_.jpg",
    price: 479.0,
    retailPrice: 1999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Western-Dresses-Knee-Length/dp/B0B86DQC7Z/ref=sr_1_23?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-23",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "(34 STRIPES) Women Fashion tape, Clothing & Body, Strong and Clear Tape for All Skin Tones and Fabric, Waterproof & Sweatproof, Bra Tape Strips.",
    asin: "B0B2RK43PL",
    countReview: 232,
    imgUrl: "https://m.media-amazon.com/images/I/51ENOz1PSFL._AC_UL320_.jpg",
    price: 155.0,
    retailPrice: 249.0,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Mincerfit-STRIPES-Clothing-Waterproof-Sweatproof/dp/B0B2RK43PL/ref=sr_1_24?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-24",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Women's Full Sleeve Top Round Neck (Empire)",
    asin: "B09LVLXNV9",
    countReview: 774,
    imgUrl: "https://m.media-amazon.com/images/I/51acPVudkhL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B09LVLXNV9/ref=sr_1_25?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Empire Black",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B09LVLXNV9/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cheetah Beige",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B0BGC1K7T7/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cheetah Black",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B0BGC1WHFT/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Empire Green",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B09M3P88SH/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Empire Maroon",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B09LVKPD3M/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Empire Peach",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeve-Empire/dp/B09LYY6PPD/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Rama Kurti Plazzo Set & Black Kurti Plazzo Set Combo Pack of 2 (Rama & Black)-P",
    asin: "B0927BBGV9",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/71V2Apo1E6S._AC_UL320_.jpg",
    price: 949.0,
    retailPrice: 4999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Fashion-SAY-Printed-Kurti-Rama-Top-M_Rama/dp/B0927BBGV9/ref=sr_1_26?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-26",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Women's Silk Embroidery Border Saree with Heavy Work Blouse Unstitched Piece",
    asin: "B09G9191P3",
    countReview: 1096,
    imgUrl: "https://m.media-amazon.com/images/I/511qxsw-WZL._AC_UL320_.jpg",
    price: 529.0,
    retailPrice: 1299,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9191P3/ref=sr_1_27?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "maroon",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9191P3/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "black",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9BMPKY/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "green",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9ZD3GM/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "nevyblue",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G99DYWN/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "purple",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9D695Z/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "rama",
        dpUrl:
          "/Nplash-Fashion-Womens-Embroidery-Unstitched/dp/B09G9F7ZP7/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Lycra & Spandex Lightly Padded Non-Wired Bralette Bra(GBRA009_Size :28 to 34)",
    asin: "B08JJ43YZQ",
    countReview: 1970,
    imgUrl: "https://m.media-amazon.com/images/I/717v7PyeRnL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 699.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/DEALSEVEN-FASHION-Non-Wired-Bralette-GBRA009_Pink_28/dp/B08JJ43YZQ/ref=sr_1_28?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-28",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "LIGHT PINK",
        dpUrl:
          "/DEALSEVEN-FASHION-Non-Wired-Bralette-GBRA009_Pink_28/dp/B08JJ43YZQ/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK",
        dpUrl:
          "/DEALSEVEN-FASHION-Non-Wired-Bralette-GBRA009_Pink_28/dp/B08JJ2G94F/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "GREY",
        dpUrl:
          "/DEALSEVEN-FASHION-Non-Wired-Bralette-GBRA009_Pink_28/dp/B08JJ3CRV9/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Cotton Blend Anarkali Solid Kurta with Dupatta (Aadhya)",
    asin: "B09XTLMFHY",
    countReview: 749,
    imgUrl: "https://m.media-amazon.com/images/I/51xd0lNT9cL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 2599,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/ANNI-DESIGNER-Anarkali-Dupatta-Aadhya-Green_M_Green_Medium/dp/B09XTLMFHY/ref=sr_1_29?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-29",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Green",
        dpUrl:
          "/ANNI-DESIGNER-Anarkali-Dupatta-Aadhya-Green_M_Green_Medium/dp/B09XTLMFHY/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/ANNI-DESIGNER-Anarkali-Dupatta-Aadhya-Green_M_Green_Medium/dp/B0B9T191NM/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Wine",
        dpUrl:
          "/ANNI-DESIGNER-Anarkali-Dupatta-Aadhya-Green_M_Green_Medium/dp/B0B9SZ5F1T/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/ANNI-DESIGNER-Anarkali-Dupatta-Aadhya-Green_M_Green_Medium/dp/B0B9SZLTMN/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Summer Slub Rayon Wrap Maxi Dress Casual Dote Printed High Neck Full Sleeve Ruffle Hem Split Beach Long Dresses",
    asin: "B09TPL7TWX",
    countReview: 252,
    imgUrl: "https://m.media-amazon.com/images/I/61vlEZn-+dL._AC_UL320_.jpg",
    price: 479.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Womens-Summer-Printed/dp/B09TPL7TWX/ref=sr_1_30?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-30",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Maroon",
        dpUrl:
          "/Leriya-Fashion-Womens-Summer-Printed/dp/B09TPL7TWX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Leriya-Fashion-Womens-Summer-Printed/dp/B09Z86XVBX/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Leriya-Fashion-Womens-Summer-Printed/dp/B09Z88Y318/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Watches Leather Strap Analogue Simple Style Ladies Fashion Watches Waterproof Watch for Wife Miss Women Birthday Gift",
    asin: "B09X1F32N1",
    countReview: 223,
    imgUrl: "https://m.media-amazon.com/images/I/71JYlQhi3aL._AC_UL320_.jpg",
    price: 759.0,
    retailPrice: 1500,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/NIBOSI-Watches-Analogue-Waterproof-Birthday/dp/B09X1F32N1/ref=sr_1_31?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-31",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Rectanglular Sunglasses for Women Retro Driving Sunlgasses Vintage Fashion Narrow Square Frame UV400 Protection",
    asin: "B08XP26HTW",
    countReview: 3514,
    imgUrl: "https://m.media-amazon.com/images/I/5193amnm+wL._AC_UL320_.jpg",
    price: 355.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/elegante-Rectanglular-Sunglasses-Sunlgasses-Protection/dp/B08XP26HTW/ref=sr_1_32?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-32",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Women's Rayon Printed Top",
    asin: "B09FTBYRGX",
    countReview: 887,
    imgUrl: "https://m.media-amazon.com/images/I/71dgp1fOBhL._AC_UL320_.jpg",
    price: 369.0,
    retailPrice: 1200,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=sr_1_33?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTC285Y/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightBlue",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G2C498/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightGreen",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G1164C/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTDBRKC/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Fashion Checker Pattern PU Leather Solid Color Shoulder Crossbody Messenger Bag Casual Ladies Small Flap Purse Handbags",
    asin: "B0B5LNTFG7",
    countReview: 21,
    imgUrl: "https://m.media-amazon.com/images/I/41A+KFmUV5L._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1099,
    productRating: "4.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LNTFG7/ref=sr_1_34?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-34",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Purple",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LNTFG7/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LQ9RG5/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LPMDR1/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LP1PCX/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women Soft Bottom Doctor Sole Slipper",
    asin: "B09RWMR2PK",
    countReview: 3839,
    imgUrl: "https://m.media-amazon.com/images/I/51iIPFL8GWL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Denill-Women-Bottom-Slipper-numeric_6/dp/B09RWMR2PK/ref=sr_1_35?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-35",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Peach",
        dpUrl:
          "/Denill-Women-Bottom-Slipper-numeric_6/dp/B09RWMR2PK/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Denill-Women-Bottom-Slipper-numeric_6/dp/B07G971WVF/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Denill-Women-Bottom-Slipper-numeric_6/dp/B09RWMGVT4/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Tan",
        dpUrl:
          "/Denill-Women-Bottom-Slipper-numeric_6/dp/B07G94JX3R/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Full Sleeve Top Round Neck Casual Tshirt (Empire4-23\" Inches)",
    asin: "B0B69NPCNK",
    countReview: 5,
    imgUrl: "https://m.media-amazon.com/images/I/51rGze59quL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 799.0,
    productRating: "3.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69NPCNK/ref=sr_1_36?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Garnet Maroon",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69NPCNK/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Royal Blue",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69J53SF/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Baby Pink",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69L24DW/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Frost White",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69K78Q6/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hunter Green",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69HXS7C/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Matt Black",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69F8YHB/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Fashions Women's Brooches & Pins for Hijab Dupatta Chunri Scrafs & Fancy Saree Pin for Women",
    asin: "B08RZ66J5F",
    countReview: 191,
    imgUrl: "https://m.media-amazon.com/images/I/711SnznhkoL._AC_UL320_.jpg",
    price: 248.0,
    retailPrice: 1000.0,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashions-Brooches-Dupatta-Saree-Pin/dp/B08RZ66J5F/ref=sr_1_37?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-37",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Tops for Women | Tops for Women Tops for Women Stylish | top for Girls | top for Women Stylish Latest (J-526-576)",
    asin: "B0BFR32ZNG",
    countReview: 19,
    imgUrl: "https://m.media-amazon.com/images/I/61Fxn7u8yGL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 2999,
    productRating: "4.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Women-Stylish-Latest-J-526-576/dp/B0BFR32ZNG/ref=sr_1_38?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Peach",
        dpUrl:
          "/Fashion-Women-Stylish-Latest-J-526-576/dp/B0BFR32ZNG/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Fashion-Women-Stylish-Latest-J-526-576/dp/B0BFQZ9Y3M/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Fashion-Women-Stylish-Latest-J-526-576/dp/B0BFR2N1T5/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/Fashion-Women-Stylish-Latest-J-526-576/dp/B0BFR2DHGX/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Cotton Straight Kurta with Pant",
    asin: "B08DD44B83",
    countReview: 38839,
    imgUrl: "https://m.media-amazon.com/images/I/51AXhiNnWqL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 2599,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/GoSriKi-Straight-Checkered-DISINI-Multi_XXL_Green_XX-Large/dp/B08DD44B83/ref=sr_1_39?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Women's Short Sleeves Casual Midi Bodycon Polyster Blend Dress (38\" Inches)",
    asin: "B08YMTY8P3",
    countReview: 298,
    imgUrl: "https://m.media-amazon.com/images/I/61iUYpH3YEL._AC_UL320_.jpg",
    price: 279.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YMTY8P3/ref=sr_1_40?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Royal Blue",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YMTY8P3/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Garnet Maroon",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YNCTY2T/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Matt Black",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YNM9KS7/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mint Green",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YNKW6G6/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YNF49H1/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Rose Peach",
        dpUrl:
          "/Dream-Beauty-Fashion-Sleeves-Bodycon/dp/B08YN53L48/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women And Girl Fashion Metal Stretchable Gold Plated Belly Chain Waist Belt Jewellery kamarband Waistband",
    asin: "B09DL7LPZ7",
    countReview: 165,
    imgUrl: "https://m.media-amazon.com/images/I/61gZbGVvhTL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 799.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7LPZ7/ref=sr_1_41?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Golden Ring",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7LPZ7/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Belt",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL6VW7L/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Leather",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7L54H/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Leaves",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL95FTL/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multi chain",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7DDZY/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Silver Ring",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL6HK62/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Rose Gold Cubic Zirconia Contemporary Rings Drop Brass Bali Earring For Women-ZPFK9472",
    asin: "B08GBZ1FJ6",
    countReview: 475,
    imgUrl: "https://m.media-amazon.com/images/I/619meuXxmxL._AC_UL320_.jpg",
    price: 159.0,
    retailPrice: 599.0,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Zirconia-Contemporary-Earring-Women-ZPFK9472/dp/B08GBZ1FJ6/ref=sr_1_42?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-42",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Latest Stylish Rose Gold Austrian Crystal Bracelet for Women and Girls (11942b), free",
    asin: "B091MK45DB",
    countReview: 634,
    imgUrl: "https://m.media-amazon.com/images/I/51JBNLtPvQS._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Austrian-11942b/dp/B091MK45DB/ref=sr_1_43?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-43",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women's Synthetic a-line Knee-Long Dress",
    asin: "B07NTC31XW",
    countReview: 3735,
    imgUrl: "https://m.media-amazon.com/images/I/61oViXf3vVL._AC_UL320_.jpg",
    price: 606.0,
    retailPrice: 2099,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Rare-Synthetic-a-line-Dress-ep3153-l_Blue_Large/dp/B07NTC31XW/ref=sr_1_44?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-44",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Stylish Fancy Golden Gold Plated Earrings for Women (9567er)",
    asin: "B07NQ1NWSY",
    countReview: 2276,
    imgUrl: "https://m.media-amazon.com/images/I/51UDJi2cwqS._AC_UL320_.jpg",
    price: 260.0,
    retailPrice: 1.999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Stylish-Earrings/dp/B07NQ1NWSY/ref=sr_1_45?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Cotton Print Unstitched Salwar Suit Material, 2 Mtr Printed Cotton Salwar, Stylish Chiffon Printed Dupatta (Design 81; Free Size)",
    asin: "B09QYVWSF5",
    countReview: 355,
    imgUrl: "https://m.media-amazon.com/images/I/716NsGJlycL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 2599,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Sun-Fashion-Lifestyle-Unstitched-Material/dp/B09QYVWSF5/ref=sr_1_46?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-46",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Double Sided Tape for Clothes Fashion Dressing Tape/Invisible Double-sided Body Tape, Clothing Bra Strip, Bikini Tape for Women, Body Clothing Stickers (36 Pack)",
    asin: "B0B4DJNQLX",
    countReview: 392,
    imgUrl: "https://m.media-amazon.com/images/I/51VYKdv8nkL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 249.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dressing-Invisible-Double-sided-Clothing-Stickers/dp/B0B4DJNQLX/ref=sr_1_47?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-47",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Paragon Women's Solea Comfort Trendy Pink-Blue Sandal",
    asin: "B09VKNBMFC",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/51oBHrotrdL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 370.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/PARAGON-Comfort-Trendy-Fashion-Pink-Blue/dp/B09VKNBMFC/ref=sr_1_48?keywords=women+fashion&qid=1668703194&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-48",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Women's Yoga Dress Pants Stretchy Work Slacks Business Casual Office Straight Leg/Bootcut Elastic Waist with 2 Pockets Regular Fit Trouser",
    asin: "B09SLGVFKS",
    countReview: 239,
    imgUrl: "https://m.media-amazon.com/images/I/51LPkXgiUsL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGVFKS/ref=sr_1_49?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGVFKS/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B0B42D4RND/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLJZ33S/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLDLBDZ/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLJGPCP/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGCVSP/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Speed X Fashion Women's Handbag",
    asin: "B07PLJXCKC",
    countReview: 5709,
    imgUrl: "https://m.media-amazon.com/images/I/81M0GZIfhmL._AC_UL320_.jpg",
    price: 674.0,
    retailPrice: 4099,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/shoes/1983355031/ref=sr_bs_1_1983355031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Tan",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B07PLJXCKC/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B09PRT3DRL/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cream",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B09PRV6GKY/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Black",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B07PM8XLWC/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B09H52BLKX/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Speed-Fashion-Womens-Tan-Handbag/dp/B09H53C1G9/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-50",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Cotton Fishcut Saree Shapewear,Petticoat,Skirt,Comfortwear(Side Slit)",
    asin: "B09KNPZ8LN",
    countReview: 451,
    imgUrl: "https://m.media-amazon.com/images/I/51CN5UllyuL._AC_UL320_.jpg",
    price: 492.0,
    retailPrice: 2100.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jaanvi-fashion-Womens-Shapewear-Comfortwear-Shapewear-Black-m/dp/B09KNPZ8LN/ref=sr_1_51?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-51",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Women Stylish Fancy and comfort Trending Flat sandal",
    asin: "B0BK2MS2Q2",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/41ZCJ2USXgL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Women-Stylish-comfort-Trending-sandal/dp/B0BK2MS2Q2/ref=sr_1_52?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-52",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Women-Stylish-comfort-Trending-sandal/dp/B0BK2MS2Q2/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-52",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Women-Stylish-comfort-Trending-sandal/dp/B0BK2JJK46/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-52",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Women-Stylish-comfort-Trending-sandal/dp/B0BK2YNPN8/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-52",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Women-Stylish-comfort-Trending-sandal/dp/B0BK2N61XH/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-52",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Stylish Chiffon Floral Printed Saree with Blouse Piece",
    asin: "B0B6G3RMQ8",
    countReview: 35,
    imgUrl: "https://m.media-amazon.com/images/I/817E0+VfbwL._AC_UL320_.jpg",
    price: 495.0,
    retailPrice: 2799,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/MIRCHI-FASHION-Chiffon-Printed-36575-Grey/dp/B0B6G3RMQ8/ref=sr_1_53?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-53",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Grey, Multi",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Printed-36575-Grey/dp/B0B6G3RMQ8/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dusty Plum , Multi",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Printed-36575-Grey/dp/B0B6GBN2FX/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Woven Silk Saree With Blouse Piece (4504_Color)",
    asin: "B09QX2Z55Y",
    countReview: 91,
    imgUrl: "https://m.media-amazon.com/images/I/71Xg1kk5+sL._AC_UL320_.jpg",
    price: 1745,
    retailPrice: 5999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B09QX2Z55Y/ref=sr_1_54?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Rani Pink",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B09QX2Z55Y/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Green",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1W7GQ1/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Magenta",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1VP998/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BFXKQ1WH/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0BG1V82CY/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Monjolika-Fashion-Womens-Blouse-4504_Pink/dp/B0B8ST3FGP/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Set of 2 Rose Gold Contemporary Cubic Zirconia Brass Kada Style Bracelet For Women-ZPFK11116",
    asin: "B09F35MLC1",
    countReview: 91,
    imgUrl: "https://m.media-amazon.com/images/I/71LxR8HzoUL._AC_UL320_.jpg",
    price: 467.0,
    retailPrice: 2750,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Contemporary-Zirconia-Bracelet-Women-ZPFK11116/dp/B09F35MLC1/ref=sr_1_55?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-55",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women's Relaxed Fit Joggers",
    asin: "B07T8PZ7KQ",
    countReview: 3359,
    imgUrl: "https://m.media-amazon.com/images/I/81x2Z3PJLCL._AC_UL320_.jpg",
    price: 281.0,
    retailPrice: 1499,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/FUNDAY-FASHION-Womens-Boyfriend-Jogger/dp/B07T8PZ7KQ/ref=sr_1_56?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-56",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark_Blue",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Boyfriend-Jogger/dp/B07T8PZ7KQ/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light_Blue",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Boyfriend-Jogger/dp/B07TCTQVD8/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Analog White Dial Women's Watch-TW000X219",
    asin: "B07QNXCKP6",
    countReview: 1877,
    imgUrl: "https://m.media-amazon.com/images/I/71WFlzi4ELL._AC_UL320_.jpg",
    price: 2845,
    retailPrice: 3695,
    productRating: "4.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Timex-Analog-White-Womens-Watch-TW000X219/dp/B07QNXCKP6/ref=sr_1_57?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-57",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women's Fit and flare Maxi Dress",
    asin: "B083BYPDKY",
    countReview: 734,
    imgUrl: "https://m.media-amazon.com/images/I/61kkoRxvMUL._AC_UL320_.jpg",
    price: 429.0,
    retailPrice: 999.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BYPDKY/ref=sr_1_58?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Wine",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BYPDKY/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Green",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BY4J91/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Lite Wine",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BY9V13/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mehandi",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BYRMSW/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BYJMR8/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sky Blue",
        dpUrl:
          "/IQRA-FASHION-Womens-Dress-FID_11-WINE-XL_Wine/dp/B083BY2JJR/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-58",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Rayon Classy Printed Maroon Top",
    asin: "B0B1DBW2HK",
    countReview: 87,
    imgUrl: "https://m.media-amazon.com/images/I/81KOZ8Iw7bL._AC_UL320_.jpg",
    price: 409.0,
    retailPrice: 1499,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/DMP-FASHION-Womens-Classy-Printed/dp/B0B1DBW2HK/ref=sr_1_59?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Maroon",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B0B1DBW2HK/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B09DQ5QM4B/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "DarkBlue",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B09MLZDTVP/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightBlue",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B09DQ5RP1G/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightGreen",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B09DQ553NS/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/DMP-FASHION-Womens-Classy-Printed/dp/B09DQ5RCTT/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Chiffon Banarasi Printed Saree With Solid Self Printed Blouse",
    asin: "B0BHNZ9GR5",
    countReview: 49,
    imgUrl: "https://m.media-amazon.com/images/I/61gA-vH799L._AC_UL320_.jpg",
    price: 775.0,
    retailPrice: 2100.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0BHNZ9GR5/ref=sr_1_60?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Pink 02",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0BHNZ9GR5/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green 04",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0BHNXN424/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green 05",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0BHNYC7SX/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green-03-A",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0B5TXM1V1/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green-03-C",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0B5TXXLZJ/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange 06",
        dpUrl:
          "/Jaanvi-Fashion-Chiffon-Banarasi-classical-12404-c-re/dp/B0BHNYSPJH/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Adorable Gold Plated Pearl Choker Necklace Set for Women",
    asin: "B08S2XF2DF",
    countReview: 6149,
    imgUrl: "https://m.media-amazon.com/images/I/811rJGje9xL._AC_UL320_.jpg",
    price: 175.0,
    retailPrice: 167,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/jewelry/2152562031/ref=sr_bs_12_2152562031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Fashion Sandal For Womens And Girls A4-Parent",
    asin: "B08FDVM8TX",
    countReview: 1137,
    imgUrl: "https://m.media-amazon.com/images/I/71SRDEGLYUL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/FASHIMO-A4-black-40-Womens-Sandals/dp/B08FDVM8TX/ref=sr_1_62?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B08FDVM8TX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "A.Black",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B09B5D8LL7/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B07X7RXXNV/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peach",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B07X8CQ8T1/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B08FDW192Q/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "A.Cream",
        dpUrl:
          "/FASHIMO-A4-black-40-Womens-Sandals/dp/B08DJ82CJZ/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "by Rajdeep Ranawat Premium Designer Women's Floral Print Kurta Kurti",
    asin: "B0B746TQQ3",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/91rRMJ5lxoL._AC_UL320_.jpg",
    price: 1979,
    retailPrice: 3599,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/RIVER-Kasturi-Regular-RV-AW22-KR-1139-2_Yellow_M/dp/B0B746TQQ3/ref=sr_1_63?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-63",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Women's Rayon Printed Top",
    asin: "B09FTBYRGX",
    countReview: 887,
    imgUrl: "https://m.media-amazon.com/images/I/71dgp1fOBhL._AC_UL320_.jpg",
    price: 369.0,
    retailPrice: 1200,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=sr_1_64?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTC285Y/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightBlue",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G2C498/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightGreen",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G1164C/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTDBRKC/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-64",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women Organza semi-stitched Lehenga choli (L8097_Green_Green_Free Size)",
    asin: "B09ZXL59X7",
    countReview: 39,
    imgUrl: "https://m.media-amazon.com/images/I/61xBBgL5QAL._AC_UL320_.jpg",
    price: 614.0,
    retailPrice: 2999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Basket-Organza-semi-stitched-L8097_Green_Green_Free/dp/B09ZXL59X7/ref=sr_1_65?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-65",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Women And Girl Fashion Metal Stretchable Gold Plated Belly Chain Waist Belt Jewellery kamarband Waistband",
    asin: "B09DL7LPZ7",
    countReview: 165,
    imgUrl: "https://m.media-amazon.com/images/I/61gZbGVvhTL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 799.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7LPZ7/ref=sr_1_66?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Golden Ring",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7LPZ7/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Belt",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL6VW7L/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Leather",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7L54H/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Leaves",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL95FTL/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multi chain",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL7DDZY/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Silver Ring",
        dpUrl:
          "/Stretchable-Jewellery-kamarband-Waistband-GoldenRing/dp/B09DL6HK62/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-66",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Fashion Sandals | Light weight, Comfortable & Trendy Flatform Sandals for Girls | Soft Footbed | Casual and Stylish Floaters for Walking, Working, All Day Wear",
    asin: "B0966CZZNS",
    countReview: 2934,
    imgUrl: "https://m.media-amazon.com/images/I/61Dxa4I8qmS._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1699,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B0966CZZNS/ref=sr_1_67?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-67",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black-Sea Green",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B0966CZZNS/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue-Pink",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B097K3L8TG/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White-Sea Green",
        dpUrl:
          "/TRASE-Outdoor-Floater-Sandals-Flatform/dp/B098JXQH9J/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Cotton Silk Unstitched Salwar Suit Dress Material, 2 Mtr Silk Salwar, Stylish Banarasi Dupatta By 24 Carat Suit (Jacquard 1; Free Size)",
    asin: "B09GVX4XGM",
    countReview: 304,
    imgUrl: "https://m.media-amazon.com/images/I/81IUsW+UYPL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/Sun-Fashion-Lifestyle-Jacquard-Unstitched/dp/B09GVX4XGM/ref=sr_1_68?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-68",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Rectanglular Sunglasses for Women Retro Driving Sunlgasses Vintage Fashion Narrow Square Frame UV400 Protection",
    asin: "B08XP26HTW",
    countReview: 3514,
    imgUrl: "https://m.media-amazon.com/images/I/5193amnm+wL._AC_UL320_.jpg",
    price: 355.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/elegante-Rectanglular-Sunglasses-Sunlgasses-Protection/dp/B08XP26HTW/ref=sr_1_69?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-69",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Latest Italian Designer 18k Gold Plated Crystal Earrings For Women and Girls",
    asin: "B075GZZ9RS",
    countReview: 1929,
    imgUrl: "https://m.media-amazon.com/images/I/61dCapSD4cL._AC_UL320_.jpg",
    price: 220.0,
    retailPrice: 1999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Earrings-8776er/dp/B075GZZ9RS/ref=sr_1_70?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-70",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Rayon Regular Three-Quarter Sleeves Straight Kurta",
    asin: "B09S3Y1CN1",
    countReview: 7,
    imgUrl: "https://m.media-amazon.com/images/I/61iSRMEVJ5L._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 499.0,
    productRating: "3.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09S3Y1CN1/ref=sr_1_71?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Off White1",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09S3Y1CN1/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue'",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09S3XVNN2/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "INDIGO",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09TFB475Q/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mint Green",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09TFB9S49/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off White",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09S3X36J1/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink1",
        dpUrl:
          "/Max-Printed-Straight-AEFKSU22LE_Off-White_2XL/dp/B09S3XS88J/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-71",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Stylish Fancy Golden Gold Plated Earrings for Women (9567er)",
    asin: "B07NQ1NWSY",
    countReview: 2276,
    imgUrl: "https://m.media-amazon.com/images/I/51UDJi2cwqS._AC_UL320_.jpg",
    price: 260.0,
    retailPrice: 1999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Stylish-Earrings/dp/B07NQ1NWSY/ref=sr_1_72?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-72",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Corduroy Button Down Pocket Shirts Casual Long Sleeve Oversized Blouses Tops",
    asin: "B0B86DBTZ4",
    countReview: 2627,
    imgUrl: "https://m.media-amazon.com/images/I/71LMGs6dccL._AC_UL320_.jpg",
    price: 419.0,
    retailPrice: 1999,
    productRating: "3.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B0B86DBTZ4/ref=sr_1_73?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White-Black",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B0B86DBTZ4/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B07HF5CHNY/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B08J4B6NC8/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B07HF4XNNH/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B09S2VGG9D/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "maroon",
        dpUrl:
          "/Leriya-Fashion-Casual-Oversized-White-Black/dp/B07MZ4GNGD/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-73",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Cotton Twill Full Sleeves Jacket Pack of 2",
    asin: "B08ZNHWFTL",
    countReview: 203,
    imgUrl: "https://m.media-amazon.com/images/I/615oGymU9LL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1699,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNHWFTL/ref=sr_1_74?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White:Neon",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNHWFTL/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon: Rani pink",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNJWB93/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White:Rani Pink",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNJ95PX/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black , Rani Pink",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNH15WG/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black, Bottle Green",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNHZNW9/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black, Maroon Red",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Cotton-Sleeves/dp/B08ZNHCB7S/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "New hot Stylish Sequin Knee Length Western Dresse Latest Mini Dress for Rayon Bodycon Women's Long Dress Shimmer Womens Girls Maxi a line one Piece Urbanic Style",
    asin: "B09PRN5VZW",
    countReview: 149,
    imgUrl: "https://m.media-amazon.com/images/I/61DZoaoEzLL._AC_UL320_.jpg",
    price: 419.0,
    retailPrice: 1999,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09PRN5VZW/ref=sr_1_75?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Navy Blue",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09PRN5VZW/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09PRMGZ4F/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09PRM343B/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Brown",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09PRMWRZM/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cream",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09VT5M8Q2/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multicolour",
        dpUrl:
          "/Leriya-Fashion-Stylish-Western-Bodycon/dp/B09VT61GKZ/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Fl0400l Slipper",
    asin: "B0995T7N9T",
    countReview: 43,
    imgUrl: "https://m.media-amazon.com/images/I/41Ykc5Q8o8L._AC_UL320_.jpg",
    price: 298.0,
    retailPrice: 499.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/FLITE-Womens-Peach-Slipper-FL0400L/dp/B0995T7N9T/ref=sr_1_76?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-76",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark Grey Peach",
        dpUrl:
          "/FLITE-Womens-Peach-Slipper-FL0400L/dp/B0995T7N9T/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Tan",
        dpUrl:
          "/FLITE-Womens-Peach-Slipper-FL0400L/dp/B0995T6NJ8/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Dresses for Women || Dresses for Women Dress || Dress for Women || Western Dresses for Women || Women Dress || Dresses (J-492-523)",
    asin: "B0BFQZ1Q6Z",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81+lEDk0kiL._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 2999,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Dresses-Women-Western-J-492-523/dp/B0BFQZ1Q6Z/ref=sr_1_77?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-77",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Women's Tai-Dye Printed Mesh Rayon Lycra High V-Collar Neck Long Kurti",
    asin: "B0B36DM8G8",
    countReview: 51,
    imgUrl: "https://m.media-amazon.com/images/I/51jwuItlp9L._AC_UL320_.jpg",
    price: 519.0,
    retailPrice: 1999,
    productRating: "2.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B36DM8G8/ref=sr_1_78?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B36DM8G8/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B6GKRZXT/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B6GHCK9W/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B36D74QN/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sky Blue",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B6GHPJ8B/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/Leriya-Fashion-Tai-Dye-V-Collar-XX-Large/dp/B0B6GHXYD1/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Latest Collection Gold Plated Combo of 2 Bracelet for Women and Girls (Rose Gold) (cmb285_8306b_9556b)",
    asin: "B07DP2ZB5L",
    countReview: 2546,
    imgUrl: "https://m.media-amazon.com/images/I/71CV1ddvQAL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 3499,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Collection-cmb285_8306b_9556b/dp/B07DP2ZB5L/ref=sr_1_79?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-79",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women Regular Fit Solid Casual Half Sleeves Shirt",
    asin: "B09X1HWN9S",
    countReview: 24,
    imgUrl: "https://m.media-amazon.com/images/I/514tTxKQL3L._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1299,
    productRating: "3.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09X1HWN9S/ref=sr_1_80?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Petrol Blue",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09X1HWN9S/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09WKN3ZWN/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09WKMSDDG/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09WKNJP3T/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09WKNG3KN/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peach",
        dpUrl:
          "/FUNDAY-FASHION-Regular-Casual-Sleeves/dp/B09WKNX9G1/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-80",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Latest Stylish Fancy Oxidised Silver Tribal Necklace Jewellery Set for Women (12164s), Multicolour, One",
    asin: "B09KXGKG36",
    countReview: 921,
    imgUrl: "https://m.media-amazon.com/images/I/71CSGmv8tQL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-12164s-Multicolour/dp/B09KXGKG36/ref=sr_1_81?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-81",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's girls Thin Strap Sling spaghetti tank top comfortable camisole crop top free size (28 to 32) bust size",
    asin: "B09MPR2F38",
    countReview: 321,
    imgUrl: "https://m.media-amazon.com/images/I/21jWwVCiVtL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B09MPR2F38/ref=sr_1_82?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Skin",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B09MPR2F38/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Black",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B0977MMW73/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Royal Blue",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B0977PPC83/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Baby-pink",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B08QRDDHVY/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B08ZD8SKN1/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark pink",
        dpUrl:
          "/DAVINA-FASHIONS-Spaghetti-Comfortable-Camisole/dp/B08X4VM7S9/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Sling Bag",
    asin: "B07R24CVN9",
    countReview: 2164,
    imgUrl: "https://m.media-amazon.com/images/I/71FpK71lT6L._AC_UL320_.jpg",
    price: 259.0,
    retailPrice: 999.0,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sleema-fashion-Fashionable-Stylish-Handbag/dp/B07R24CVN9/ref=sr_1_83?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-83",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Pink",
        dpUrl:
          "/Sleema-fashion-Fashionable-Stylish-Handbag/dp/B07R24CVN9/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Sleema-fashion-Fashionable-Stylish-Handbag/dp/B07G4CB182/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Sleema-fashion-Fashionable-Stylish-Handbag/dp/B07G49XBT3/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Binary Star Fashion Leggings|Combo|Collections",
    asin: "B0B7FB82LV",
    countReview: 42,
    imgUrl: "https://m.media-amazon.com/images/I/51Uxfsi2PXL._AC_UL320_.jpg",
    price: 389.0,
    retailPrice: 799.0,
    productRating: "3.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FB82LV/ref=sr_1_84?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "SKIN-RED-BLACK",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FB82LV/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK-RED-T.BLUE",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FB5GN5/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK-WHITE-MAROON",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FBDG6Y/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "ROSE-P.GREEN-BLUE",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7F9ZGD4/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK-WHITE-RED",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FCHBNX/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLUE-BLACK-RED",
        dpUrl:
          "/Womens-Regular-Cotton-Leggings-BSF-123_Multicolor_3XL/dp/B0B7FC9HVD/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-84",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Fashion-13 Sports Running,Walking & Gym Shoes for Eva Sole Casual Sneaker Lightweight Lace-Up Shoes for Girl's",
    asin: "B0B5ZS6FQ2",
    countReview: 4067,
    imgUrl: "https://m.media-amazon.com/images/I/61ZAzlUu4-L._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/ASIAN-Fashion-13-Running-Lightweight-Numeric_6/dp/B0B5ZS6FQ2/ref=sr_1_85?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-85",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "2Multi",
        dpUrl:
          "/ASIAN-Fashion-13-Running-Lightweight-Numeric_6/dp/B0B5ZS6FQ2/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-85",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/ASIAN-Fashion-13-Running-Lightweight-Numeric_6/dp/B07FRWTNP3/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-85",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/ASIAN-Fashion-13-Running-Lightweight-Numeric_6/dp/B07FS3DQNL/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-85",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "1Grey",
        dpUrl:
          "/ASIAN-Fashion-13-Running-Lightweight-Numeric_6/dp/B0994L7JFN/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-85",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "KHEEAA Fashion's Women's Traditional Cotton Patiala Salwar for Women (Free Size; Pack of- 1) (Colour)",
    asin: "B09GMPXLVG",
    countReview: 3,
    imgUrl: "https://m.media-amazon.com/images/I/61eEukVUXdS._AC_UL320_.jpg",
    price: 389.0,
    retailPrice: 599.0,
    productRating: "4.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMPXLVG/ref=sr_1_86?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "RANI PINK",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMPXLVG/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BEIGE",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMQW94P/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMQBNR9/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BOTTLE GREEN",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMQ3FRJ/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "CREAM",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMQNWP6/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "GREY",
        dpUrl:
          "/KHEEAA-Fashions-Womens-Traditional-Patiala/dp/B09GMPPPR8/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-86",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Plus Size Short Sleeve Deep V Neck Self Belted Casual Peplum Wrap Blouse Top",
    asin: "B09TP93LGX",
    countReview: 630,
    imgUrl: "https://m.media-amazon.com/images/I/61+YxDK87uL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 1999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP93LGX/ref=sr_1_87?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Beige",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP93LGX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP94M9Q/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP7LT2W/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP9YBQV/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP8XZBC/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/Leriya-Fashion-Womens-Sleeve-Belted/dp/B09TP6LB9N/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Fashion Jewellery Gold Plated Ear rings Combo of Earrings for Girls and Women",
    asin: "B0B5521TL3",
    countReview: 21,
    imgUrl: "https://m.media-amazon.com/images/I/61qGsfR+i1L._AC_UL320_.jpg",
    price: 327.0,
    retailPrice: 1599,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/YouBella-Fashion-Jewellery-Plated-Earrings/dp/B0B5521TL3/ref=sr_1_88?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-88",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Women's Banarasi Art Silk Half Half Saree with Blouse Piece",
    asin: "B097775RLH",
    countReview: 8,
    imgUrl: "https://m.media-amazon.com/images/I/91IoNdJ1yYS._AC_UL320_.jpg",
    price: 527.0,
    retailPrice: 2649,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Nivah-Fashion-Womens-Kanjivaram-NH-S21-Blue/dp/B097775RLH/ref=sr_1_89?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-89",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/Nivah-Fashion-Womens-Kanjivaram-NH-S21-Blue/dp/B097775RLH/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-89",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Nivah-Fashion-Womens-Kanjivaram-NH-S21-Blue/dp/B09777JKKK/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-89",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Nivah-Fashion-Womens-Kanjivaram-NH-S21-Blue/dp/B09777P419/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-89",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise",
        dpUrl:
          "/Nivah-Fashion-Womens-Kanjivaram-NH-S21-Blue/dp/B09776T7P7/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-89",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Analog Women's Watch",
    asin: "B014KJ58P8",
    countReview: 2105,
    imgUrl: "https://m.media-amazon.com/images/I/81Fw82U3paL._AC_UL320_.jpg",
    price: 1984,
    retailPrice: 3295,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Timex-Fashion-Analog-Womens-Watch-TW000T610/dp/B014KJ58P8/ref=sr_1_90?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-90",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Fancy Cotton Blend Leheriya Zig Zag Printed Saree with Blouse Piece",
    asin: "B09MTD4D8K",
    countReview: 43,
    imgUrl: "https://m.media-amazon.com/images/I/81SIYB60ubL._AC_UL320_.jpg",
    price: 485.0,
    retailPrice: 2699,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/MIRCHI-FASHION-Chevron-Pattern-33012-Grey/dp/B09MTD4D8K/ref=sr_1_91?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-91",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Grey",
        dpUrl:
          "/MIRCHI-FASHION-Chevron-Pattern-33012-Grey/dp/B09MTD4D8K/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/MIRCHI-FASHION-Chevron-Pattern-33012-Grey/dp/B0B6G65C24/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Tomato Red",
        dpUrl:
          "/MIRCHI-FASHION-Chevron-Pattern-33012-Grey/dp/B0B6FTH6RX/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Self-design Regular Jacket",
    asin: "B07ZPXZXYX",
    countReview: 108,
    imgUrl: "https://m.media-amazon.com/images/I/81m4kvwW-vL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1499,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B07ZPXZXYX/ref=sr_1_92?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Maroon",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B07ZPXZXYX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Army Green",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B07ZKXFF6G/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Pink",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B0852HDZ8Z/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard Yellow",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B07ZKX93DV/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Neon Green",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B08CHNXKWW/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/FUNDAY-FASHION-Design-Cotton-Jacket/dp/B08CHLD9SR/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-92",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Wonderful Hanging Ball Plushy Drop Earrings For Women/Girls",
    asin: "B07VDTKL42",
    countReview: 2866,
    imgUrl: "https://m.media-amazon.com/images/I/51ldct2ysCL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/jewelry/7124380031/ref=sr_bs_44_7124380031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Black Women Training Zip-Up Jacket, Sports Running Yoga Jacket for Running, Sports and Workout",
    asin: "B0B3TS6VXF",
    countReview: 3,
    imgUrl: "https://m.media-amazon.com/images/I/41yMfQL5KbL._AC_UL320_.jpg",
    price: 999.0,
    retailPrice: 1599,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/HIDELT-Training-Zip-Up-Running-Workout/dp/B0B3TS6VXF/ref=sr_1_94?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-94",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Women's Ikkat Cotton Unstitched Salwar Suit Dress Material, 2 Mtr Cotton Salwar, Stylish Ikkat Dupatta By 24 Carat Suit (Ikkat 1; Free Size)",
    asin: "B098LB5XMP",
    countReview: 397,
    imgUrl: "https://m.media-amazon.com/images/I/51CEHART+SS._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 2599,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/Sun-Fashion-Lifestyle-Unstitched-Material/dp/B098LB5XMP/ref=sr_1_95?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-95",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Women's Utsav 9-comf-aw19 Flat Sandal",
    asin: "B09ZYM1R98",
    countReview: 35,
    imgUrl: "https://m.media-amazon.com/images/I/81H07ahKiiL._AC_UL320_.jpg",
    price: 662.0,
    retailPrice: 849.0,
    productRating: "2.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/BATA-Women-Utsav-9-Comf-Aw19-6612423/dp/B09ZYM1R98/ref=sr_1_96?keywords=women+fashion&qid=1668703211&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-96",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "womens 32-998 Fashion Sandals",
    asin: "B0843PW7PS",
    countReview: 204,
    imgUrl: "https://m.media-amazon.com/images/I/61pwKBqYDUL._AC_UL320_.jpg",
    price: 626.0,
    retailPrice: 1290,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Metro-Womens-Outdoor-Sandals-32-998/dp/B0843PW7PS/ref=sr_1_97?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-97",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Chikoo",
        dpUrl:
          "/Metro-Womens-Outdoor-Sandals-32-998/dp/B0843PW7PS/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-97",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Gun Metal",
        dpUrl:
          "/Metro-Womens-Outdoor-Sandals-32-998/dp/B0843N24SD/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-97",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Metro-Womens-Outdoor-Sandals-32-998/dp/B0843GJ4QW/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-97",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Latest Stylish Traditional Oxidised Silver Necklace Jewellery Set for Women (13199s)",
    asin: "B0BFQCCBDF",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61FO49z5hNL._AC_UL320_.jpg",
    price: 359.0,
    retailPrice: 1999,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Traditional-13199s/dp/B0BFQCCBDF/ref=sr_1_98?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-98",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Latest Stylish Metal Boho Midi Finger Rings for Women and Girls - Set of 10 (rrsd14183r), Silver",
    asin: "B0B5LF3C4R",
    countReview: 17,
    imgUrl: "https://m.media-amazon.com/images/I/51gSm3Oj5FL._AC_UL320_.jpg",
    price: 249.0,
    retailPrice: 1499,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Latest-Stylish/dp/B0B5LF3C4R/ref=sr_1_99?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-99",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Rose Gold Kundan & Dazzling Austrian Diamonds Embellished Jhumki Drops Earring For Women-ZPFK13786",
    asin: "B0B1JGPSNJ",
    countReview: 15,
    imgUrl: "https://m.media-amazon.com/images/I/71nXd36F25L._AC_UL320_.jpg",
    price: 539.0,
    retailPrice: 2995,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dazzling-Austrian-Diamonds-Embellished-Women-ZPFK13786/dp/B0B1JGPSNJ/ref=sr_1_100?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-100",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Women's Solid Regular Jacket",
    asin: "B07PB5N8Z5",
    countReview: 3812,
    imgUrl: "https://m.media-amazon.com/images/I/71h8PRDMbFL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1499,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Funday-Fashion-Sleeve-Womens-Jacket/dp/B07PB5N8Z5/ref=sr_1_101?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-101",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark Blue",
        dpUrl:
          "/Funday-Fashion-Sleeve-Womens-Jacket/dp/B07PB5N8Z5/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-101",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Funday-Fashion-Sleeve-Womens-Jacket/dp/B07P6RD8NL/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-101",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Italian Designer Copper Earrings for Women (Blue)(9794er)",
    asin: "B07F785NXL",
    countReview: 942,
    imgUrl: "https://m.media-amazon.com/images/I/7152HpxyXfL._AC_UL320_.jpg",
    price: 225.0,
    retailPrice: 1999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Designer-9794er/dp/B07F785NXL/ref=sr_1_102?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-102",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Earrings for Women & Girls | Fashion Multicolor Crystal and Opal Drop | Rose Gold Plated | Oval Shaped Drop Earrings | Accessories Jewellery | Birthday & Anniversary Gift",
    asin: "B01G6XSADS",
    countReview: 1982,
    imgUrl: "https://m.media-amazon.com/images/I/61KGVH4uCSL._AC_UL320_.jpg",
    price: 219.0,
    retailPrice: 999.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Austrian-Designer-YCFJER-01077-RG/dp/B01G6XSADS/ref=sr_1_103?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-103",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Women's Handbag (BLACK PRINT_Black)",
    asin: "B081JX6YZB",
    countReview: 2147,
    imgUrl: "https://m.media-amazon.com/images/I/81whhNx7zjL._AC_UL320_.jpg",
    price: 287.0,
    retailPrice: 999.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/jsm-fashion-stylish-beautyful-black/dp/B081JX6YZB/ref=sr_1_104?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-104",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Rose Gold Tone Dazzling Austrian Diamonds Embellished Party Bling Cuff Bracelet For Women-ZPFK12103",
    asin: "B09RFKF4S8",
    countReview: 17,
    imgUrl: "https://m.media-amazon.com/images/I/71KW7GqrfaL._AC_UL320_.jpg",
    price: 350.0,
    retailPrice: 1995,
    productRating: "3.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Dazzling-Austrian-Diamonds-Embellished-Women-ZPFK12103/dp/B09RFKF4S8/ref=sr_1_105?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-105",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "1 Gram Gold Jewellery Chains For Men Chain For Boys Accessories For Women Antique Jewellery Fashion Jewelry For Women Jewellery For Girls Gold Necklace For Women Men Accessories",
    asin: "B07DW7GRN3",
    countReview: 283,
    imgUrl: "https://m.media-amazon.com/images/I/61tCNkVaURL._AC_UL320_.jpg",
    price: 213.0,
    retailPrice: 899.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/AanyaCentric-Plated-Plain-Chain-Women/dp/B07DW7GRN3/ref=sr_1_106?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-106",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Earrings for Women & Girls | Fashion Crystal Studded Dangler Earring | Western Long Dangler Earrings | Accessories Jewellery | Birthday & Anniversary Gift",
    asin: "B08ZYQDG6D",
    countReview: 423,
    imgUrl: "https://m.media-amazon.com/images/I/61nhTY3k-TL._AC_UL320_.jpg",
    price: 356.0,
    retailPrice: 1249,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Elegant-Fashion-Earrings/dp/B08ZYQDG6D/ref=sr_1_107?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-107",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Doll Fashion Women 2 Layer hoopskirts White for Heavy Lehenga and Ball Gown with Hoop and Layer",
    asin: "B0BDVDTXKG",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/213Grl5HgHL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 999.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Women-Layer-hoopskirts-Lehenga/dp/B0BDVDTXKG/ref=sr_1_108?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-108",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "ROKKS Woman's Fashionable Mid-Heel Sandals Flip-Flops Regular Wear Comfortable Slipper for Girls/Ladies(AE-Heel-031)",
    asin: "B0BHP5JWCC",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/51VY9CFE1uL._AC_UL320_.jpg",
    price: 289.0,
    retailPrice: 599.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Enterprises-Fashion-Mid-Heel-Comfortable-Heel-Black-05/dp/B0BHP5JWCC/ref=sr_1_109?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-109",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Delicate Ad Geometric Gold Plated Stud & Drop Earrings For Women Girls Western Stylish Latest Fancy Earrings Set Combo",
    asin: "B0B7MQ51WR",
    countReview: 8,
    imgUrl: "https://m.media-amazon.com/images/I/71CLLBAa3EL._AC_UL320_.jpg",
    price: 249.0,
    retailPrice: 999.0,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Frill-Delicate-Geometric-Earrings/dp/B0B7MQ51WR/ref=sr_1_110?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-110",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Latest Stylish Platinum Plated Austrian Crystal Bracelet for Women and Girls (11956b), blue, free",
    asin: "B091M67W1H",
    countReview: 11,
    imgUrl: "https://m.media-amazon.com/images/I/61BAD-slDBS._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 1999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Platinum-11956b/dp/B091M67W1H/ref=sr_1_111?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-111",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Fashion Checker Pattern PU Leather Solid Color Shoulder Crossbody Messenger Bag Casual Ladies Small Flap Purse Handbags",
    asin: "B0B5LNTFG7",
    countReview: 21,
    imgUrl: "https://m.media-amazon.com/images/I/41A+KFmUV5L._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1099,
    productRating: "4.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LNTFG7/ref=sr_1_112?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-112",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Purple",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LNTFG7/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-112",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LQ9RG5/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-112",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LPMDR1/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-112",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/ALMURAT-Fashion-Shoulder-Crossbody-Messenger/dp/B0B5LP1PCX/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-112",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "(34 STRIPES) Women Fashion tape, Clothing & Body, Strong and Clear Tape for All Skin Tones and Fabric, Waterproof & Sweatproof, Bra Tape Strips.",
    asin: "B0B2RK43PL",
    countReview: 232,
    imgUrl: "https://m.media-amazon.com/images/I/51ENOz1PSFL._AC_UL320_.jpg",
    price: 155.0,
    retailPrice: 249.0,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Mincerfit-STRIPES-Clothing-Waterproof-Sweatproof/dp/B0B2RK43PL/ref=sr_1_113?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-113",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Women's PU Leather Fashion Biker Jacket",
    asin: "B0BCYP6LWZ",
    countReview: 7,
    imgUrl: "https://m.media-amazon.com/images/I/51IPDkyB9-L._AC_UL320_.jpg",
    price: 1499,
    retailPrice: 2499,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/YOONIKK-Womens-Leather-Fashion-Bikers/dp/B0BCYP6LWZ/ref=sr_1_114?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-114",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/YOONIKK-Womens-Leather-Fashion-Bikers/dp/B0BCYP6LWZ/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-114",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/YOONIKK-Womens-Leather-Fashion-Bikers/dp/B0BCYMDN5V/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-114",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Cotton Fishcut Saree Shapewear,Petticoat,Skirt,Comfortwear(Side Slit)",
    asin: "B09KNPZ8LN",
    countReview: 451,
    imgUrl: "https://m.media-amazon.com/images/I/51CN5UllyuL._AC_UL320_.jpg",
    price: 492.0,
    retailPrice: 2100.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jaanvi-fashion-Womens-Shapewear-Comfortwear-Shapewear-Black-m/dp/B09KNPZ8LN/ref=sr_1_115?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-115",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Women's Yoga Dress Pants Stretchy Work Slacks Business Casual Office Straight Leg/Bootcut Elastic Waist with 2 Pockets Regular Fit Trouser",
    asin: "B09SLGVFKS",
    countReview: 239,
    imgUrl: "https://m.media-amazon.com/images/I/51LPkXgiUsL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGVFKS/ref=sr_1_116?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGVFKS/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B0B42D4RND/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLJZ33S/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLDLBDZ/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLJGPCP/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Leriya-Fashion-Stretchy-Business-Straight/dp/B09SLGCVSP/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-116",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "womens Footsteps - She*s Cute Sandal",
    asin: "B09M3W6JDB",
    countReview: 18,
    imgUrl: "https://m.media-amazon.com/images/I/61Kr5peocJL._AC_UL320_.jpg",
    price: 3119,
    retailPrice: 4799,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Skechers-Womens-Fashion-Sandals-111096-BBK-FOOTSTEPS-SHE-CUTE/dp/B09M3W6JDB/ref=sr_1_117?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-117",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "womens Lille Beige 4 UK Fashion Sandals 6618515",
    asin: "B07W6QMP6G",
    countReview: 231,
    imgUrl: "https://m.media-amazon.com/images/I/71JZ2+Ny0zL._AC_UL320_.jpg",
    price: 7129,
    retailPrice: 8999.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/BATA-Womens-Fashion-Sandals-4-6618915/dp/B07W6QMP6G/ref=sr_1_118?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-118",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Women Stylish Trending and comfort Fancy Flat Fashion sandal…",
    asin: "B09268V95P",
    countReview: 1246,
    imgUrl: "https://m.media-amazon.com/images/I/61MajYBZfRL._AC_UL320_.jpg",
    price: 289.0,
    retailPrice: 999.0,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/AK-SKY-Stylish-Trending-comfort/dp/B09268V95P/ref=sr_1_119?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-119",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White",
        dpUrl:
          "/AK-SKY-Stylish-Trending-comfort/dp/B09268V95P/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-119",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/AK-SKY-Stylish-Trending-comfort/dp/B0926CKSTB/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-119",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/AK-SKY-Stylish-Trending-comfort/dp/B092698VC8/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-119",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "women's Jewellery Oxidised Mirror Bangle Bracelet (Silver)",
    asin: "B09JBYQFNR",
    countReview: 18,
    imgUrl: "https://m.media-amazon.com/images/I/71U6n6gXt9L._AC_UL320_.jpg",
    price: 196.0,
    retailPrice: 599.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Total-Fashion-Jewellery-Oxidised-Bracelet/dp/B09JBYQFNR/ref=sr_1_120?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-120",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Droposale (36 Strips) Fashion Body Tape, Clear Fabric Strong Double Sided Tape for Clothes Dress Bra Skin Bikini (Pack of 1)",
    asin: "B09YCWCSYB",
    countReview: 160,
    imgUrl: "https://m.media-amazon.com/images/I/710NbzrSC+L._AC_UL320_.jpg",
    price: 175.0,
    retailPrice: 249.0,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Droposale-Strips-Fashion-Fabric-Clothes/dp/B09YCWCSYB/ref=sr_1_121?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-121",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [],
  },
  {
    productDescription:
      "Earrings for Women and Girls | Fashion Silver Dangler Earring | Silver Toned Long Chain Dangler Earring | Butterfly Shaped Western Danglers Earrings | Accessories Jewellery for Women | Birthday Gift for Girls and Women Anniversary Gift for Wife",
    asin: "B07XZBLWNF",
    countReview: 147,
    imgUrl: "https://m.media-amazon.com/images/I/51C3lw3VZxL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Exquisite-Butterfly-Earrings/dp/B07XZBLWNF/ref=sr_1_122?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-122",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "18k Rose Gold Plated Cubic Zirconia Adjustable Bracelet Jewellery with Pull-Chain for Women & Girls (ADB161RG)",
    asin: "B09MWC3KDZ",
    countReview: 43,
    imgUrl: "https://m.media-amazon.com/images/I/71obr8a4hVL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 2.999,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jewels-Adjustable-Jewellery-Pull-Chain-ADB161RG/dp/B09MWC3KDZ/ref=sr_1_123?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-123",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Non Precious Metal Jhumki Earrings (Multicolour, ZPFK9024)",
    asin: "B07X1XB6ZM",
    countReview: 1095,
    imgUrl: "https://m.media-amazon.com/images/I/71J9644uGEL._AC_UL320_.jpg",
    price: 269.0,
    retailPrice: 339,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Pearls-Inspired-Enamelling-Women-ZPFK9024/dp/B07X1XB6ZM/ref=sr_1_124?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-124",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "womens Sf0079l Slipper",
    asin: "B08ZNKYFFM",
    countReview: 158,
    imgUrl: "https://m.media-amazon.com/images/I/71E3e4CyetL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 425,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sparx-Womens-Green-Slipper-SF0079L/dp/B08ZNKYFFM/ref=sr_1_125?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-125",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "R. Blue S. Green",
        dpUrl:
          "/Sparx-Womens-Green-Slipper-SF0079L/dp/B08ZNKYFFM/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-125",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK RED",
        dpUrl:
          "/Sparx-Womens-Green-Slipper-SF0079L/dp/B08ZNK8SZD/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-125",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "B.Pink D.Pink",
        dpUrl:
          "/Sparx-Womens-Green-Slipper-SF0079L/dp/B08ZNM63H7/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-125",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Red",
        dpUrl:
          "/Sparx-Womens-Green-Slipper-SF0079L/dp/B08ZNJVV4H/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-125",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "18k Rose Gold Plated Latest Fancy Stylish Copper Zircon Bali Earrings for Women and Girls",
    asin: "B099S9FJ52",
    countReview: 826,
    imgUrl: "https://m.media-amazon.com/images/I/513MF4mXUcL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shining-Diva-Fashion-Earrings-12838er/dp/B099S9FJ52/ref=sr_1_126?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-126",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Floral Printed Latest & Stylish PU Leather Handbags",
    asin: "B09HL184MX",
    countReview: 152,
    imgUrl: "https://m.media-amazon.com/images/I/91MeaG7K+rL._AC_UL320_.jpg",
    price: 495.0,
    retailPrice: 1299,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HL184MX/ref=sr_1_127?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HL184MX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HL2P6SS/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HL2N34S/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HL24R1H/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Pink",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HP62HK3/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/FASHION-Printed-Stylish-Leather-Handbags/dp/B09HP55V8L/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-127",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "womens Sf0558l Fashion Slippers",
    asin: "B07R4P22HR",
    countReview: 2490,
    imgUrl: "https://m.media-amazon.com/images/I/81VGnEU2uqL._AC_UL320_.jpg",
    price: 280.0,
    retailPrice: 299.0,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sparx-Womens-Fashion-Slippers-4-SF0558L_RDWH0004/dp/B07R4P22HR/ref=sr_1_128?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-128",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "RED WHITE",
        dpUrl:
          "/Sparx-Womens-Fashion-Slippers-4-SF0558L_RDWH0004/dp/B07R4P22HR/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-128",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK WHITE",
        dpUrl:
          "/Sparx-Womens-Fashion-Slippers-4-SF0558L_RDWH0004/dp/B07R6N19VW/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-128",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NAVY WHITE",
        dpUrl:
          "/Sparx-Womens-Fashion-Slippers-4-SF0558L_RDWH0004/dp/B07R5P8R9J/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-128",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Flat Sandals for Women & Girls|Lightweight, Comfortable & Trendy |Soft footbed| Casual and Stylish Sandals for Walking, Working, All Day Wear",
    asin: "B07XMNPTS7",
    countReview: 3329,
    imgUrl: "https://m.media-amazon.com/images/I/61IxDXGJWDL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 499.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/TRASE-Qure-Black-Women-Sandal/dp/B07XMNPTS7/ref=sr_1_129?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-129",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/TRASE-Qure-Black-Women-Sandal/dp/B07XMNPTS7/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-129",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/TRASE-Qure-Black-Women-Sandal/dp/B06XJ534BC/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-129",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Rayon Printed Top",
    asin: "B09FTBYRGX",
    countReview: 887,
    imgUrl: "https://m.media-amazon.com/images/I/71dgp1fOBhL._AC_UL320_.jpg",
    price: 369.0,
    retailPrice: 1.2,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=sr_1_130?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTBYRGX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTC285Y/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightBlue",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G2C498/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "LightGreen",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B094G1164C/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/DMP-FASHION-Womens-Rayon-Printed/dp/B09FTDBRKC/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-130",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women Fashion Block Heel Sandals",
    asin: "B098746DYX",
    countReview: 398,
    imgUrl: "https://m.media-amazon.com/images/I/615srujRbCS._AC_UL320_.jpg",
    price: 379.0,
    retailPrice: 999.0,
    productRating: "3.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B098746DYX/ref=sr_1_131?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B098746DYX/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peach",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B09874TMJ9/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B09873X9ZV/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cream",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B08TBJJQ67/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cgreen",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B09874Z8CC/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/SHOFIEE-Women-Fashion-Sandals-Numeric_7/dp/B09873MD6B/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-131",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Stylish Chiffon Contrast Border-Pallu Printed Saree with Blouse Piece",
    asin: "B09MTCX1YY",
    countReview: 430,
    imgUrl: "https://m.media-amazon.com/images/I/81pY2tcvWnL._AC_UL320_.jpg",
    price: 459.0,
    retailPrice: 1899,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09MTCX1YY/ref=sr_1_132?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Mustard, Brown",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09MTCX1YY/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey, Coffee",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09XVDDH87/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Magenta Pink, Coffee",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09W8Z3FG2/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peacock Blue, Coffee",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09W91RR63/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink, Coffee",
        dpUrl:
          "/MIRCHI-FASHION-Chiffon-Contrast-33034-Mustard/dp/B09XVG7SH3/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-132",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Women's Slipper",
    asin: "B09TW4CBMB",
    countReview: 396,
    imgUrl: "https://m.media-amazon.com/images/I/71+DBcOQQ+L._AC_UL320_.jpg",
    price: 385.0,
    retailPrice: 499.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW4CBMB/ref=sr_1_133?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Aqua Navy",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW4CBMB/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Red",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B0B2RKLC9V/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Baby Pink Wine",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW54DF1/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black White",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW2MQHJ/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Pink Navy",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW429VV/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peach Black",
        dpUrl:
          "/Flite-Womens-Slippers-Aqua-FL2008LAQNV0005/dp/B09TW5KGSF/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-133",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Womens Violet Heels, (6616967)",
    asin: "B08FMGN9JM",
    countReview: 241,
    imgUrl: "https://m.media-amazon.com/images/I/61NehP0+waS._AC_UL320_.jpg",
    price: 899.0,
    retailPrice: 990.0,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/BATA-Violet-Black-Fashion-Sandals/dp/B08FMGN9JM/ref=sr_1_134?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-134",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Double Stick Strips 36 Count",
    asin: "B0006IO3NG",
    countReview: 3508,
    imgUrl: "https://m.media-amazon.com/images/I/71jAMkcf7YL._AC_UL320_.jpg",
    price: 141.0,
    retailPrice: 299.0,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/automotive/5258203031/ref=sr_bs_38_5258203031_1",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Attractive Gold Plated Necklace Kada Combo Set for Women (CBMIX104227)",
    asin: "B09Y3DJT2T",
    countReview: 197,
    imgUrl: "https://m.media-amazon.com/images/I/71Pz1oNuAPL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 1.164,
    productRating: "3.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sukkhi-Attractive-Plated-Necklace-CBMIX104227/dp/B09Y3DJT2T/ref=sr_1_136?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-136",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Women's Full Sleeve Top Round Neck Casual Tshirt (Empire4-23\" Inches)",
    asin: "B0B69NPCNK",
    countReview: 5,
    imgUrl: "https://m.media-amazon.com/images/I/51rGze59quL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 799.0,
    productRating: "3.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69NPCNK/ref=sr_1_137?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Garnet Maroon",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69NPCNK/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Royal Blue",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69J53SF/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Baby Pink",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69L24DW/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Frost White",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69K78Q6/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hunter Green",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69HXS7C/ref=cs_sr_dp_5?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Matt Black",
        dpUrl:
          "/Dream-Beauty-Fashion-Top4-Empire-Maroon-M/dp/B0B69F8YHB/ref=cs_sr_dp_6?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-137",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Pink Meenakari Lotus Design Ethnic Green Beads Drop Earring For Women-ZPFK14740",
    asin: "B0BDG3ZD9J",
    countReview: 4,
    imgUrl: "https://m.media-amazon.com/images/I/51BZI56-GLL._AC_UL320_.jpg",
    price: 292.0,
    retailPrice: 1950,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Pearls-Meenakari-Earring-Women-ZPFK14740/dp/B0BDG3ZD9J/ref=sr_1_138?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-138",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Green Golden Non Precious Metal Dangler Earrings for Women (ZPFK9033)",
    asin: "B07X5ZQTBY",
    countReview: 162,
    imgUrl: "https://m.media-amazon.com/images/I/71NLiRni++L._AC_UL320_.jpg",
    price: 428,
    retailPrice: 2967,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Pearls-Traditional-Meenakari-Women-ZPFK9033/dp/B07X5ZQTBY/ref=sr_1_139?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-139",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Green & Pink Stones Ethnic Collar Bone Necklace Earring & Ring Set For Women-ZPFK11665",
    asin: "B09Q18JZTQ",
    countReview: 113,
    imgUrl: "https://m.media-amazon.com/images/I/61UNdghx4cL._AC_UL320_.jpg",
    price: 450,
    retailPrice: 2495,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Zaveri-Pearls-Necklace-Earring-Women-ZPFK11665/dp/B09Q18JZTQ/ref=sr_1_140?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-140",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Sweatshirt/Hoodie for Women and Girl",
    asin: "B08JYGDGJB",
    countReview: 1491,
    imgUrl: "https://m.media-amazon.com/images/I/817pXzcZIFL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1099,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/FUNDAY-FASHION-Womens-Hoodies-SYS-4095_Yellow_X-Large/dp/B08JYGDGJB/ref=sr_1_141?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-141",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Yellow",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Hoodies-SYS-4095_Yellow_X-Large/dp/B08JYGDGJB/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-141",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red,Black",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Hoodies-SYS-4095_Yellow_X-Large/dp/B09R5TGY85/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-141",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Hoodies-SYS-4095_Yellow_X-Large/dp/B082J15CZM/ref=cs_sr_dp_3?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-141",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/FUNDAY-FASHION-Womens-Hoodies-SYS-4095_Yellow_X-Large/dp/B082HZYM5W/ref=cs_sr_dp_4?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-141",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Women's Gerogette Embroidered & Sequence Lehenga Choli(FB-LH-L8215-17_Free Size)",
    asin: "B0BCVQ6ZFL",
    countReview: 44,
    imgUrl: "https://m.media-amazon.com/images/I/41GM7elc6pL._AC_UL320_.jpg",
    price: 999.0,
    retailPrice: 2999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Basket-Semi-Stitched-P-FB-LH-L8215-Green_Free/dp/B0BCVQ6ZFL/ref=sr_1_142?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-142",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "womens Carlene Fashion Slippers",
    asin: "B07YGNSCLH",
    countReview: 435,
    imgUrl: "https://m.media-amazon.com/images/I/71VPPmVqwVL._AC_UL320_.jpg",
    price: 1157,
    retailPrice: 1499,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/BATA-Carlene-Fashion-Slippers-7-5714911/dp/B07YGNSCLH/ref=sr_1_143?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-143",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Brown",
        dpUrl:
          "/BATA-Carlene-Fashion-Slippers-7-5714911/dp/B07YGNSCLH/ref=cs_sr_dp_1?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-143",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/BATA-Carlene-Fashion-Slippers-7-5714911/dp/B07X4YWZ3C/ref=cs_sr_dp_2?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-143",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Stylish Oxidised Chain Pendant Necklace for Women & Girls",
    asin: "B09GP8Q871",
    countReview: 115,
    imgUrl: "https://m.media-amazon.com/images/I/51NxDOD-b7L._AC_UL320_.jpg",
    price: 190.0,
    retailPrice: 499.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Total-Fashion-Stylish-Oxidised-Necklace/dp/B09GP8Q871/ref=sr_1_144?keywords=women+fashion&qid=1668703226&qu=eyJxc2MiOiI4LjcyIiwicXNhIjoiNy45NSIsInFzcCI6IjMuOTUifQ%3D%3D&sr=8-144",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
];
