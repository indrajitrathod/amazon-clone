export const mobileProductData = [
  {
    productDescription:
      "Redmi A1 (Light Blue, 2GB RAM, 32GB Storage) | Helio A22 | 5000 mAh Battery | 8MP AI Dual Cam | Leather Texture Design | Android 12",
    asin: "B0BBN4DZBD",
    countReview: 3977,
    imgUrl: "https://m.media-amazon.com/images/I/81UT07JsBqL._AC_UY218_.jpg",
    price: 6499,
    retailPrice: 8999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Storage-Battery-Leather-Texture/dp/B0BBN4DZBD/ref=ice_ac_b_dpb?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Light Blue",
        dpUrl:
          "/Redmi-Storage-Battery-Leather-Texture/dp/B0BBN4DZBD/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Redmi-Storage-Battery-Leather-Texture/dp/B0BBN56J5H/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Green",
        dpUrl:
          "/Redmi-Storage-Battery-Leather-Texture/dp/B0BBN3WF7V/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Mobile Phone, Mini Lightweight 5MP Rear Camera Phone Type-C for Work (White)",
    asin: "B0BM92VGM8",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61DWjqDDJ+L._AC_UY218_.jpg",
    price: 9496,
    retailPrice: 16978,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobile-Phone-Lightweight-Camera-Type-C/dp/B0BM92VGM8/ref=sr_1_omk_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: "Get it Saturday, December 10 - Wednesday, December 14",
    variations: [],
  },
  {
    productDescription:
      "Redmi 9A Sport (Coral Green, 2GB RAM, 32GB Storage) | 2GHz Octa-core Helio G25 Processor | 5000 mAh Battery",
    asin: "B09GFLXVH9",
    countReview: 304580,
    imgUrl: "https://m.media-amazon.com/images/I/810KHyQ4WcL._AC_UY218_.jpg",
    price: 6999,
    retailPrice: 8499,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-9A-Sport-Octa-core-Processor/dp/B09GFLXVH9/ref=sr_1_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Coral Green",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B09GFLXVH9/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Carbon Black",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B09GFM8CGS/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Blue",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B09GFP3Z43/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Purple",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B09GFNZT24/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Black",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B08697N43G/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/Redmi-9A-Sport-Octa-core-Processor/dp/B086973W9M/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M13 (Aqua Green, 4GB, 64GB Storage) | 6000mAh Battery | Upto 8GB RAM with RAM Plus",
    asin: "B0B4F2TTTS",
    countReview: 12356,
    imgUrl: "https://m.media-amazon.com/images/I/81-fFXQdPTL._AC_UY218_.jpg",
    price: 11999,
    retailPrice: 14999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2TTTS/ref=sr_1_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Light Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2TTTS/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F3G74S/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2K7N1/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F52B5X/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F1YC3J/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2YTYN/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi 9 Activ (Carbon Black, 4GB RAM, 64GB Storage) | Octa-core Helio G35 | 5000 mAh Battery",
    asin: "B09GFPVD9Y",
    countReview: 304580,
    imgUrl: "https://m.media-amazon.com/images/I/911TJ1CDbLL._AC_UY218_.jpg",
    price: 7999,
    retailPrice: 10999,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Activ-Carbon-Black-Storage/dp/B09GFPVD9Y/ref=sr_1_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Carbon Black",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B09GFPVD9Y/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Coral Green",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B09GFLXVH9/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Blue",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B09GFP3Z43/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Purple",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B09GFNZT24/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Black",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B08697N43G/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/Redmi-Activ-Carbon-Black-Storage/dp/B086973W9M/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "realme narzo 50 (Speed Blue, 4GB RAM+64GB Storage) Helio G96 Processor | 50MP AI Triple Camera | 120Hz Ultra Smooth Display",
    asin: "B09RMQYHLH",
    countReview: 10895,
    imgUrl: "https://m.media-amazon.com/images/I/81gRC3KTeaL._AC_UY218_.jpg",
    price: 12999,
    retailPrice: 15999,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-Storage-Processor-Triple-Display/dp/B09RMQYHLH/ref=sr_1_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Speed Blue",
        dpUrl:
          "/realme-Storage-Processor-Triple-Display/dp/B09RMQYHLH/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Speed Black",
        dpUrl:
          "/realme-Storage-Processor-Triple-Display/dp/B09RMG1M98/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "OPPO A74 5G (Fantastic Purple,6GB RAM,128GB Storage) with No Cost EMI/Additional Exchange Offers",
    asin: "B08VB34KJ1",
    countReview: 30951,
    imgUrl: "https://m.media-amazon.com/images/I/71geVdy6-OS._AC_UY218_.jpg",
    price: 14999,
    retailPrice: 20990,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/OPPO-Fantastic-Purple-128GB-Storage/dp/B08VB34KJ1/ref=sr_1_7?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Fantastic Purple",
        dpUrl:
          "/OPPO-Fantastic-Purple-128GB-Storage/dp/B08VB34KJ1/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Fluid Black",
        dpUrl:
          "/OPPO-Fantastic-Purple-128GB-Storage/dp/B08VB2CMR3/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "OPPO A31 (Mystery Black, 6GB RAM, 128GB Storage) with No Cost EMI/Additional Exchange Offers",
    asin: "B08444S68L",
    countReview: 57556,
    imgUrl: "https://m.media-amazon.com/images/I/71KCwNV6MuL._AC_UY218_.jpg",
    price: 11999,
    retailPrice: 15990,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Oppo-Mystery-Storage-Additional-Exchange/dp/B08444S68L/ref=sr_1_8?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      'realme narzo 50i (Carbon Black, 2GB RAM+32GB Storage) Octa Core Processor | 6.5" inch Large Display',
    asin: "B09FKGDJNC",
    countReview: 27866,
    imgUrl: "https://m.media-amazon.com/images/I/81HJ4pkOsiL._AC_UY218_.jpg",
    price: 7499,
    retailPrice: 7999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-narzo-Carbon-Black-Storage/dp/B09FKGDJNC/ref=sr_1_9?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Carbon Black",
        dpUrl:
          "/realme-narzo-Carbon-Black-Storage/dp/B09FKGDJNC/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mint Green",
        dpUrl:
          "/realme-narzo-Carbon-Black-Storage/dp/B09FKDH6FS/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M32 Prime Edition (Light Blue, 6GB RAM, 128GB)",
    asin: "B0BD3V985M",
    countReview: 53117,
    imgUrl: "https://m.media-amazon.com/images/I/81OC0ojxH6L._AC_UY218_.jpg",
    price: 15499,
    retailPrice: 23000,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Prime-Light-128GB/dp/B0BD3V985M/ref=sr_1_10?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/Samsung-Galaxy-Prime-Light-128GB/dp/B0BD3V985M/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Samsung-Galaxy-Prime-Light-128GB/dp/B096VDG9QV/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Samsung-Galaxy-Prime-Light-128GB/dp/B096VD213D/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Black",
        dpUrl:
          "/Samsung-Galaxy-Prime-Light-128GB/dp/B0BD4QR481/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi 10A (Slate Grey, 4GB RAM, 64GB Storage) | 2 Ghz Octa Core Helio G25 | 5000 mAh Battery | Finger Print Sensor | Upto 5GB RAM with RAM Booster",
    asin: "B09XB7SRQ5",
    countReview: 9067,
    imgUrl: "https://m.media-amazon.com/images/I/71kVEmAMyEL._AC_UY218_.jpg",
    price: 8999,
    retailPrice: 11999,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Storage-Battery-Finger-Booster/dp/B09XB7SRQ5/ref=sr_1_11?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Slate Grey",
        dpUrl:
          "/Redmi-Storage-Battery-Finger-Booster/dp/B09XB7SRQ5/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Charcoal Black",
        dpUrl:
          "/Redmi-Storage-Battery-Finger-Booster/dp/B09XB8GFBQ/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/Redmi-Storage-Battery-Finger-Booster/dp/B09XB7DPW1/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      'realme narzo 50i (Mint Green, 4GB RAM+64GB Storage) Octa Core Processor | 6.5" inch Large Display',
    asin: "B09FKBQ3JM",
    countReview: 27866,
    imgUrl: "https://m.media-amazon.com/images/I/81QqVNKWtML._AC_UY218_.jpg",
    price: 8999,
    retailPrice: 9999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-narzo-Mint-Green-Storage/dp/B09FKBQ3JM/ref=sr_1_12?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Mint Green",
        dpUrl:
          "/realme-narzo-Mint-Green-Storage/dp/B09FKBQ3JM/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Carbon Black",
        dpUrl:
          "/realme-narzo-Mint-Green-Storage/dp/B09FKGDJNC/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi 9 Activ (Coral Green, 4GB RAM, 64GB Storage)| Octa-core Helio G35 | 5000 mAh Battery",
    asin: "B09GFLFMPS",
    countReview: 304580,
    imgUrl: "https://m.media-amazon.com/images/I/91kAtEXPIeL._AC_UY218_.jpg",
    price: 7999,
    retailPrice: 10999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Redmi-Activ-Coral-Green-Storage/dp/B09GFLFMPS/ref=sr_1_13?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Coral Green",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B09GFLFMPS/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Carbon Black",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B09GFM8CGS/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Blue",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B09GFP3Z43/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Purple",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B09GFNZT24/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Black",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B08697N43G/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/Redmi-Activ-Coral-Green-Storage/dp/B086973W9M/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M13 (Aqua Green, 6GB, 128GB Storage) | 6000mAh Battery | Upto 12GB RAM with RAM Plus",
    asin: "B0B4F2XCK3",
    countReview: 12356,
    imgUrl: "https://m.media-amazon.com/images/I/81-fFXQdPTL._AC_UY218_.jpg",
    price: 13999,
    retailPrice: 17999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2XCK3/ref=sr_1_14?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Light Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2XCK3/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F3G74S/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2ZWL3/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F38D6K/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F1YC3J/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-6000mAh-Battery/dp/B0B4F2YTYN/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      'realme narzo 50i (Mint Green, 2GB RAM+32GB Storage) Octa Core Processor | 6.5" inch Large Display',
    asin: "B09FKDH6FS",
    countReview: 27866,
    imgUrl: "https://m.media-amazon.com/images/I/81QqVNKWtML._AC_UY218_.jpg",
    price: 7499,
    retailPrice: 7999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-narzo-Mint-Green-Storage/dp/B09FKDH6FS/ref=sr_1_15?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Mint Green",
        dpUrl:
          "/realme-narzo-Mint-Green-Storage/dp/B09FKDH6FS/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Carbon Black",
        dpUrl:
          "/realme-narzo-Mint-Green-Storage/dp/B09FKGDJNC/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "realme narzo 50A Prime (Flash Black, 4GB RAM+128GB Storage) FHD+ Display | 50MP AI Triple Camera (No Charger Variant)",
    asin: "B09WYXDNWW",
    countReview: 5722,
    imgUrl: "https://m.media-amazon.com/images/I/81IPrkMDqVL._AC_UY218_.jpg",
    price: 12499,
    retailPrice: 14499,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-Storage-Display-Charger-Variant/dp/B09WYXDNWW/ref=sr_1_16?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-16",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/realme-Storage-Display-Charger-Variant/dp/B09WYXDNWW/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/realme-Storage-Display-Charger-Variant/dp/B0BG1NWLY3/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701262&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Smartphone X80 Pro (Cosmic Black, 256 GB) (12 GB RAM)",
    asin: "B0BGS1QQ9X",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/31pUWrJ4y-L._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Smartphone-X80-Pro-Cosmic-Black/dp/B0BGS1QQ9X/ref=sr_1_1?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Smartphone X80 Pro (Cosmic Black, 256 GB) (12 GB RAM)",
    asin: "B0BH8RT51K",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/316BhhIoOPL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Smartphone-X80-Pro-Cosmic-Black/dp/B0BH8RT51K/ref=sr_1_2?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Redmi 9 Activ (Metallic Purple, 4GB RAM, 64GB Storage) | Octa-core Helio G35 | 5000 mAh Battery",
    asin: "B09GFNZT24",
    countReview: 304580,
    imgUrl: "https://m.media-amazon.com/images/I/919IyPIfczL._AC_UY218_.jpg",
    price: 7999,
    retailPrice: 10999,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Redmi-Activ-Metallic-Purple-Storage/dp/B09GFNZT24/ref=sr_1_33?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Metallic Purple",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B09GFNZT24/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Carbon Black",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B09GFM8CGS/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Coral Green",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B09GFLXVH9/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Metallic Blue",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B09GFP3Z43/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Black",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B08697N43G/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/Redmi-Activ-Metallic-Purple-Storage/dp/B086973W9M/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-33",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi Note 11T 5G (Stardust White, 8GB RAM, 128GB ROM) | Dimensity 810 5G | 33W Pro Fast Charging | Charger Included | Additional Exchange Offers| Get 2 Months of YouTube Premium Free!",
    asin: "B09LHX1YFX",
    countReview: 27766,
    imgUrl: "https://m.media-amazon.com/images/I/71C4pfJ7ecL._AC_UY218_.jpg",
    price: 19999,
    retailPrice: 22999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Note-11T-5G-Dimensity/dp/B09LHX1YFX/ref=sr_1_34?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-34",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Stardust White",
        dpUrl:
          "/Redmi-Note-11T-5G-Dimensity/dp/B09LHX1YFX/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aquamarine Blue",
        dpUrl:
          "/Redmi-Note-11T-5G-Dimensity/dp/B09LJ1BF8Q/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Matte Black",
        dpUrl:
          "/Redmi-Note-11T-5G-Dimensity/dp/B09LJ12YNK/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-34",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M13 (Midnight Blue, 4GB, 64GB Storage) | 6000mAh Battery | Upto 8GB RAM with RAM Plus",
    asin: "B0B4F52B5X",
    countReview: 12356,
    imgUrl: "https://m.media-amazon.com/images/I/812YsUxpyfL._AC_UY218_.jpg",
    price: 11999,
    retailPrice: 14999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F52B5X/ref=sr_1_35?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Dark Blue",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F52B5X/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Green",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F3G74S/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F2K7N1/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Green",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F2TTTS/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Blue",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F1YC3J/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust Brown",
        dpUrl:
          "/Samsung-Midnight-Storage-6000mAh-Battery/dp/B0B4F2YTYN/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Tecno Spark 8T (Atlantic Blue, 4GB RAM,64GB Storage)| 50MP AI Camera | 7GB Expandable RAM",
    asin: "B09MKPX6RQ",
    countReview: 6282,
    imgUrl: "https://m.media-amazon.com/images/I/71AWvZMY6LL._AC_UY218_.jpg",
    price: 8699,
    retailPrice: 12999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Tecno-Spark-8T-Expandable-64GB/dp/B09MKPX6RQ/ref=sr_1_36?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Atlantic Blue",
        dpUrl:
          "/Tecno-Spark-8T-Expandable-64GB/dp/B09MKPX6RQ/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Iris Purple",
        dpUrl:
          "/Tecno-Spark-8T-Expandable-64GB/dp/B0B5KNWCN1/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise Cyan",
        dpUrl:
          "/Tecno-Spark-8T-Expandable-64GB/dp/B09MKP344P/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Tahiti Gold",
        dpUrl:
          "/Tecno-Spark-8T-Expandable-64GB/dp/B0B5KMBM7X/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-36",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M32 Prime Edition (Black, 6GB RAM, 128GB)",
    asin: "B0BD4QR481",
    countReview: 53117,
    imgUrl: "https://m.media-amazon.com/images/I/81Jov18YvpL._AC_UY218_.jpg",
    price: 15499,
    retailPrice: 35100,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Prime-Black-128GB/dp/B0BD4QR481/ref=sr_1_37?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-37",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Prime Black",
        dpUrl:
          "/Samsung-Galaxy-Prime-Black-128GB/dp/B0BD4QR481/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Samsung-Galaxy-Prime-Black-128GB/dp/B096VDG9QV/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Samsung-Galaxy-Prime-Black-128GB/dp/B096VD213D/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Samsung-Galaxy-Prime-Black-128GB/dp/B0BD3V985M/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Tecno Pop 5 LTE (Deepsea Luster, 2GB RAM,32GB Storage) | Front Flash | 8MP Dual Camera",
    asin: "B09PRDQN1C",
    countReview: 5515,
    imgUrl: "https://m.media-amazon.com/images/I/51TBsZTSgUL._AC_UY218_.jpg",
    price: 6099,
    retailPrice: 8999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Tecno-Deepsea-Luster-5000mAh-Resistant/dp/B09PRDQN1C/ref=sr_1_38?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Deepsea Luster",
        dpUrl:
          "/Tecno-Deepsea-Luster-5000mAh-Resistant/dp/B09PRDQN1C/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Ice Blue",
        dpUrl:
          "/Tecno-Deepsea-Luster-5000mAh-Resistant/dp/B09PRDHRKV/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise Cyan",
        dpUrl:
          "/Tecno-Deepsea-Luster-5000mAh-Resistant/dp/B09PRFR3WR/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "POCO C31 (Shadow Gray, 64 GB) (4 GB RAM)",
    asin: "B09NY7W8YD",
    countReview: 90,
    imgUrl: "https://m.media-amazon.com/images/I/81GK77jatUL._AC_UY218_.jpg",
    price: 8197,
    retailPrice: 11999,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/POCO-C31-Shadow-Gray-RAM/dp/B09NY7W8YD/ref=sr_1_39?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Samsung Galaxy M32 5G (Sky Blue, 8GB RAM, 128GB Storage) | Dimensity 720 Processor | 5000mAh Battery| Knox Security",
    asin: "B09CGJFY5N",
    countReview: 31725,
    imgUrl: "https://m.media-amazon.com/images/I/71os5DRhuSL._AC_UY218_.jpg",
    price: 20999,
    retailPrice: 25999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Blue-128GB-Storage/dp/B09CGJFY5N/ref=sr_1_40?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-40",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Sky Blue",
        dpUrl:
          "/Samsung-Galaxy-Blue-128GB-Storage/dp/B09CGJFY5N/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Slate Black",
        dpUrl:
          "/Samsung-Galaxy-Blue-128GB-Storage/dp/B09CGM3H3Q/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "JioFi Next 32 GB, 2 GB RAM, Blue Smartphone",
    asin: "B09NFMHNXJ",
    countReview: 737,
    imgUrl: "https://m.media-amazon.com/images/I/61qacJf-mbL._AC_UY218_.jpg",
    price: 4999,
    retailPrice: 7299,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Jio-Phone-Next-Blue-Smartphone/dp/B09NFMHNXJ/ref=sr_1_41?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-41",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [],
  },
  {
    productDescription:
      "Samsung Galaxy M33 5G (Mystique Green, 8GB, 128GB Storage) | 6000mAh Battery | Upto 16GB RAM with RAM Plus | Travel Adapter to be Purchased Separately",
    asin: "B09TWHTBKQ",
    countReview: 18280,
    imgUrl: "https://m.media-amazon.com/images/I/81I3w4J6yjL._AC_UY218_.jpg",
    price: 20499,
    retailPrice: 25999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Mystique-Storage-Purchased-Separately/dp/B09TWHTBKQ/ref=sr_1_42?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-42",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Mystique Green",
        dpUrl:
          "/Samsung-Mystique-Storage-Purchased-Separately/dp/B09TWHTBKQ/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-42",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Deep Ocean Blue",
        dpUrl:
          "/Samsung-Mystique-Storage-Purchased-Separately/dp/B09TWH8YHM/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-42",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Emerald Brown",
        dpUrl:
          "/Samsung-Mystique-Storage-Purchased-Separately/dp/B0B14C482C/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-42",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Samsung Galaxy M13 5G (Aqua Green, 4GB, 64GB Storage) | 5000mAh Battery | Upto 8GB RAM with RAM Plus",
    asin: "B0B4F3G74S",
    countReview: 12356,
    imgUrl: "https://m.media-amazon.com/images/I/81AQybT5k6L._AC_UY218_.jpg",
    price: 13999,
    retailPrice: 16999,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F3G74S/ref=sr_1_43?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Aqua Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F3G74S/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F2K7N1/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F52B5X/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Green",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F2TTTS/ref=cs_sr_dp_4?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Midnight Blue",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F1YC3J/ref=cs_sr_dp_5?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust Brown",
        dpUrl:
          "/Samsung-Galaxy-Storage-5000mAh-Battery/dp/B0B4F2YTYN/ref=cs_sr_dp_6?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Tecno Pop 5 LTE (Turquoise Cyan, 2GB RAM,32GB Storage) | Front Flash | 8MP Dual Camera",
    asin: "B09PRFR3WR",
    countReview: 5515,
    imgUrl: "https://m.media-amazon.com/images/I/81zgpNmQQYL._AC_UY218_.jpg",
    price: 6299,
    retailPrice: 8999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Tecno-Turquoise-5000mAh-Camera-Resistant/dp/B09PRFR3WR/ref=sr_1_44?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-44",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Turquoise Cyan",
        dpUrl:
          "/Tecno-Turquoise-5000mAh-Camera-Resistant/dp/B09PRFR3WR/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Deepsea Luster",
        dpUrl:
          "/Tecno-Turquoise-5000mAh-Camera-Resistant/dp/B09PRDQN1C/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Ice Blue",
        dpUrl:
          "/Tecno-Turquoise-5000mAh-Camera-Resistant/dp/B09PRDHRKV/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi Note 11T 5G (Aquamarine Blue, 6GB RAM, 128GB ROM)| Dimensity 810 5G | 33W Pro Fast Charging | Charger Included | Additional Exchange Offers| Get 2 Months of YouTube Premium Free!",
    asin: "B09LJ116B5",
    countReview: 27766,
    imgUrl: "https://m.media-amazon.com/images/I/71hBRAcpDnL._AC_UY218_.jpg",
    price: 17999,
    retailPrice: 20999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Note-11T-5G-Aquamarine/dp/B09LJ116B5/ref=sr_1_45?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Aquamarine Blue",
        dpUrl:
          "/Redmi-Note-11T-5G-Aquamarine/dp/B09LJ116B5/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust White",
        dpUrl:
          "/Redmi-Note-11T-5G-Aquamarine/dp/B09LHZSMRR/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Matte Black",
        dpUrl:
          "/Redmi-Note-11T-5G-Aquamarine/dp/B09LHYZ3GJ/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Tecno Spark 9 (Sky Mirror, 6GB RAM,128GB Storage) | 11GB Expandable RAM | Helio G37 Gaming Processor",
    asin: "B0B56YRBNT",
    countReview: 2084,
    imgUrl: "https://m.media-amazon.com/images/I/91J1pz6vBcL._AC_UY218_.jpg",
    price: 9249,
    retailPrice: 13499,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Tecno-Spark-Storage-Expandable-Processor/dp/B0B56YRBNT/ref=sr_1_46?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-46",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Sky Mirror",
        dpUrl:
          "/Tecno-Spark-Storage-Expandable-Processor/dp/B0B56YRBNT/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Infinity Black",
        dpUrl:
          "/Tecno-Spark-Storage-Expandable-Processor/dp/B0B56YZT37/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Redmi Note 11 (Horizon Blue, 6GB RAM, 64GB Storage)|90Hz FHD+ AMOLED Display | Qualcomm® Snapdragon™ 680-6nm | 33W Charger Included | Get 2 Months of YouTube Premium Free!",
    asin: "B09QS9CWLV",
    countReview: 45403,
    imgUrl: "https://m.media-amazon.com/images/I/81zLNgcvlaL._AC_UY218_.jpg",
    price: 13499,
    retailPrice: 18999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Redmi-Horizon-Qualcomm%C2%AE-SnapdragonTM-Included/dp/B09QS9CWLV/ref=sr_1_47?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-47",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Horizon Blue",
        dpUrl:
          "/Redmi-Horizon-Qualcomm%C2%AE-SnapdragonTM-Included/dp/B09QS9CWLV/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Space Black",
        dpUrl:
          "/Redmi-Horizon-Qualcomm%C2%AE-SnapdragonTM-Included/dp/B09QS9X16F/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Stardust White",
        dpUrl:
          "/Redmi-Horizon-Qualcomm%C2%AE-SnapdragonTM-Included/dp/B09QS8WD3C/ref=cs_sr_dp_3?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "realme narzo 50i Prime (Dark Blue 4GB RAM+64GB Storage) Octa-core Processor | 5000 mAh Battery",
    asin: "B0BBM7L888",
    countReview: 1218,
    imgUrl: "https://m.media-amazon.com/images/I/81Te0dZU7nL._AC_UY218_.jpg",
    price: 8999,
    retailPrice: 9998,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/realme-Storage-Octa-core-Processor-Battery/dp/B0BBM7L888/ref=sr_1_48?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-48",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Dark Blue",
        dpUrl:
          "/realme-Storage-Octa-core-Processor-Battery/dp/B0BBM7L888/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-48",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mint Green",
        dpUrl:
          "/realme-Storage-Octa-core-Processor-Battery/dp/B0BBMBL64D/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701294&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-48",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Devi Mobiles X80 Pro 5G 256 GB, 12 GB RAM, Cosmic Black, Mobile Phone",
    asin: "B0BMJ22SH6",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31mJQLWL3PL._AC_UY218_.jpg",
    price: 9600.0,
    retailPrice: 10000.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Mobiles-Cosmic-Black-Mobile-Phone/dp/B0BMJ22SH6/ref=sr_1_3?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles X80 256 GB (Urban Blue, 12 GB RAM)",
    asin: "B0BMGXXHNH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41iket8EfYL._AC_UY218_.jpg",
    price: 8200.0,
    retailPrice: 8400.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Devi-Mobiles-X80-Urban-Blue/dp/B0BMGXXHNH/ref=sr_1_4?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "India Gadgets - Glory G1 Pro Android 11 Mobile Phone: 8Gb + 256Gb: 48MP Camera + Night Vision + Thermal Camera + 110dB Loudspeaker: 6200mAh Battery: Waterproof Smartphone (Black)",
    asin: "B09NJVZDG3",
    countReview: 3,
    imgUrl: "https://m.media-amazon.com/images/I/618MUyxf3uL._AC_UY218_.jpg",
    price: 8150,
    retailPrice: 9100.0,
    productRating: "3.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/India-Gadgets-Loudspeaker-Waterproof-Smartphone/dp/B09NJVZDG3/ref=sr_1_5?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it Monday, December 26 - Tuesday, December 27",
    variations: [],
  },
  {
    productDescription: "Akash Mobile. - 12PRO (12GB RAM, 256GB ROM) - 67000",
    asin: "B0B7B59THL",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/21s0Z7R9M0L._AC_UY218_.jpg",
    price: 7300.0,
    retailPrice: 9000.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Akash-Mobile-12PRO-256GB-67000/dp/B0B7B59THL/ref=sr_1_6?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it by Wednesday, November 23",
    variations: [],
  },
  {
    productDescription: "Smartphone X80 (Cosmic Black, 128 GB) (8 GB RAM)",
    asin: "B0BGS2QH85",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31K2V3VaEoL._AC_UY218_.jpg",
    price: 7000.0,
    retailPrice: 9000.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Smartphone-X80-Cosmic-Black-128/dp/B0BGS2QH85/ref=sr_1_7?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      'India Gadgets - Glory G1S 5G Rugged Android Mobile Phone: 8Gb + 128Gb: 48MP + 20MP Night Vision Camera + Thermal Camera: 6.53" FHD+ Display: 5500mAh Battery With 18W Fast Charging: Waterproof Smartphone',
    asin: "B0BM57RW69",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61nOhm0NNvL._AC_UY218_.jpg",
    price: 6950,
    retailPrice: 7790.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/India-Gadgets-Charging-Waterproof-Smartphone/dp/B0BM57RW69/ref=sr_1_8?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it Thursday, December 15 - Friday, December 16",
    variations: [],
  },
  {
    productDescription: "Smartphone X80 (Cosmic Black, 128 GB) (8 GB RAM)",
    asin: "B0BH8QZFB1",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/316BhhIoOPL._AC_UY218_.jpg",
    price: 9700.0,
    retailPrice: 10500.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Smartphone-X80-Cosmic-Black-128/dp/B0BH8QZFB1/ref=sr_1_9?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "iQOO 9 Pro 5G (Legend, 12GB RAM, 256GB Storage) | Snapdragon 8 Gen 1 Mobile Processor | 120W FlashCharge",
    asin: "B07WDKLM55",
    countReview: 1154,
    imgUrl: "https://m.media-amazon.com/images/I/61lWkGr0RiL._AC_UY218_.jpg",
    price: 6990,
    retailPrice: 7999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WDKLM55/ref=sr_1_10?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Legend",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WDKLM55/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Cruise",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WHPVM7N/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "iQOO 9 Pro 5G (Dark Cruise, 12GB RAM, 256GB Storage) | Snapdragon 8 Gen 1 Mobile Processor | 120W FlashCharge",
    asin: "B07WHPVM7N",
    countReview: 1154,
    imgUrl: "https://m.media-amazon.com/images/I/616FcX9aiEL._AC_UY218_.jpg",
    price: 6499,
    retailPrice: 7999,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WHPVM7N/ref=sr_1_11?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Dark Cruise",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WHPVM7N/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Legend",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WDKLM55/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "iQOO 9 Pro 5G (Legend, 8GB RAM, 256GB Storage) | Snapdragon 8 Gen 1 Mobile Processor | 120W FlashCharge",
    asin: "B07WJXKQRN",
    countReview: 1154,
    imgUrl: "https://m.media-amazon.com/images/I/61lWkGr0RiL._AC_UY218_.jpg",
    price: 5999,
    retailPrice: 7499,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WJXKQRN/ref=sr_1_12?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Legend",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WJXKQRN/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Cruise",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WHPVM7N/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "iQOO 9 Pro 5G (Dark Cruise, 8GB RAM, 256GB Storage) | Snapdragon 8 Gen 1 Mobile Processor | 120W FlashCharge",
    asin: "B07WJVZZVY",
    countReview: 1154,
    imgUrl: "https://m.media-amazon.com/images/I/616FcX9aiEL._AC_UY218_.jpg",
    price: 5999,
    retailPrice: 7499,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WJVZZVY/ref=sr_1_13?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Dark Cruise",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WJVZZVY/ref=cs_sr_dp_1?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Legend",
        dpUrl:
          "/iQOO-Storage-Snapdragon-Processor-FlashCharge/dp/B07WDKLM55/ref=cs_sr_dp_2?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Smartphone Reno8 Pro 5G 12 GB RAM, 256 GB ROM-",
    asin: "B0B9SMFQR2",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/214Pgcui3-L._AC_UY218_.jpg",
    price: 12300.0,
    retailPrice: 15700.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Smartphone-Reno8-Pro-RAM-ROM/dp/B0B9SMFQR2/ref=sr_1_14?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      'India Gadgets - Armor 11T 5G Rugged Android 11 Mobile Phone: 8Gb + 256Gb: 48MP Quad Camera with Thermal Imaging Camera: 6.1" HD+ Display: 5200mAh Battery: Waterproof IP68 & IP69K Smartphone (Black)',
    asin: "B096MVQSXJ",
    countReview: 11,
    imgUrl: "https://m.media-amazon.com/images/I/71667uoSE7S._AC_UY218_.jpg",
    price: 14700.0,
    retailPrice: 19000.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/India-Gadgets-Android-Waterproof-Smartphone/dp/B096MVQSXJ/ref=sr_1_15?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "Get it Monday, December 26 - Tuesday, December 27",
    variations: [],
  },
  {
    productDescription: "Smartphone Reno7 5G 256 GB ROM, 12GB RAM",
    asin: "B0B9SPVKQ2",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/312XE6XwPgL._AC_UY218_.jpg",
    price: 19000.0,
    retailPrice: 21000.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Smartphone-Reno7-256-ROM-12GB/dp/B0B9SPVKQ2/ref=sr_1_16?keywords=mobiles&qid=1668701112&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-16",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles V23 Pro 5G Stardust Black, 8GB RAM, 128GB Storage",
    asin: "B0BMGYS9GG",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51TcJs6lvqL._AC_UY218_.jpg",
    price: 5500.0,
    retailPrice: 6500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Stardust-Black-128GB-Storage/dp/B0BMGYS9GG/ref=sr_1_1?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles X80 Pro 5G 256 GB, 12 GB RAM, Cosmic Black, Mobile Phone",
    asin: "B0BMJ22SH6",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31mJQLWL3PL._AC_UY218_.jpg",
    price: 9600.0,
    retailPrice: 10000.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Cosmic-Black-Mobile-Phone/dp/B0BMJ22SH6/ref=sr_1_2?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles X80 256 GB (Urban Blue, 12 GB RAM)",
    asin: "B0BMGXXHNH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41iket8EfYL._AC_UY218_.jpg",
    price: 8200.0,
    retailPrice: 8400.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-X80-Urban-Blue/dp/B0BMGXXHNH/ref=sr_1_3?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles Y75 5G (Glowing Galaxy, 8GB RAM, 128GB ROM)",
    asin: "B0BMGWYB1Z",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51l3-C9zVaL._AC_UY218_.jpg",
    price: 3500.0,
    retailPrice: 4000.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Glowing-Galaxy-128GB/dp/B0BMGWYB1Z/ref=sr_1_4?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles V23 Pro 5G (Stardust Black, 8GB RAM, 128GB Storage)",
    asin: "B0BMGXVJ1K",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51TcJs6lvqL._AC_UY218_.jpg",
    price: 5500.0,
    retailPrice: 6500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Stardust-Black-128GB-Storage/dp/B0BMGXVJ1K/ref=sr_1_5?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles V23 5G 256 GB, 12 GB RAM, Sunshine Gold, Mobile Phone",
    asin: "B0BMGV3S5F",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/21N+9qHSywL._AC_UY218_.jpg",
    price: 5700.0,
    retailPrice: 6100.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Sunshine-Mobile-Phone/dp/B0BMGV3S5F/ref=sr_1_6?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles V25 Pro 128 GB (Pure Black, 8 GB RAM)",
    asin: "B0BMGV4RS9",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31mJQLWL3PL._AC_UY218_.jpg",
    price: 5100.0,
    retailPrice: 5500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-V25-Pure-Black/dp/B0BMGV4RS9/ref=sr_1_7?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles V23e 5G 128 GB, 8 GB RAM, Sunshine Gold",
    asin: "B0BMGWTPTF",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51ZpiwFL6LL._AC_UY218_.jpg",
    price: 4100.0,
    retailPrice: 4800.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-V23e-Sunshine-Gold/dp/B0BMGWTPTF/ref=sr_1_8?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles V23 Pro 5G (Stardust Black, 8GB RAM, 128GB Storage)",
    asin: "B0BMGX4TBF",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51FpU1+T-kL._AC_UY218_.jpg",
    price: 5500.0,
    retailPrice: 6500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Stardust-Black-128GB-Storage/dp/B0BMGX4TBF/ref=sr_1_9?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles V23 5G 128 GB, 8 GB RAM, Stardust Black",
    asin: "B0BMGS4WRN",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51FpU1+T-kL._AC_UY218_.jpg",
    price: 4500.0,
    retailPrice: 5100.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-V23-Stardust-Black/dp/B0BMGS4WRN/ref=sr_1_10?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles V23 Pro 5G (Stardust Black, 12GB RAM, 256GB Storage)",
    asin: "B0BMGS4R3Z",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/316zbgVsAyL._AC_UY218_.jpg",
    price: 3500.0,
    retailPrice: 4500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Stardust-Black-256GB-Storage/dp/B0BMGS4R3Z/ref=sr_1_11?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles Y35 (Agate Black, 8GB RAM, 128GB Storage)",
    asin: "B0BMGPSST4",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51i-TuLJJOL._AC_UY218_.jpg",
    price: 3600.0,
    retailPrice: 4200.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mobiles-Agate-Black-128GB-Storage/dp/B0BMGPSST4/ref=sr_1_12?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles Y23 Pro 256 GB, 12 GB RAM, Sunshine Gold, Mobile Phone",
    asin: "B0BMGQZF1B",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51TcJs6lvqL._AC_UY218_.jpg",
    price: 4500.0,
    retailPrice: 5500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Sunshine-Mobile-Phone/dp/B0BMGQZF1B/ref=sr_1_13?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription:
      "Devi Mobiles Y75 5G (Glowing Galaxy, 8GB RAM, 128GB ROM)",
    asin: "B0BMGR1PHJ",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51Bi6F7FORL._AC_UY218_.jpg",
    price: 3600.0,
    retailPrice: 4200.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Glowing-Galaxy-128GB/dp/B0BMGR1PHJ/ref=sr_1_14?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles Y22 (6GB RAM + 128GB)",
    asin: "B0BMGRGPHH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51511n6RLGL._AC_UY218_.jpg",
    price: 19000.0,
    retailPrice: 25000.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Y22-6GB-128GB/dp/B0BMGRGPHH/ref=sr_1_15?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
  {
    productDescription: "Devi Mobiles Y75 (Dancing Waves, 8GB RAM, 128GB ROM)",
    asin: "B0BMGS9BTD",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51Bi6F7FORL._AC_UY218_.jpg",
    price: 3600.0,
    retailPrice: 4200.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Devi-Mobiles-Dancing-Waves-128GB/dp/B0BMGS9BTD/ref=sr_1_16?keywords=mobiles&qid=1668700841&qu=eyJxc2MiOiI4LjQ2IiwicXNhIjoiOC4wOSIsInFzcCI6IjYuNjMifQ%3D%3D&sr=8-16",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Friday, November 25",
    variations: [],
  },
];
