const bestSellersProductData = [
  {
    productDescription:
      "PINPOINT SELLER Women and Girl's Stylish Rayon Solid/Printed Kaftan",
    asin: "B09RWXGJB4",
    countReview: 77,
    imgUrl: "https://m.media-amazon.com/images/I/81D+H1YZhgL._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 999.0,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWXGJB4/ref=sr_1_49?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWXGJB4/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWX6MJ6/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWWM6W4/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWX7Q7C/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWX28FL/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Stylish-Printed-Kaftan/dp/B09RWX9SVZ/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-49",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "STRIFF Adjustable Laptop Stand Patented Riser Ventilated Portable Foldable Compatible with MacBook Notebook Tablet Tray Desk Table Book with Free Phone Stand(Black)",
    asin: "B07XCM6T4N",
    countReview: 22208,
    imgUrl: "https://m.media-amazon.com/images/I/71Zf9uUp+GL._AC_UL320_.jpg",
    price: 371.0,
    retailPrice: 1.499,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/computers/1375314031/ref=sr_bs_1_1375314031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Prime Seller Women's Soft Litchi Silk banarasi jacquard Saree With Matching Blouse Piece(Black and Red)",
    asin: "B08ZV4JXYH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81YSAGJsKyL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1.599,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Prime-Seller-banarasi-jacquard-Matching/dp/B08ZV4JXYH/ref=sr_1_51?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-51",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Printed Tshirt (WE0400_White_L)",
    asin: "B0BG3S9JYT",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/412w--OEwYL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1.999,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Printed-WE0400_White_L/dp/B0BG3S9JYT/ref=sr_1_52?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-52",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Skechers Mens D'Lites Arch Fit - Better Sel Sneaker",
    asin: "B09X23MFTQ",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/71-33u9IEfL._AC_UL320_.jpg",
    price: 5.524,
    retailPrice: 8.499,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Skechers-Black-Gray-Mens-Casual-Shoes-237311-BKGY-DLites-Arch/dp/B09X23MFTQ/ref=sr_1_53?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-53",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "BLACK/GRAY",
        dpUrl:
          "/Skechers-Black-Gray-Mens-Casual-Shoes-237311-BKGY-DLites-Arch/dp/B09X23MFTQ/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NATURAL",
        dpUrl:
          "/Skechers-Black-Gray-Mens-Casual-Shoes-237311-BKGY-DLites-Arch/dp/B09XXQ2JSF/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NAVY/ORANGE",
        dpUrl:
          "/Skechers-Black-Gray-Mens-Casual-Shoes-237311-BKGY-DLites-Arch/dp/B09XXRG22R/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NVY/ORNG",
        dpUrl:
          "/Skechers-Black-Gray-Mens-Casual-Shoes-237311-BKGY-DLites-Arch/dp/B09XXTJ6BN/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Cable World Baby Kids Water Mat Toys Inflatable Tummy Time Leakproof Water Mat, Fun Activity Play Center Indoor and Outdoor Water Mat for Baby Random Design",
    asin: "B0B97KSWWS",
    countReview: 223,
    imgUrl: "https://m.media-amazon.com/images/I/71qL5Q3LbnL._AC_UL320_.jpg",
    price: 249.0,
    retailPrice: 1.499,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Cable-World-Inflatable-Leakproof-Activity/dp/B0B97KSWWS/ref=sr_1_54?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-54",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Instagram Trandy Custom Regular Fit Tshirt",
    asin: "B0B3JMFVPT",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31zfDDXnaCL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Instagram-Regular-White_M/dp/B0B3JMFVPT/ref=sr_1_55?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-55",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "The Sellers",
    asin: "B081876HYJ",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/71OiYnHkmfL._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "2.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sellers-Michelle-Brown-ebook/dp/B081876HYJ/ref=sr_1_56?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-56",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Cotton Digital Printed Short Kurti for Women",
    asin: "B0B4GDPKTH",
    countReview: 22,
    imgUrl: "https://m.media-amazon.com/images/I/81U1rOnKgKL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 799.0,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GDPKTH/ref=sr_1_57?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-57",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue And Red",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GDPKTH/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-57",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GCBMZL/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-57",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Blue",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GBX7NZ/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-57",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Finalize 3D Manual Roller Face Body Massager With And Anti Aging 100% Natural Jade Stone Roller Msagger With Gua Sha Tools for Face and Body Massager",
    asin: "B0B7Y2WT2X",
    countReview: 39,
    imgUrl: "https://m.media-amazon.com/images/I/41TCW3p0jQL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1.999,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Finalize-Manual-Massager-Natural-Msagger/dp/B0B7Y2WT2X/ref=sr_1_58?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-58",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Volatility Carrom Board (32 inch Large with Stand ,Coin Striker Powder)",
    asin: "B0BC5FJ367",
    countReview: 11,
    imgUrl: "https://m.media-amazon.com/images/I/71FTjWbLDzL._AC_UL320_.jpg",
    price: 2.499,
    retailPrice: 6.999,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Volatility-Carrom-Board-Striker-Powder/dp/B0BC5FJ367/ref=sr_1_59?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-59",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER 2 in 1 Moon Lamp Cool Mist Humidifiers Essential Oil Diffuser Aroma Air Humidifier with Led Night Light Colorful Change for Car, Office, Babies, humidifiers for Home, air humidifier for Room",
    asin: "B09C3Q2PBY",
    countReview: 473,
    imgUrl: "https://m.media-amazon.com/images/I/61Zyh8HAm3S._AC_UL320_.jpg",
    price: 649.0,
    retailPrice: 999.0,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Humidifiers-Humidifier-humidifiers/dp/B09C3Q2PBY/ref=sr_1_60?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-60",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Blue Rayon Two Tone Embroidered Short Kurti for Women",
    asin: "B0BC1P2FZC",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71T7Y+YqQ3L._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 799.0,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1P2FZC/ref=sr_1_61?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-61",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1P2FZC/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-61",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1LPBCQ/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-61",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1MJWFX/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-61",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1K5X8M/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-61",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Sparx Mens Sx0677g Running",
    asin: "B09BZNPS98",
    countReview: 2849,
    imgUrl: "https://m.media-amazon.com/images/I/71JxwvpS7iL._AC_UL320_.jpg",
    price: 959.0,
    retailPrice: 1.249,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09BZNPS98/ref=sr_1_62?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-62",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "T. BLUE BLACK",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09BZNPS98/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK RED",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B098FGM8DJ/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "D. GREY NEON GREEN",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B08ZNHY9M5/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "N. BLUE NEON ORANGE",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09879NLW2/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-62",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "MACPLUS Makeup Sponge Set Beauty Blender with Egg Case, Soft Sponge For Liquid Foundation, Creams, and Powders Latex Free Wet and Dry Makeup (Multicolor 4 Big + 3 Mini -7 Pcs set)",
    asin: "B0B4FY1HZ2",
    countReview: 187,
    imgUrl: "https://m.media-amazon.com/images/I/51SufCj2KzL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 399.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/MACPLUS-Blender-Foundation-Powders-Multicolor/dp/B0B4FY1HZ2/ref=sr_1_63?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-63",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Plastic Water Bottle, 1000ml, Set of 6, White",
    asin: "B092VX4JKZ",
    countReview: 23,
    imgUrl: "https://m.media-amazon.com/images/I/81+W1Lk4ttS._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 999.0,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Plastic-Bottle-1000ml/dp/B092VX4JKZ/ref=sr_1_64?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-64",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Pink Cotton Printed Noodle Strap Short Dress for Women",
    asin: "B0BCZ1YKWV",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81lpspCTsXL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 899.0,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ1YKWV/ref=sr_1_65?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-65",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ1YKWV/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ2M63G/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off-White",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZH9Q8T/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ2PJ87/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "MB Seller Manual Hand Press Water Dispenser for 20 Litre Bottle | Portable Dispenser Pump for Water Can/Bottle",
    asin: "B0BGFNKGVH",
    countReview: 9,
    imgUrl: "https://m.media-amazon.com/images/I/41PmvjPMgKL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 999.0,
    productRating: "2.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/MB-Seller-Manual-Dispenser-Portable/dp/B0BGFNKGVH/ref=sr_1_66?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-66",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "CLASSIC CHART PATTERN SHEET - 6 SHEET(A5 SIZE)",
    asin: "B0BFWHZ9V5",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/91-P7SYLLNL._AC_UL320_.jpg",
    price: 239.0,
    retailPrice: 499.0,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/CLASSIC-CHART-PATTERN-SHEET-SIZE/dp/B0BFWHZ9V5/ref=sr_1_67?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-67",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription: "Le sel: Essai sur la chimie (French Edition)",
    asin: "B00WK14TYA",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/A1RHCwXonLL._AC_UL320_.jpg",
    price: 103.95,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/sel-Essai-sur-chimie-French-ebook/dp/B00WK14TYA/ref=sr_1_68?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-68",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Catron Stainless Steel Spice Grinder Glass, Steel Squeeze Mill,Pepper Grinder or Salt Shaker Manual Shaker, Spice Storage with Adjustable Coarseness Setting (Clear, Pack of 1)",
    asin: "B0B7146T5L",
    countReview: 116,
    imgUrl: "https://m.media-amazon.com/images/I/511NXpXM5TL._AC_UL320_.jpg",
    price: 249.0,
    retailPrice: 999.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/kitchen/1380010031/ref=sr_bs_20_1380010031_1",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Women's Rayon Checkered Straight Kurti",
    asin: "B09RWPC4FR",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/41gjB18BjVL._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 999.0,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPC4FR/ref=sr_1_70?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-70",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Green",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPC4FR/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-70",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWQ1QR6/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-70",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPLPDR/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-70",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPPDZ1/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-70",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Felt Fetish Sanitary Napkin Pad Holder - Owl",
    asin: "B078Q74KTD",
    countReview: 67,
    imgUrl: "https://m.media-amazon.com/images/I/713HvtJOBPL._AC_UL320_.jpg",
    price: 310.0,
    retailPrice: 0.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Felt-Fetish-Sanitary-Napkin-Holder/dp/B078Q74KTD/ref=sr_1_71?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-71",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trendy Custom Regular Fit Printed Tshirt (WE0354_White_L)",
    asin: "B0BD893JHR",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/31XtOC-aLaL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1.999,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Printed-WE0354_White_L/dp/B0BD893JHR/ref=sr_1_72?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-72",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "TOUGH LEE 5 in 1 Cleaning Pen for Airpods Pro 1 2 3, Gadget Cleaner & Cleaning Kit Brush Set || Suitable for Earbuds, Mobile, Laptop, Keyboard, Airpod Charging Case, Headphone & Earphone",
    asin: "B0B92WFYRN",
    countReview: 117,
    imgUrl: "https://m.media-amazon.com/images/I/41RwhlO6YoL._AC_UL320_.jpg",
    price: 189.0,
    retailPrice: 999.0,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/electronics/1389029031/ref=sr_bs_24_1389029031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Pink Cotton Digital Printed Short Dress for Women",
    asin: "B0B4WDPK8Q",
    countReview: 22,
    imgUrl: "https://m.media-amazon.com/images/I/81vcr9XeJmL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 799.0,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4WDPK8Q/ref=sr_1_74?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-74",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Beige",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4WDPK8Q/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4WDSSSX/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off White",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4WF2BBX/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Sparx Mens Casual Stripped Sneakers",
    asin: "B077MZ785Q",
    countReview: 43685,
    imgUrl: "https://m.media-amazon.com/images/I/51g6S2XoAcL._AC_UL320_.jpg",
    price: 707.0,
    retailPrice: 899.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/shoes/1983577031/ref=sr_bs_26_1983577031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "WHITE BLACK",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B077MZ785Q/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black white",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B077N6PY6M/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NAVY WHITE",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B07WHSR8Z1/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off white Black",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B0B4KD3BN1/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White Black",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B077N7BL4M/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue White",
        dpUrl:
          "/Sparx-Sneakers-9-India-43-33-SD0323G/dp/B08L92V3RC/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-75",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Think and Grow Rich: The Landmark Bestseller Now Revised and Updated for the 21st Century (Think and Grow Rich Series) & Penguin Random House The Power Of Positive Thinking",
    asin: "B0BLVZVMYP",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61K5tnw09JL._AC_UL320_.jpg",
    price: 566.0,
    retailPrice: 798.0,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Think-Grow-Rich-Landmark-Bestseller/dp/B0BLVZVMYP/ref=sr_1_76?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-76",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Lapster 5-in-1 Multi-Function Laptop Cleaning Brush/Keyboard Cleaning kit/Gadget Cleaning kit Gap Duster Key-Cap Puller for Laptop, Keyboard and Earphones(Multi Colour)",
    asin: "B0BBW16VH9",
    countReview: 217,
    imgUrl: "https://m.media-amazon.com/images/I/51+UotA4ExL._AC_UL320_.jpg",
    price: 179.0,
    retailPrice: 999.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Lapster-Multi-Function-Cleaning-Keyboard-Earphones/dp/B0BBW16VH9/ref=sr_1_77?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-77",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Women's Cotton Blend Printed Straight Kurti",
    asin: "B09RWQ2LW8",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/31YOhkFHIbL._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 999.0,
    productRating: "2.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWQ2LW8/ref=sr_1_78?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWQ2LW8/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWNRRNQ/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWP7JYF/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWNTNS2/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWQ3LFH/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Printed-Straight/dp/B09RWQ6H4X/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-78",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "JM SELLER Set of 2 Rolls Printing Antibacterial Cabinet Plastic Foam Household Wardrobe Moisture Drawer Pad Waterproof Non-Slip Paper Kitchen Cupboard Liners Roll Mat, 30x300 cm",
    asin: "B09GYKWG93",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/61irH6TsY5L._AC_UL320_.jpg",
    price: 269.0,
    retailPrice: 699.0,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Antibacterial-Household-Waterproof/dp/B09GYKWG93/ref=sr_1_79?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-79",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Volatility Carrom Board (32 inch Large with Coin Striker Powder)",
    asin: "B0BC5F4NMJ",
    countReview: 40,
    imgUrl: "https://m.media-amazon.com/images/I/7198JnZ28mL._AC_UL320_.jpg",
    price: 1.497,
    retailPrice: 4.999,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Volatility-Carrom-Board-Striker-Powder/dp/B0BC5F4NMJ/ref=sr_1_80?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-80",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription: "Le Sel (French Edition)",
    asin: "B07CPK826D",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81qQTb8J5nL._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sel-French-David-Seznec-ebook/dp/B07CPK826D/ref=sr_1_81?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-81",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Damon Salvatore Printed Casual Tshirt (White)",
    asin: "B0B82HJBQQ",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41g6Y7ERsOL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 2.549,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Salvatore-Printed/dp/B0B82HJBQQ/ref=sr_1_82?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-82",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Campus Men's Maxico Running Shoes",
    asin: "B08WD1HLM2",
    countReview: 15392,
    imgUrl: "https://m.media-amazon.com/images/I/71UDBmG9kbL._AC_UL320_.jpg",
    price: 894.0,
    retailPrice: 1.299,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Campus-Maxico-Running-Shoes-India/dp/B08WD1HLM2/ref=sr_1_83?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "BLU/RED",
        dpUrl:
          "/Campus-Maxico-Running-Shoes-India/dp/B08WD1HLM2/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLU/LMN",
        dpUrl:
          "/Campus-Maxico-Running-Shoes-India/dp/B084JPY22C/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLU/SKY",
        dpUrl:
          "/Campus-Maxico-Running-Shoes-India/dp/B084JQP8G8/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "CH.GRY/T.BLU",
        dpUrl:
          "/Campus-Maxico-Running-Shoes-India/dp/B0855Z3D73/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "MOD.BLU/BLU",
        dpUrl:
          "/Campus-Maxico-Running-Shoes-India/dp/B0855ZDZCX/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-83",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "beatXP Kitchen Scale Multipurpose Portable Electronic Digital Weighing Scale | Weight Machine With Back light LCD Display | White |10 kg | 2 Year Warranty |",
    asin: "B0B61DSF17",
    countReview: 1026,
    imgUrl: "https://m.media-amazon.com/images/I/61Is7sICRJL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1.999,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/beatXP-Multipurpose-Portable-Electronic-Weighing/dp/B0B61DSF17/ref=sr_1_84?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-84",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "The Seller Rayon Frill Printed top for Office Wear, Casual Wear,Top for Women/Girls Top",
    asin: "B0BHWX89B8",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41Lau76iCqL._AC_UL320_.jpg",
    price: 379.0,
    retailPrice: 999.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Seller-Rayon-Printed-Office-Casual/dp/B0BHWX89B8/ref=sr_1_85?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-85",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "The Inheritance Games & One Of Us Is Next: The sequel to the international bestseller One Of Us Is Lying & The Hawthorne Legacy: TikTok Made Me Buy It (The Inheritance Games)",
    asin: "B0BM9BF374",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61zWxdOntHL._AC_UL320_.jpg",
    price: 975.0,
    retailPrice: 1.299,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Inheritance-Games-One-Next-international/dp/B0BM9BF374/ref=sr_1_86?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-86",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Wolpin Microfiber Cleaning Cloths, 5 Pcs 25 x 25 cms Multi-Colour | Highly Absorbent, Lint and Streak Free, Multi -Purpose Wash Cloth for Kitchen, Car, Window, Stainless Steel, Silverware, 300 GSM",
    asin: "B09SLP445P",
    countReview: 830,
    imgUrl: "https://m.media-amazon.com/images/I/71bATUHpN3L._AC_UL320_.jpg",
    price: 179.0,
    retailPrice: 799.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Microfiber-Multi-Colour-Absorbent-Stainless-Silverware/dp/B09SLP445P/ref=sr_1_87?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-87",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Mr Bean Printed Tshirt (White)",
    asin: "B0B5DX8P63",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41NwiOh-OyL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Printed-White_XL/dp/B0B5DX8P63/ref=sr_1_88?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-88",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Zovent Telescopic Car Wax Drag Nano Fiber Car Wash Brush Car Dusting Tool Car Mop Wax Dash Duster Exterior Interior Cleaning Kit Car Duster 1 Pcs Set",
    asin: "B0B5715XQP",
    countReview: 169,
    imgUrl: "https://m.media-amazon.com/images/I/61br4gtuaOS._AC_UL320_.jpg",
    price: 259.0,
    retailPrice: 999.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Zovent-Telescopic-Brush-Exterior-Cleaning/dp/B0B5715XQP/ref=sr_1_89?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-89",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Ivaan - Thor's Hammer Keychain Metal Alloy Marvel Superhero Movie Key India Seller (Silver)",
    asin: "B07C1WSMDG",
    countReview: 162,
    imgUrl: "https://m.media-amazon.com/images/I/81E6Be2SqFL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 899.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/Hammer-Keychain-Marvel-Superhero-Seller/dp/B07C1WSMDG/ref=sr_1_90?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-90",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "ASIAN Men's Wonder-13 Sports Running Shoes",
    asin: "B01MRN1BY4",
    countReview: 80102,
    imgUrl: "https://m.media-amazon.com/images/I/61utX8kBDlL._AC_UL320_.jpg",
    price: 450.0,
    retailPrice: 699.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/shoes/ref=sr_bs_42_shoes_1",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "1Green",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B01MRN1BY4/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B07N4R7WKR/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B07N4RK5NP/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B07N4R6545/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "2Grey",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B07DLBZ3MK/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Asian-shoes-Sports-Firozi-Indian/dp/B07BN1XQR8/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-91",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "ORJILO Super Soft Microfiber Car Duster Exterior with Extendable Handle, Car Brush Duster for Car Cleaning Dusting - Grey",
    asin: "B0B5XHFGK8",
    countReview: 678,
    imgUrl: "https://m.media-amazon.com/images/I/61E16mQiRCS._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/ORJILO-Microfiber-Exterior-Extendable-Cleaning/dp/B0B5XHFGK8/ref=sr_1_92?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-92",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Tales From the Lands of Nuts and Grapes (Spanish and Portuguese Folklore)",
    asin: "B00LZSW8AQ",
    countReview: 5,
    imgUrl: "https://m.media-amazon.com/images/I/91uxRz9lTuL._AC_UL320_.jpg",
    price: 503.5,
    retailPrice: 0.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Tales-Grapes-Spanish-Portuguese-Folklore-ebook/dp/B00LZSW8AQ/ref=sr_1_93?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-93",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Hardcover",
        dpUrl:
          "/Tales-Grapes-Spanish-Portuguese-Folklore/dp/1356047319/ref=sr_1_93?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Tales-Lands-Nuts-Grapes-Portuguese/dp/144370654X/ref=sr_1_93?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Seller Women's Cotton Anarkali Feeding/Maternity Kurti",
    asin: "B0BHZ1MP6Q",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51OAVO-RTyL._AC_UL320_.jpg",
    price: 569.0,
    retailPrice: 1.199,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Seller-Maternity-Pregnancy-Comfortable-Pregnant/dp/B0BHZ1MP6Q/ref=sr_1_94?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-94",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "The Hawthorne Legacy: TikTok Made Me Buy It (The Inheritance Games) & One Of Us Is Next: The sequel to the international bestseller One Of Us Is Lying",
    asin: "B0BLVVP1YT",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61o--Gp2K-L._AC_UL320_.jpg",
    price: 583.0,
    retailPrice: 849.0,
    productRating: "0.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Hawthorne-Legacy-Inheritance-international-bestseller/dp/B0BLVVP1YT/ref=sr_1_95?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-95",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Mamaearth Rice Face Wash With Rice Water & Niacinamide for Glass Skin - 100 ml",
    asin: "B09Z65JJ69",
    countReview: 843,
    imgUrl: "https://m.media-amazon.com/images/I/51COhKQY9iL._AC_UL320_.jpg",
    price: 233.0,
    retailPrice: 259.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mamaearth-Rice-Water-Niacinamide-Glass/dp/B09Z65JJ69/ref=sr_1_96?keywords=best+sellers&qid=1668703584&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-96",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Hair, Scalp Massage & Bathing Brush For Cleaning Body | Silicon Wash Scrubber, Cleaner & Massager For Shampoo, Soap | Softener Skin Care Brushes For Shower, Bathroom, Men, Women",
    asin: "B08Y3BHY9P",
    countReview: 180,
    imgUrl: "https://m.media-amazon.com/images/I/81Y3TjETsWL._AC_UL320_.jpg",
    price: 189.0,
    retailPrice: 449.0,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Cleaning-Scrubber-Massager/dp/B08Y3BHY9P/ref=sr_1_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "The Seller: A Dark Romance (Loki Renard's Standalone Dark Contemporary Romances)",
    asin: "B07TYK25XP",
    countReview: 30,
    imgUrl: "https://m.media-amazon.com/images/I/81LUbpvlpJL._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Seller-Dark-Romance-Loki-Renard-ebook/dp/B07TYK25XP/ref=sr_1_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Silicone Scrubbing Gloves, Non-Slip, Dishwashing and Pet Grooming, Magic Latex Gloves for Household Cleaning Great for Protecting Hands in Dishwashing (Multicolor)(1 Pair)",
    asin: "B08PZ3F89M",
    countReview: 35,
    imgUrl: "https://m.media-amazon.com/images/I/61clSRXolwL._AC_UL320_.jpg",
    price: 239.0,
    retailPrice: 0.0,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/JM-SELLER-Dishwashing-Protecting-Multicolor/dp/B08PZ3F89M/ref=sr_1_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Sparx mens Ss0101g Sandals",
    asin: "B00IZ97K3I",
    countReview: 22932,
    imgUrl: "https://m.media-amazon.com/images/I/61BTn+U4i0L._AC_UL320_.jpg",
    price: 566.0,
    retailPrice: 849.0,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/shoes/1983576031/ref=sr_bs_3_1983576031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black-Grey",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B00IZ97K3I/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "CAMEL",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B00IZ97MRW/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Camel",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B08RNRLY9N/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NAVY BLUE",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B00IZ97PHY/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B08RNPFVW2/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Sparx-Black-Athletic-Outdoor-Sandals/dp/B08RNLTQ2W/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Revive Heat Seal Mini Food Sealer-Impulse Machine for Sealing Plastic Bags Packaging",
    asin: "B09T6LKX62",
    countReview: 22,
    imgUrl: "https://m.media-amazon.com/images/I/41aVSmdLvnL._AC_UL320_.jpg",
    price: 99.0,
    retailPrice: 349.0,
    productRating: "2.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Heat-Mini-Sealer-Impulse-Sealing-Packaging/dp/B09T6LKX62/ref=sr_1_5?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it Saturday, November 19 - Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Printed Charlie Chaplin Tshirt (White)",
    asin: "B0B5DXQMZG",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41XqVRR4YkL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1.599,
    productRating: null,
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Printed-White_Medium/dp/B0B5DXQMZG/ref=sr_1_6?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "MOHITAM Sewing Machine Electric Handheld Sewing Machine Mini Handy Stitch Portable Needlework Cordless Handmade DIY Tool Clothes Portable (White-Y1) (WHITE-6)",
    asin: "B0BGZQ57LQ",
    countReview: 19,
    imgUrl: "https://m.media-amazon.com/images/I/51DbwR5f9SL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 999.0,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/kitchen/2083428031/ref=sr_bs_6_2083428031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Handheld Garment Steamer Iron Fast Heat-up Portable Family Fabric Steam Brush Handy Vapor Facial Steamer for Home and Travel (Colour May Vary)",
    asin: "B07PSL67D8",
    countReview: 12,
    imgUrl: "https://m.media-amazon.com/images/I/51sauR3gjVL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 999.0,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Handheld-Garment-Portable/dp/B07PSL67D8/ref=sr_1_8?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Green Cotton Embroidered Long Kurti for Women",
    asin: "B0B7JJZQ92",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/61NmUfySQYL._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 699.0,
    productRating: "5.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Yellow-Embroidered-Kurti/dp/B0B7JJZQ92/ref=sr_1_9?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Yellow-Embroidered-Kurti/dp/B0B7JJZQ92/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/PINPOINT-SELLER-Yellow-Embroidered-Kurti/dp/B0B7JJP3W8/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/PINPOINT-SELLER-Yellow-Embroidered-Kurti/dp/B0B7JQ642B/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Yellow-Embroidered-Kurti/dp/B0B7JHVK4N/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "CHAMRIA COMBO OF BEST SELLERS -KARORPATI GOLI 200GMS, MANGOLA 200GMS & MUNAKKA PACHAK 200GMS",
    asin: "B08YK1LPSQ",
    countReview: 15,
    imgUrl: "https://m.media-amazon.com/images/I/81DVHXOgXJS._AC_UL320_.jpg",
    price: 350.0,
    retailPrice: 385.0,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/CHAMRIA-SELLERS-KARORPATI-MANGOLA-MUNAKKA/dp/B08YK1LPSQ/ref=sr_1_10?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      '2023 Pocket Planner: Two-Year-Plus Monthly Planner, Pocket Calendar: August 2022 - December 2024, 3.5" x 6.5" - Fish in Indigo (CHECKBOOK2 YEAR POCKET PLANNER)',
    asin: "153191702X",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/81iotaFej3L._AC_UL320_.jpg",
    price: 487.0,
    retailPrice: 573.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/2023-Pocket-Planner-Two-Year-Plus-CHECKBOOK2/dp/153191702X/ref=sr_1_11?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "Get it Tuesday, November 29 - Friday, December 2",
    variations: [],
  },
  {
    productDescription:
      "RADIANT Door Bottom Sealing Strip Guard for Home (Size-36 inch) (Pack of 1) (Brown)",
    asin: "B08TQQBG7C",
    countReview: 17750,
    imgUrl: "https://m.media-amazon.com/images/I/419N2-QRjkL._AC_UL320_.jpg",
    price: 83.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/gp/bestsellers/home-improvement/9835428031/ref=sr_bs_11_9835428031_1",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Hair Care Thermal Head Spa Cap Treatment with Beauty Steamer Nourishing Heating Cap, Spa Cap For Hair, Spa Cap Steamer For Women color pink",
    asin: "B08PZCC248",
    countReview: 106,
    imgUrl: "https://m.media-amazon.com/images/I/41Dy4nVUvZL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/JM-SELLER-Thermal-Treatment-Nourishing/dp/B08PZCC248/ref=sr_1_13?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "SALCKO Sel Roti Maker, Traditional Nepali Soli Machine - Silver",
    asin: "B08WX2YVJV",
    countReview: 45,
    imgUrl: "https://m.media-amazon.com/images/I/51MFymHGtxL._AC_UL320_.jpg",
    price: 989.0,
    retailPrice: 2.5,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/SALCKO-Traditional-Nepali-Machine-Transparent/dp/B08WX2YVJV/ref=sr_1_14?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "Prime Seller Women's Soft Litchi Silk banarasi jacquard Saree With Matching Blouse Piece(Black and Red)",
    asin: "B08ZV4JXYH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81YSAGJsKyL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1.599,
    productRating: null,
    prime: true,
    dpUrl:
      "/Prime-Seller-banarasi-jacquard-Matching/dp/B08ZV4JXYH/ref=sr_1_15?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Dettol Liquid Handwash Refill - Original Germ Protection Hand Wash- 1500ml | Antibacterial Formula | 10x Better Germ Protection",
    asin: "B01LNA2MQK",
    countReview: 73905,
    imgUrl: "https://m.media-amazon.com/images/I/51jYzijyp5L._AC_UL320_.jpg",
    price: 171.0,
    retailPrice: 209.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/beauty/26984089031/ref=sr_bs_15_26984089031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "The Seller: A Story of Justice",
    asin: "B06XSSDPX5",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/A1iaKn56QnL._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Seller-Story-Justice-Pedogate-ebook/dp/B06XSSDPX5/ref=sr_1_17?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-17",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Green Cotton Embroidered Kurta and Bottom Set for Women",
    asin: "B0B7N95WG5",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71viW7uWDcL._AC_UL320_.jpg",
    price: 649.0,
    retailPrice: 999.0,
    productRating: null,
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Embroidered-Bottom/dp/B0B7N95WG5/ref=sr_1_18?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-18",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Green",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-Bottom/dp/B0B7N95WG5/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-Bottom/dp/B0B7NCZSBB/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-Bottom/dp/B0B7N9VDHF/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-Bottom/dp/B0B7N7Z9FS/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Acmed Pimple Care for Acne Prone Skin (Pack of 2) Face Wash (140 ml)-Buy Only From Seller E Mega Mart India",
    asin: "B07L2Y15V9",
    countReview: 1198,
    imgUrl: "https://m.media-amazon.com/images/I/613kstWTL+L._AC_UL320_.jpg",
    price: 358.0,
    retailPrice: 398.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Acmed-Pimple-Care-Prone-Wash/dp/B07L2Y15V9/ref=sr_1_19?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-19",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Instagram Printed Tshirt",
    asin: "B0B53H54XP",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51MN-CToz8L._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 999.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Regular-Instagram-White_XL/dp/B0B53H54XP/ref=sr_1_20?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-20",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "All the Light we Cannot See: The Breathtaking World Wide Bestseller & War of Lanka (Ram Chandra Series Book 4) & The Cruel Prince (The Folk of the Air)",
    asin: "B0BM9LNVFD",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61IRIVoFmfL._AC_UL320_.jpg",
    price: 944.0,
    retailPrice: 1.548,
    productRating: null,
    prime: true,
    dpUrl:
      "/All-Light-Cannot-See-Breathtaking/dp/B0BM9LNVFD/ref=sr_1_21?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-21",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Plastic Water Bottle for Fridge Checkered Leak-Proof Water Bottles - 1 Litre randomy Any Colors Will Come (Set of 2)",
    asin: "B092VW3HWH",
    countReview: 31,
    imgUrl: "https://m.media-amazon.com/images/I/71zdUdXcsHL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 999.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Plastic-Checkered-Leak-Proof/dp/B092VW3HWH/ref=sr_1_22?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-22",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Bathroom mat, Super Absorbent Soft Carpet, Rubber Back Runner Mat for Kitchen Bathroom & Floors, Anti Slip Bathmat for Bedroom, Living Room, Floor and Door Mat(40 x 60 cm)(Multicolor)",
    asin: "B0B66PHLN3",
    countReview: 15,
    imgUrl: "https://m.media-amazon.com/images/I/51eftFA1XTL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 599.0,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/JM-SELLER-Bathroom-Absorbent-Multicolor/dp/B0B66PHLN3/ref=sr_1_23?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-23",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Pink Cotton Printed Noodle Strap Short Dress for Women",
    asin: "B0BCZ1YKWV",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81lpspCTsXL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 899.0,
    productRating: null,
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ1YKWV/ref=sr_1_24?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-24",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ1YKWV/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ2M63G/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off-White",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZH9Q8T/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Printed-Noodle/dp/B0BCZ2PJ87/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Wonderchef - Stainless Steel Coconut Scraper/Coconut Scrapper for Kitchen/Vacuum Base Coconut Scraper, Silver",
    asin: "B019IDVF5I",
    countReview: 6974,
    imgUrl: "https://m.media-amazon.com/images/I/51Pk-Fi5PuL._AC_UL320_.jpg",
    price: 249.0,
    retailPrice: 350.0,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/kitchen/27054074031/ref=sr_bs_24_27054074031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Hammonds Flycatcher RFID Protected Brown Leather Wallet for Men|5 Card Slots| 1 Coin Pocket|2 Hidden Compartment|2 Currency Slots|1 ID Slot|with Easy Access Card Container",
    asin: "B08245VBYC",
    countReview: 8467,
    imgUrl: "https://m.media-amazon.com/images/I/91xv1zjVmtS._AC_UL320_.jpg",
    price: 498.0,
    retailPrice: 1.599,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B08245VBYC/ref=sr_1_26?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Brown",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B08245VBYC/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK with BELT",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B09MTYW73W/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B06ZZMF76T/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B08MTZ4CCV/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black Sumo",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B09TFZ3NP5/ref=cs_sr_dp_5?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue Sumo",
        dpUrl:
          "/Hammonds-Flycatcher-Protected-Compartment-Container/dp/B09TG1292F/ref=cs_sr_dp_6?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Online cane seller Bamboo, Wooden, Cane Shoe Rack ,Slipper Stand ,Utility Rack ,Planter Stand, Bookshelf ,Shelf for Home Kitchen, Rack for Office Files",
    asin: "B097RD6CPB",
    countReview: 4,
    imgUrl: "https://m.media-amazon.com/images/I/81zONQubRfL._AC_UL320_.jpg",
    price: 1.099,
    retailPrice: 1.999,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/Slipper-Utility-Planter-Bookshelf-Kitchen/dp/B097RD6CPB/ref=sr_1_27?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-27",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER Portable Handheld Garment Steamer For Clothes And Facial Steamer for Face and Nose at Home and in Travel (Multi Colour)",
    asin: "B09BQBP2MN",
    countReview: 21,
    imgUrl: "https://m.media-amazon.com/images/I/311e5TaExpL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1.299,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Portable-Handheld-Garment/dp/B09BQBP2MN/ref=sr_1_28?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-28",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER 16 Colors Changing LED 3D, Sensor Touch Night Light Moon Lamp with Wooden Stand, Remote Control and USB Rechargeable for Bedrooms Decoration Ball (15 cm, RGB)Pack of 1",
    asin: "B09B9P3FNP",
    countReview: 79,
    imgUrl: "https://m.media-amazon.com/images/I/713Gf1Zc--L._AC_UL320_.jpg",
    price: 799.0,
    retailPrice: 999.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Changing-Rechargeable-Decoration/dp/B09B9P3FNP/ref=sr_1_29?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-29",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Blue Rayon Embroidered Long Kurti for Women",
    asin: "B0BC1SB7HK",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/714fVqZx9VL._AC_UL320_.jpg",
    price: 579.0,
    retailPrice: 899.0,
    productRating: null,
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1SB7HK/ref=sr_1_30?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-30",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1SB7HK/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1QDFCN/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1H49J8/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1TBWFG/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Kushuvi Cushion Without Beans ( 4 XL )",
    asin: "B0BMGH2D1Q",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41K9z1JNhdL._AC_UL320_.jpg",
    price: 149.0,
    retailPrice: 399.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Kushuvi-Cushion-Without-Beans-XL/dp/B0BMGH2D1Q/ref=sr_1_31?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-31",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "SEL: SYSTEM BALANCED BY LANGUAGE: “A Reform in Language”: JESUS-ARCHETYPE-SAVIOR-ANOINTED, LOGOCENTRISM - THE INSUFFICIENCY OF THE LAW, RETURN-TO-MOUNT-SERMON.",
    asin: "B088SQSZPZ",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71uokENTZ9L._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/SEL-JESUS-ARCHETYPE-SAVIOR-ANOINTED-INSUFFICIENCY-RETURN-MOUNT-SERMON-ebook/dp/B088SQSZPZ/ref=sr_1_32?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-32",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Sel-JESUS-ARCHETYPE-SAVIOR-ANOINTED-INSUFFICIENCY-RETURN-MOUNT-SERMON/dp/B088T7VMCN/ref=sr_1_32?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "All the Light we Cannot See: The Breathtaking World Wide Bestseller & War of Lanka (Ram Chandra Series Book 4) & The Kid Who Came From Space",
    asin: "B0BM9NLNJW",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61+G6nRVR3L._AC_UL320_.jpg",
    price: 941.0,
    retailPrice: 1.448,
    productRating: null,
    prime: true,
    dpUrl:
      "/All-Light-Cannot-See-Breathtaking/dp/B0BM9NLNJW/ref=sr_1_33?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-33",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Spenfrone Multi-Purpose 360° Rotating Organizer Tray/Kitchen Organizer/Multi- Function Rotating Tray/Cosmetics Organizer (Cream, Pack of 1)",
    asin: "B0B97GHMJ5",
    countReview: 249,
    imgUrl: "https://m.media-amazon.com/images/I/61lZz9eA8eL._AC_UL320_.jpg",
    price: 129.0,
    retailPrice: 0.0,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/kitchen/1380346031/ref=sr_bs_33_1380346031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Blue Rayon Two Tone Embroidered Short Kurti for Women",
    asin: "B0BC1MJCFF",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71T7Y+YqQ3L._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 799.0,
    productRating: null,
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1MJCFF/ref=sr_1_35?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-35",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1MJCFF/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Purple",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1KMD3L/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC2521NK/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Rayon-Embroidered-Kurti/dp/B0BC1N4D1D/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Lipzlite Lip Lightening cream By Authorized Seller E Mega Mart India",
    asin: "B07VMDM6G6",
    countReview: 126,
    imgUrl: "https://m.media-amazon.com/images/I/51gRnkzJeEL._AC_UL320_.jpg",
    price: 246.0,
    retailPrice: 259.0,
    productRating: "3.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Lipzlite-Lightening-cream-Authorized-Seller/dp/B07VMDM6G6/ref=sr_1_36?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "JM SELLER 260 W 2021 Edition Electric Beater High Speed Hand Mixer Egg Beater for Cake Making and Whipping Cream with 7 Speed Control (White) with Free Spatula and Oil Brush",
    asin: "B08JV91JTK",
    countReview: 474,
    imgUrl: "https://m.media-amazon.com/images/I/61CS4Ot1DxL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 0.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/JM-SELLER-Electric-Beater-180-Watt/dp/B08JV91JTK/ref=sr_1_37?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-37",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Cotton Digital Printed Short Kurti for Women",
    asin: "B0B4GDPKTH",
    countReview: 22,
    imgUrl: "https://m.media-amazon.com/images/I/81U1rOnKgKL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 799.0,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GDPKTH/ref=sr_1_38?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Blue And Red",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GDPKTH/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GCBMZL/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Blue",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Digital-Printed/dp/B0B4GBX7NZ/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Healthgenie Digital Weight Machine Thick Tempered Glass LCD Display With 3 Years Warranty Weighing Scale( Sporty Red)",
    asin: "B0B1QXJG9Z",
    countReview: 735,
    imgUrl: "https://m.media-amazon.com/images/I/51QKThfZxCL._AC_UL320_.jpg",
    price: 659.0,
    retailPrice: 1.199,
    productRating: "4.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Healthgenie-Tempered-Warranty-Sporty-Red/dp/B0B1QXJG9Z/ref=sr_1_39?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Sparx Mens Sx0677g Running",
    asin: "B09BZNPS98",
    countReview: 2849,
    imgUrl: "https://m.media-amazon.com/images/I/71JxwvpS7iL._AC_UL320_.jpg",
    price: 959.0,
    retailPrice: 1.249,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09BZNPS98/ref=sr_1_40?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-40",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "T. BLUE BLACK",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09BZNPS98/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK RED",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B098FGM8DJ/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "D. GREY NEON GREEN",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B08ZNHY9M5/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "N. BLUE NEON ORANGE",
        dpUrl:
          "/Sparx-SM-677-Turkey-Sports-SX0677G_TBBK_0009/dp/B09879NLW2/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "DREAM SELLER Unisex Cotton Linen Trandy Custom Regular Fit Tshirt",
    asin: "B0B3JLWS78",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/411DuzJ-mtL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 599.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/DREAM-SELLER-Unisex-Regular-White_WE0023_L-Large/dp/B0B3JLWS78/ref=sr_1_41?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-41",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Sel: Les enquêtes du département V 9",
    asin: "B0B4BPMKL2",
    countReview: 1697,
    imgUrl: "https://m.media-amazon.com/images/I/91lQ1yNhcgL._AC_UL320_.jpg",
    price: 0.0,
    retailPrice: 887.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Audible-Sel-enqu%C3%AAtes-d%C3%A9partement/dp/B0B4BPMKL2/ref=sr_1_42?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-42",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Washing Machine Cleaner, Descaler For Washing Machine Front Load and Top Load, Washing Machine Cleaning Powder, Descaling Powder For Washing Machine, Drum Cleaner For Front Load Washing Machine 100 gm (Pack Of 5) (Washing Machine Cleaning Powder)",
    asin: "B0BGBYVWXF",
    countReview: 163,
    imgUrl: "https://m.media-amazon.com/images/I/61HzGTMvYjL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 999.0,
    productRating: "4.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/Washing-Machine-Descaler-Cleaning-Descaling/dp/B0BGBYVWXF/ref=sr_1_43?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-43",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "War of Lanka (Ram Chandra Series Book 4) & The Guest List: From the author of The Hunting Party, the No.1 Sunday Times bestseller and prize winning mystery thriller in 2021",
    asin: "B0BLVQ366P",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61W2s7jPWmL._AC_UL320_.jpg",
    price: 648.0,
    retailPrice: 949.0,
    productRating: null,
    prime: true,
    dpUrl:
      "/Lanka-Chandra-Book-Guest-List/dp/B0BLVQ366P/ref=sr_1_44?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-44",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Cotton Long Embroidered Kurta for Women",
    asin: "B0B3DX4X5R",
    countReview: 4,
    imgUrl: "https://m.media-amazon.com/images/I/81i+SJxyrlL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 899.0,
    productRating: "2.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Cotton-Embroidered-HIVA_001_Pink_XL/dp/B0B3DX4X5R/ref=sr_1_45?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Pink",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-HIVA_001_Pink_XL/dp/B0B3DX4X5R/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-HIVA_001_Pink_XL/dp/B0B3DXL4VM/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Orange",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-HIVA_001_Pink_XL/dp/B0B3F1MR21/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Yellow",
        dpUrl:
          "/PINPOINT-SELLER-Cotton-Embroidered-HIVA_001_Pink_XL/dp/B0B3DVX6S7/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "STRIFF Adjustable Laptop Stand Patented Riser Ventilated Portable Foldable Compatible with MacBook Notebook Tablet Tray Desk Table Book with Free Phone Stand(Black)",
    asin: "B07XCM6T4N",
    countReview: 22208,
    imgUrl: "https://m.media-amazon.com/images/I/71Zf9uUp+GL._AC_UL320_.jpg",
    price: 371.0,
    retailPrice: 1.499,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/computers/1375314031/ref=sr_bs_45_1375314031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "PINPOINT SELLER Women's Rayon Checkered Straight Kurti",
    asin: "B09RWPC4FR",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/41gjB18BjVL._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 999.0,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPC4FR/ref=sr_1_47?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-47",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Green",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPC4FR/ref=cs_sr_dp_1?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWQ1QR6/ref=cs_sr_dp_2?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPLPDR/ref=cs_sr_dp_3?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/PINPOINT-SELLER-Womens-Checkered-Straight/dp/B09RWPPDZ1/ref=cs_sr_dp_4?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "amiciVision Metal LED Torch Flashlight 2000 Lumens,XML T6 Water Resistance 5 Modes Adjustable Focus with 3 AAA Battery & Mount (Black)",
    asin: "B07BYST568",
    countReview: 4814,
    imgUrl: "https://m.media-amazon.com/images/I/71DNbqkXBFL._AC_UL320_.jpg",
    price: 999.0,
    retailPrice: 1.5,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/amiciAuto-amiciKart-Degree-Bicycle-Flashlight/dp/B07BYST568/ref=sr_1_48?keywords=best+sellers&qid=1668703602&qu=eyJxc2MiOiI2Ljk1IiwicXNhIjoiNS42NSIsInFzcCI6IjMuMDAifQ%3D%3D&sr=8-48",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
];
