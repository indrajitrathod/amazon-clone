export const mensFashionProductData = [
  
  {
    productDescription:
      "Men's Jewellery 3D Cuboid Vertical Bar/Stick Stainless Steel Black Silver Locket Pendant Necklace For Boys and Men",
    asin: "B0BD9629LX",
    countReview: 213,
    imgUrl: "https://m.media-amazon.com/images/I/51P-ohUTdgL._AC_UL320_.jpg",
    price: 99.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/jewelry/ref=sr_bs_1_jewelry_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Men's Regular Fit Washed Full Sleeve Denim Jacket",
    asin: "B08PFYQSPV",
    countReview: 1388,
    imgUrl: "https://m.media-amazon.com/images/I/71cnAWrBHXL._AC_UL320_.jpg",
    price: 1099,
    retailPrice: 2399,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B08PFYQSPV/ref=sr_1_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Light Grey",
        dpUrl:
          "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B08PFYQSPV/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B08PFZBB2P/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B08PFYLCZF/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B08PFYB45F/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Olive Green",
        dpUrl:
          "/Urbano-Fashion-Regular-Washed-jakt-denim-lgrey-m/dp/B0BDMZLQ7B/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Fashion Jewellery High Polished Stylish Stainless Steel Full Kada Style Bracelet For Boys and Men BR1000049",
    asin: "B0BFHRFV85",
    countReview: 17,
    imgUrl: "https://m.media-amazon.com/images/I/51RCyVOxVIL._AC_UL320_.jpg",
    price: 179.0,
    retailPrice: 999.0,
    productRating: "3.1 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/jewelry/15752580031/ref=sr_bs_3_15752580031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Charming Silver King Lion Silver Plated Stylish Silver Ring For Men Boys Stainless Steel Tough Dude Chain Rings for Men Accessories Jewellery for Men",
    asin: "B0B8CF8KCR",
    countReview: 20,
    imgUrl: "https://m.media-amazon.com/images/I/61aBAUzjRmL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Frill-Stainless-Accessories-Jewellery/dp/B0B8CF8KCR/ref=sr_1_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Mens Casual Stripped Sneakers",
    asin: "B077N7DDL1",
    countReview: 43685,
    imgUrl: "https://m.media-amazon.com/images/I/51g6S2XoAcL._AC_UL320_.jpg",
    price: 707.0,
    retailPrice: 899.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/shoes/1983577031/ref=sr_bs_5_1983577031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "White Black",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B077N7DDL1/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black white",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B077MXB3MX/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "NAVY WHITE",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B07WGPN2ZQ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Off white Black",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B0B4KF6LFM/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue White",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B08L92V5QD/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy white",
        dpUrl:
          "/Sparx-Mens-Sneakers-8-India-SD0323G/dp/B07V5TV3M4/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-6",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Cotton Regular Fit Casual Shirt",
    asin: "B091648P3K",
    countReview: 755,
    imgUrl: "https://m.media-amazon.com/images/I/61100SqRbcL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091648P3K/ref=sr_1_7?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Astronaut Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091648P3K/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091666M2K/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cerulean Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B09167VQHJ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091662NQX/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091661JWX/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B0B71WJ56T/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-7",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Light Blue Slim Fit Heavy Distressed/Torn Jeans",
    asin: "B08S388MLD",
    countReview: 350,
    imgUrl: "https://m.media-amazon.com/images/I/61bya74GM8L._AC_UL320_.jpg",
    price: 842.0,
    retailPrice: 1799,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Light-Distressed-avdisheavywhsk-lblu-32/dp/B08S388MLD/ref=sr_1_8?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Men's Printed Full Sleeve Slim Fit Cotton T-Shirt",
    asin: "B08JQJYRZW",
    countReview: 4240,
    imgUrl: "https://m.media-amazon.com/images/I/61bDoqhvEPL._AC_UL320_.jpg",
    price: 420.0,
    retailPrice: 1099,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B08JQJYRZW/ref=sr_1_9?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark Green",
        dpUrl:
          "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B08JQJYRZW/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua Blue",
        dpUrl:
          "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B09HH657RM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White Grey",
        dpUrl:
          "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B09HH9RLHS/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B09HH9G49K/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Olive Green",
        dpUrl:
          "/Urbano-Fashion-Printed-T-Shirt-aopleaffull-drgreen-m/dp/B09HH8Q9HX/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-9",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Hub Men's Digital Printed Stylish Shirts",
    asin: "B09C41GM4V",
    countReview: 224,
    imgUrl: "https://m.media-amazon.com/images/I/41RDwJ0ZiHL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1999,
    productRating: "3.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/parth-fashion-Digital-Printed-Stylish/dp/B09C41GM4V/ref=sr_1_10?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C41GM4V/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black 2",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C41BSRN/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black 3",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C3Z4GQZ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black 5",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C41X83P/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C3ZNPRB/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue 2",
        dpUrl:
          "/parth-fashion-Digital-Printed-Stylish/dp/B09C4196K6/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-10",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Solid Henley Neck Slim Fit Full Sleeve Cotton T-Shirt",
    asin: "B08PBZ4LRN",
    countReview: 818,
    imgUrl: "https://m.media-amazon.com/images/I/61VqyMOJ9xL._AC_UL320_.jpg",
    price: 420.0,
    retailPrice: 1099,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B08PBZ4LRN/ref=sr_1_11?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B08PBZ4LRN/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bottle Green",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B09XVC2R9N/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Green",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B08PBXX75M/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B08PBZDPXJ/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard Melange",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B09XVB7MNR/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Urbano-Fashion-Henley-T-Shirt-hentee-Full-Black-XL/dp/B08PC12FJ6/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-11",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Casual Shirt",
    asin: "B09GNXP7XG",
    countReview: 571,
    imgUrl: "https://m.media-amazon.com/images/I/71qTbVnVZZL._AC_UL320_.jpg",
    price: 397.0,
    retailPrice: 1999,
    productRating: "3.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNXP7XG/ref=sr_1_12?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark Green",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNXP7XG/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cream",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GP18F8J/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNW9FG9/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink 1",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNZK8J3/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White 1",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNYSFD6/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Destiny-Fashion-Regular-green_print_44_dark-Green_44/dp/B09GNZW7CR/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-12",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Jeans",
    asin: "B08D9SSG2R",
    countReview: 2596,
    imgUrl: "https://m.media-amazon.com/images/I/71m0W58-zQL._AC_UL320_.jpg",
    price: 789.0,
    retailPrice: 1599,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B08D9SSG2R/ref=sr_1_13?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Light Blue",
        dpUrl:
          "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B08D9SSG2R/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B0829B6F8G/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Grey",
        dpUrl:
          "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B08KTQQ955/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Ice Blue",
        dpUrl:
          "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B08D9SZRCV/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Urbano-Fashion-Jeans-epsrustsprcrs-lblue-34_Light-Blue_34/dp/B08D9T2R39/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Slim Fit Casual Trouser",
    asin: "B075KFHVNV",
    countReview: 6257,
    imgUrl: "https://m.media-amazon.com/images/I/61jgOJ2QLmL._AC_UL320_.jpg",
    price: 799.0,
    retailPrice: 1499,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B075KFHVNV/ref=sr_1_14?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B075KFHVNV/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B075KJNS8J/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Khaki",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B075KQCTX5/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B07BVQPLHF/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B075KKGGLF/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Royal Blue",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Casual-chino-black-32-fba/dp/B084ZTHJHT/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Light Grey Slim Fit Jogger Jeans Stretch",
    asin: "B07D6MMQ3P",
    countReview: 7306,
    imgUrl: "https://m.media-amazon.com/images/I/71J5v1vrKaL._AC_UL320_.jpg",
    price: 757.0,
    retailPrice: 1599,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07D6MMQ3P/ref=sr_1_15?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Light Grey",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07D6MMQ3P/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07MXNBRBM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07QYK2ZTV/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue 2",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07MKG74J5/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B07MKG9C6G/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Urbano-Fashion-Stretchable-Jogger-hpsjogger-lgrey-34-fba/dp/B074RKXTKB/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Formal Trousers",
    asin: "B07YT179J4",
    countReview: 11623,
    imgUrl: "https://m.media-amazon.com/images/I/717-WaIUi9L._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 2499,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/apparel/5836982031/ref=sr_bs_15_5836982031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Light Grey",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B07YT179J4/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B07S2Z1C7V/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black 4",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B07YT19LZH/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B07S55P1N5/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue 3",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B07YT1GKKQ/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Charcoal Solid",
        dpUrl:
          "/Amazon-Brand-Stretchable-Trousers-SY-AW19-MFT-049_Light/dp/B09VL28Z2R/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-16",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Stylish Hooded Graphic Printed T-Shirt (White)",
    asin: "B0BDVG7BHS",
    countReview: 14,
    imgUrl: "https://m.media-amazon.com/images/I/71R+Ygx2bJL._AC_UL320_.jpg",
    price: 479.0,
    retailPrice: 1999,
    productRating: "2.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/LEWEL-Stylish-Graphic-Printed-T-Shirt/dp/B0BDVG7BHS/ref=sr_1_17?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-17",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Cotton Men's Casual Shirt",
    asin: "B08VGQ7CW9",
    countReview: 3329,
    imgUrl: "https://m.media-amazon.com/images/I/617DyiGpBQL._AC_UL320_.jpg",
    price: 579.0,
    retailPrice: 2499,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/Vida-Loca-Cotton-Casual-Kurta/dp/B08VGQ7CW9/ref=sr_1_18?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "White",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B08VGQ7CW9/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B08VGLBV7X/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B08VGT3M96/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B08VGQFYCX/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Peach",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B098FGBTNR/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/Vida-Loca-Cotton-Casual-Kurta/dp/B09BZP86L8/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-18",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Casual Shirt",
    asin: "B099MSRR42",
    countReview: 621,
    imgUrl: "https://m.media-amazon.com/images/I/512pBZ7L-JL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 2299,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B099MSRR42/ref=sr_1_19?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Olive",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B099MSRR42/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Burnt Orange",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B09RBB4SZR/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Camel",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B09RB9VRY5/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Brown",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B09RBDQ4X5/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Maroon",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B09RBD48V7/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Diverse-Mens-Solid-Shirt-DCMCF99SC22L38-4448_Olive/dp/B099N2WNPR/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-19",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Fashion Mens Binary Sports Watch Digital LED Matrix Waterproof Outdoor Casual Black Bracelet Square Blue Backlit Watches",
    asin: "B07L5ZH9TK",
    countReview: 1710,
    imgUrl: "https://m.media-amazon.com/images/I/61ondMNSwdL._AC_UL320_.jpg",
    price: 4333,
    retailPrice: 6999,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Classic-Digital-Waterproof-Stainless-Watches/dp/B07L5ZH9TK/ref=sr_1_20?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-20",
    series: null,
    deliveryMessage: "Get it Saturday, November 26 - Wednesday, November 30",
    variations: [],
  },
  {
    productDescription:
      "Saheli Fashion Men's Poly Cotton Digital Printed Stitched Half Sleeve Shirt Casual Shirt Hawaii Shirt for Men",
    asin: "B0B8ZHR75S",
    countReview: 18,
    imgUrl: "https://m.media-amazon.com/images/I/61os5unWZHL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 1999,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZHR75S/ref=sr_1_21?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZHR75S/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multicolor 5",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZLGLRY/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multicolor 7",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZG2K3X/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multicolor 9",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZL7N4X/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Multicolor 3",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZGWB94/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-X-Large/dp/B0B8ZJK22X/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Chunk Street Style Mens Black Street Fashion Sneakers/Casual Shoes for Mens",
    asin: "B097NDR6J5",
    countReview: 50,
    imgUrl: "https://m.media-amazon.com/images/I/71V3mNArbsS._AC_UL320_.jpg",
    price: 1299,
    retailPrice: 2499,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Bacca-Bucci-Street-Fashion-Sneakers/dp/B097NDR6J5/ref=sr_1_22?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-22",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Men's Printed Sweatshirt Hoodie",
    asin: "B0BHBXKNK4",
    countReview: 40,
    imgUrl: "https://m.media-amazon.com/images/I/41iwLuxR4bL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 1999,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHBXKNK4/ref=sr_1_23?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Grey",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHBXKNK4/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHBKSCWL/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHC7X5WD/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHB5TR15/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHC6FXH3/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/Modern-Soul-Printed-Sweatshirt-Hoodie/dp/B0BHBFTGB9/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-23",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Fashion Trunk",
    asin: "B00W04L20G",
    countReview: 12102,
    imgUrl: "https://m.media-amazon.com/images/I/91LB6YTEIDL._AC_UL320_.jpg",
    price: 329.0,
    retailPrice: 570.0,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B00W04L20G/ref=sr_1_24?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B00W04L20G/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Charcoal Melange",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B00W04L9WM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B071RVNLBV/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pack of 2 - Black + Grey",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B09GYQQRPC/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pack of 2 - Black Jaspe + Charcoal Melange",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B09GYQQBGT/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pack of 3 - Black + Black + Grey",
        dpUrl:
          "/Jockey-US21-0105-BLACK-Black-Trunks-US21-0105-BLACK_Black_M/dp/B09H7W8RR2/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Cotton Full Sleeve Short Kurta",
    asin: "B0B9HLZ87J",
    countReview: 116,
    imgUrl: "https://m.media-amazon.com/images/I/41enGazs4eL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HLZ87J/ref=sr_1_25?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-25",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Sky",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HLZ87J/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HKR5SR/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Regular Fit Formal Shirt",
    asin: "B075KK8SZ4",
    countReview: 25613,
    imgUrl: "https://m.media-amazon.com/images/I/81vuqr1DAtL._AC_UL320_.jpg",
    price: 419.0,
    retailPrice: 1199,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/apparel/1968096031/ref=sr_bs_25_1968096031_1",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Navy/Golden",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B075KK8SZ4/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Antenna White",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B07MN3CW6B/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B07D7688XD/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B07D75FCH8/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bracket White",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B074C6RXV5/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Circle White",
        dpUrl:
          "/Diverse-Printed-Regular-Cotton-DVF01F2L01-263-40_Navy_40/dp/B074C5X9F6/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Casual Shirt",
    asin: "B08R21T75D",
    countReview: 2859,
    imgUrl: "https://m.media-amazon.com/images/I/61kj0DfT6VL._AC_UL320_.jpg",
    price: 498.0,
    retailPrice: 1999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21T75D/ref=sr_1_27?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Peach",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21T75D/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R217KNH/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21HM6J/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21D26K/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sky Blue",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R219912/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Regular Fit Casual Shirt",
    asin: "B07QGM3VQM",
    countReview: 2241,
    imgUrl: "https://m.media-amazon.com/images/I/61QO-J8oXpL._AC_UL320_.jpg",
    price: 498.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B07QGM3VQM/ref=sr_1_28?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "White",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B07QGM3VQM/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Baby Pink",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B08DNW7YX6/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B09MYMZKJQ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B09MYRDVD2/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sky Blue",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B084PYCN9H/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Shirt-Sleeves/dp/B09TH5RFXS/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Mandarin Collar Casual Shirt",
    asin: "B07HK6NT81",
    countReview: 17805,
    imgUrl: "https://m.media-amazon.com/images/I/61YQd1wdQBL._AC_UL320_.jpg",
    price: 649.0,
    retailPrice: 1849,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07HK6NT81/ref=sr_1_29?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Dusty Pink",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07HK6NT81/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B08ZN4TMYX/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bottle Green",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B08ZKS3YDW/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Burgundy",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H36D2YR/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dusty Green",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H354YBT/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dusty Orange",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H3663M6/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-29",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Hunter Brown Leather Men's RFID Blocking Wallet (BLCHCASE)",
    asin: "B084H74LHV",
    countReview: 2040,
    imgUrl: "https://m.media-amazon.com/images/I/71bfPnNplmL._AC_UL320_.jpg",
    price: 499.0,
    retailPrice: 1699,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Fashion-Freak%C2%AE-Credit-Card-Holder/dp/B084H74LHV/ref=sr_1_30?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-30",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "6 Pocket Relaxed and Regular Fit Cotton Jogger Pants for Men. Design for Casual and Sporty Looks. (Dark Green)",
    asin: "B0839K7L7J",
    countReview: 4,
    imgUrl: "https://m.media-amazon.com/images/I/71bU34JBu3L._AC_UL320_.jpg",
    price: 847.0,
    retailPrice: 3999,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urban-Legends-Relaxed-Regular-GREEN30/dp/B0839K7L7J/ref=sr_1_31?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-31",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Men's Artificial Leather Belt",
    asin: "B07SJGR95G",
    countReview: 3570,
    imgUrl: "https://m.media-amazon.com/images/I/71PWQ1b8LHL._AC_UL320_.jpg",
    price: 498.0,
    retailPrice: 1200,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SJGR95G/ref=sr_1_32?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black9",
        dpUrl:
          "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SJGR95G/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black12",
        dpUrl:
          "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SKJBQZ3/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black15",
        dpUrl:
          "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SKGRTV9/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black3",
        dpUrl:
          "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SKHJR2N/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black6",
        dpUrl:
          "/VOGARD-Auto-Lock-Buckle-Black/dp/B07SKHYQSX/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Stylish Hooded Graphic Printed T-Shirt (Olive Green, Black)",
    asin: "B0BDVH9T4C",
    countReview: 6,
    imgUrl: "https://m.media-amazon.com/images/I/610jF2HhdNL._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 1999,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/LEWEL-Stylish-Fashion-Graphic-Printed/dp/B0BDVH9T4C/ref=sr_1_33?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-33",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Bracelets for Men and Boys | Fashion Leather Bracelet for Men | Stainless Steel Type Leather Bracelets for Men | Birthday Gift for Men and Boys Anniversary Gift for Husband",
    asin: "B07XZBY9T4",
    countReview: 606,
    imgUrl: "https://m.media-amazon.com/images/I/61dihiOpwqL._AC_UL320_.jpg",
    price: 284.0,
    retailPrice: 1267,
    productRating: "3.9 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Handcrafted-Magnetic-Clasp-Bracelet/dp/B07XZBY9T4/ref=sr_1_34?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-34",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Pendant for Men Black Men Pendant Crystal Adjustable Black Leather Rope Pendant for Men and Women.",
    asin: "B07CV7LSWN",
    countReview: 2762,
    imgUrl: "https://m.media-amazon.com/images/I/41FB2hvq3mL._AC_UL320_.jpg",
    price: 208.0,
    retailPrice: 1245,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Crystal-Collection-YCFJPD-56WOLF-BK/dp/B07CV7LSWN/ref=sr_1_35?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-35",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Men T-Shirt",
    asin: "B09PNL3KD8",
    countReview: 32,
    imgUrl: "https://m.media-amazon.com/images/I/71yn+Gyou7L._AC_UL320_.jpg",
    price: 429.0,
    retailPrice: 2499,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Amazon-Brand-Inkast-Regular-INK-SS22-TSH-16_Vin/dp/B09PNL3KD8/ref=sr_1_36?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Sports Fahion Shoes for Men, Gym, Running, Sports",
    asin: "B084VR1Q4G",
    countReview: 296,
    imgUrl: "https://m.media-amazon.com/images/I/51HeJHJmXXL._AC_UL320_.jpg",
    price: 277.0,
    retailPrice: 799.0,
    productRating: "2.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B084VR1Q4G/ref=sr_1_37?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "black",
        dpUrl:
          "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B084VR1Q4G/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B084VVMZ4J/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B09J2CP5LJ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black And White",
        dpUrl:
          "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B09D433J24/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "grey",
        dpUrl:
          "/UDDIBABA-Fashion-Walking-Lightweight-Numeric_8/dp/B09J2D7W9C/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-37",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Square Sunglasses",
    asin: "B07Y8S46XB",
    countReview: 4406,
    imgUrl: "https://m.media-amazon.com/images/I/51uYuk9R8pL._AC_UL320_.jpg",
    price: 321.0,
    retailPrice: 2299,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/elegante-Branded-Inspired-Sunglass-Silver-Black/dp/B07Y8S46XB/ref=sr_1_38?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Fly Fashion Polyester Travel Sling Crossbody Backpack Shoulder Chest Bag Lightweight Men Women",
    asin: "B08NYG27PK",
    countReview: 109,
    imgUrl: "https://m.media-amazon.com/images/I/51FR5ixPQzL._AC_UL320_.jpg",
    price: 446.0,
    retailPrice: 1499,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fly-Fashion-Polyester-Crossbody-Lightweight/dp/B08NYG27PK/ref=sr_1_39?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "FREE Delivery",
    variations: [
      {
        value: "Blue",
        dpUrl:
          "/Fly-Fashion-Polyester-Crossbody-Lightweight/dp/B08NYG27PK/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-39",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Fly-Fashion-Polyester-Crossbody-Lightweight/dp/B08NYFKV9H/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-39",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Shirt",
    asin: "B07HK7SMGC",
    countReview: 24297,
    imgUrl: "https://m.media-amazon.com/images/I/618Wek95laS._AC_UL320_.jpg",
    price: 647.0,
    retailPrice: 1849,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/apparel/1968094031/ref=sr_bs_39_1968094031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Dusty Pink",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B07HK7SMGC/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B0B7RJ7C8M/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bottle Green",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B07W1Y4N3W/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B07FK4T6PV/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Burgundy",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B07H36VQDX/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Camel",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-C301_Dustypink_L_Dustypink_L/dp/B08CKRYRFQ/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Square Black Sunglasses-Pack of 1",
    asin: "B071CP6K43",
    countReview: 14906,
    imgUrl: "https://m.media-amazon.com/images/I/61+THk53YIL._AC_UL320_.jpg",
    price: 798.0,
    retailPrice: 899.0,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/apparel/1968036031/ref=sr_bs_40_1968036031_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Wrist Watches for Men Leather Strap Analog Chronograph Fashion Business Sport Design Mens Watch 30M Waterproof Stylish Elegant Gifts for Men",
    asin: "B09JGNLXVT",
    countReview: 100,
    imgUrl: "https://m.media-amazon.com/images/I/71I7c2u-fXS._AC_UL320_.jpg",
    price: 2890,
    retailPrice: 4990,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/BENYAR-Chronograph-Business-Waterproof-Blue-Gold/dp/B09JGNLXVT/ref=sr_1_42?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-42",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription: "Men's Cotton Crew Neck Sweatshirt",
    asin: "B07DPTV1MR",
    countReview: 18464,
    imgUrl: "https://m.media-amazon.com/images/I/61Die4MR6GL._AC_UL320_.jpg",
    price: 839.0,
    retailPrice: 1299,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/apparel/ref=sr_bs_42_apparel_1",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Yellow",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPTV1MR/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Anthra melange",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPV4WBM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPMK73N/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey Melange",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPMK4Z5/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPV9NRP/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/Allen-Solly-Sweatshirt-ASSTORGP773994L_Yellow-13-0758TCX/dp/B07DPTN7YM/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Regular Fit T-Shirt (Pack of 3)",
    asin: "B084L927MC",
    countReview: 34903,
    imgUrl: "https://m.media-amazon.com/images/I/71vp8Lec9JL._AC_UL320_.jpg",
    price: 474.0,
    retailPrice: 2397,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B084L927MC/ref=sr_1_44?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Multicolour",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B084L927MC/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Royal Melange",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B0BFDGQW73/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B0BCKTD3MT/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon Melange",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B0BFDKFD78/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue / Maroon / Royal Blue Melange",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B08L83TJ5H/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Melange",
        dpUrl:
          "/Scott-International-Regular-T-Shirt-SS20-3RN-BU-GR-CH-XL_Multicolored_X-Large/dp/B0BFDHQ1DN/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Leather Belt for Men",
    asin: "B0824PLNV5",
    countReview: 6064,
    imgUrl: "https://m.media-amazon.com/images/I/81jv56HsVOL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2100.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urban-Forest-Clark-Leather-Buckle/dp/B0824PLNV5/ref=sr_1_45?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Brown with Matte Black Buckle",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B0824PLNV5/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey with Black Buckle",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B08TWWLY9W/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Tan",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B0849P3LHT/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-45",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Washed Jeans Stretchable",
    asin: "B076CSDWMK",
    countReview: 6703,
    imgUrl: "https://m.media-amazon.com/images/I/61EJxytnV5L._AC_UL320_.jpg",
    price: 615.0,
    retailPrice: 1299,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B076CSDWMK/ref=sr_1_46?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B076CSDWMK/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B08PC2W3PC/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Green",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B08PC2RVY5/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B08PC2CT18/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Brown",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B08PC1MCWY/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Ice Blue",
        dpUrl:
          "/Urbano-Fashion-Mens-Jeans-eps-black-36-06/dp/B09817SLZV/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Stainless Steel Jewellery Set Combo Pack of 2 Cuboid Vertical Bar Pendant With Black Finish Full Kada Bracelet For Boys and Men CO1000439",
    asin: "B0BJ6LGW87",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/61DOQHHFsqL._AC_UL320_.jpg",
    price: 169.0,
    retailPrice: 1199,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/okos-Stainless-Jewellery-Vertical-CO1000439/dp/B0BJ6LGW87/ref=sr_1_47?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-47",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Men's Slim Fit Jeans",
    asin: "B07VGL2SVC",
    countReview: 1046,
    imgUrl: "https://m.media-amazon.com/images/I/71wzIfg+6hL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1599,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Brown-Stretchable-aveps700-brown-36/dp/B07VGL2SVC/ref=sr_1_48?keywords=mens+fashion&qid=1668703080&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-48",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Men's Slim Fit Jeans",
    asin: "B01MQHAN4O",
    countReview: 765,
    imgUrl: "https://m.media-amazon.com/images/I/51UlwkHPtmL._AC_UL320_.jpg",
    price: 649.0,
    retailPrice: 1299,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Mens-Jeans-eps-black-38201-32/dp/B01MQHAN4O/ref=sr_1_49?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-49",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Men Cotton Cap (Pack of 1) (ILU132_Black_Free Size)",
    asin: "B01BMEQEH0",
    countReview: 2681,
    imgUrl: "https://m.media-amazon.com/images/I/61bBYBR2SFL._AC_UL320_.jpg",
    price: 229.0,
    retailPrice: 499.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Drunken-Mens-Cotton-Snapback-Black_Free/dp/B01BMEQEH0/ref=sr_1_50?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-50",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Leather Belt for Men",
    asin: "B0824PLNV5",
    countReview: 6064,
    imgUrl: "https://m.media-amazon.com/images/I/81jv56HsVOL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2100.0,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urban-Forest-Clark-Leather-Buckle/dp/B0824PLNV5/ref=sr_1_51?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-51",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Brown with Matte Black Buckle",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B0824PLNV5/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-51",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey with Black Buckle",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B08TWWLY9W/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-51",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Tan",
        dpUrl:
          "/Urban-Forest-Clark-Leather-Buckle/dp/B0849P3LHT/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-51",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Bracelets for Men and Boys | Leather Bracelet Set for Men | Multi Layered Strand Leather Wrap Wrist Bracelet Combo | Accessories Jewellery for Men | Birthday Gift for Men and Boys Anniversary Gift for Husband",
    asin: "B07MX8BX1L",
    countReview: 317,
    imgUrl: "https://m.media-amazon.com/images/I/51bizsyqH1L._AC_UL320_.jpg",
    price: 229.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Leather-Wraps-YCFJER-105MLTWRP-BK/dp/B07MX8BX1L/ref=sr_1_52?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-52",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Aenon Fashion Snow Proof Winter Cap for Men Inside Fur Wool Unisex Beanie Cap with Neck Warmer Set Knit Hat Thick Fleece Lined Winter Hat for Men & Women (Pack of 2 Pcs). Black",
    asin: "B0B3D56KYQ",
    countReview: 14,
    imgUrl: "https://m.media-amazon.com/images/I/61kxw8OuGRL._AC_UL320_.jpg",
    price: 215.0,
    retailPrice: 1499,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Aenon-Fashion-passion-Womens-Slouchy/dp/B0B3D56KYQ/ref=sr_1_53?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-53",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "BROWN",
        dpUrl:
          "/Aenon-Fashion-passion-Womens-Slouchy/dp/B0B3D56KYQ/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Aenon-Fashion-passion-Womens-Slouchy/dp/B0B3D6XWBS/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Aenon-Fashion-passion-Womens-Slouchy/dp/B0B3D14RCR/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-53",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Black Stretch Jeans",
    asin: "B07DFPG3NH",
    countReview: 5051,
    imgUrl: "https://m.media-amazon.com/images/I/611hFiiUv4L._AC_UL320_.jpg",
    price: 615.0,
    retailPrice: 1299,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B07DFPG3NH/ref=sr_1_54?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B07DFPG3NH/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue",
        dpUrl:
          "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B09CZCNKWS/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B09CZBTX9V/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Grey",
        dpUrl:
          "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B09CZD8L19/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Light Blue",
        dpUrl:
          "/Urbano-Fashion-Black-Stretch-eps-black-30-07/dp/B09CZCJMJV/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-54",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Jeans",
    asin: "B07VGL2SVC",
    countReview: 1046,
    imgUrl: "https://m.media-amazon.com/images/I/71wzIfg+6hL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1599,
    productRating: "3.5 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Brown-Stretchable-aveps700-brown-36/dp/B07VGL2SVC/ref=sr_1_55?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-55",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Men's Nylon woven fabric Belt, Hole free plastic flap buckle",
    asin: "B07NRY1VZ2",
    countReview: 4618,
    imgUrl: "https://m.media-amazon.com/images/I/51K1xbtBEkL._AC_UL320_.jpg",
    price: 238.0,
    retailPrice: 1499,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07NRY1VZ2/ref=sr_1_56?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Black",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07NRY1VZ2/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07QLWV2LM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07QLW5G9J/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Grey",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B09QCTYGBT/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Golden",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07QKZ9MG2/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/ZORO-Tactical-Automatic-Survival-ZR-NB40-BK-49/dp/B07QLT629X/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-56",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Fashion Stainless Steel Case | Luxury Analog Watch for Men",
    asin: "B0B82V939L",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/81xVINaIKwL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 1999,
    productRating: "3.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Lorenz-Fashion-Stainless-Luxury-MK-4014R/dp/B0B82V939L/ref=sr_1_57?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-57",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "Men's Fashion Jewellery Set Combo Pack of Rhodium Plated Rice Shape Link Chain With Solid Black Polish Full Kada Bracelet For Boys and Men CO1000440BLK",
    asin: "B0BJ6P5B68",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51KOwTiD7mL._AC_UL320_.jpg",
    price: 99.0,
    retailPrice: 999.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/okos-Fashion-Jewellery-Bracelet-CO1000440BLK/dp/B0BJ6P5B68/ref=sr_1_58?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-58",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Men's Slim Fit Chinos",
    asin: "B07YF2WJHD",
    countReview: 555,
    imgUrl: "https://m.media-amazon.com/images/I/7122odVxo9L._AC_UL320_.jpg",
    price: 799.0,
    retailPrice: 1499,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Casual-Stretch-chino-skyblue-34-02/dp/B07YF2WJHD/ref=sr_1_59?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-59",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Sky Blue",
        dpUrl:
          "/Urbano-Fashion-Casual-Stretch-chino-skyblue-34-02/dp/B07YF2WJHD/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Urbano-Fashion-Casual-Stretch-chino-skyblue-34-02/dp/B07YF3KC7L/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-59",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men's Chest Pocket Fashion Chest Pocket Backpack Drawstring Reflective Practical lamp Bag Men and Women Night Running Exercise Hiking",
    asin: "B08YJX79Y5",
    countReview: 170,
    imgUrl: "https://m.media-amazon.com/images/I/61OhVGsRUoL._AC_UL320_.jpg",
    price: 749.0,
    retailPrice: 1052,
    productRating: "3.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/GUSTAVE%C2%AE-Backpack-Drawstring-Reflective-Practical/dp/B08YJX79Y5/ref=sr_1_60?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-60",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/GUSTAVE%C2%AE-Backpack-Drawstring-Reflective-Practical/dp/B08YJX79Y5/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/GUSTAVE%C2%AE-Backpack-Drawstring-Reflective-Practical/dp/B08YJRTZFM/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-60",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Men Ethnic Wear Red Sleeveless Modi Jackets Waistcoats",
    asin: "B09TKV16MM",
    countReview: 8,
    imgUrl: "https://m.media-amazon.com/images/I/51SwpZW4kmL._AC_UL320_.jpg",
    price: 899.0,
    retailPrice: 1999,
    productRating: "2.9 out of 5 stars",
    prime: true,
    dpUrl:
      "/N-B-F-Fashion-Sleeveless-Jackets-Waistcoats/dp/B09TKV16MM/ref=sr_1_61?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-61",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Analogue Black Dial Men's Watch & Wayfarer Sunglasses Combo for Men |Gift Combo Set for Men & Boys (CM-103SN)",
    asin: "B07BZ1DMFY",
    countReview: 1964,
    imgUrl: "https://m.media-amazon.com/images/I/81SbqrhpsOL._AC_UL320_.jpg",
    price: 279.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Lorenz-Analogue-Wayfarer-Sunglasses-CM-103SN/dp/B07BZ1DMFY/ref=sr_1_62?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-62",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription: "Men's Mandarin Collar Casual Shirt",
    asin: "B07HK6NT81",
    countReview: 17805,
    imgUrl: "https://m.media-amazon.com/images/I/61YQd1wdQBL._AC_UL320_.jpg",
    price: 649.0,
    retailPrice: 1849,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07HK6NT81/ref=sr_1_63?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Dusty Pink",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07HK6NT81/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B08ZN4TMYX/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bottle Green",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B08ZKS3YDW/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Burgundy",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H36D2YR/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dusty Green",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H354YBT/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dusty Orange",
        dpUrl:
          "/Dennis-Lingo-Solid-Casual-CC201_Dustypink_M_Dustypink_M/dp/B07H3663M6/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-63",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Dark Blue Slim Fit Washed Jeans Stretchable",
    asin: "B07VCG3V8T",
    countReview: 258,
    imgUrl: "https://m.media-amazon.com/images/I/61uFPT+R38L._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 1499,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Denim-Stretchable-epsjean-dblu-34/dp/B07VCG3V8T/ref=sr_1_64?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-64",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription: "Men's Cotton Full Sleeve Short Kurta",
    asin: "B0B9HLZ87J",
    countReview: 116,
    imgUrl: "https://m.media-amazon.com/images/I/41enGazs4eL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HLZ87J/ref=sr_1_65?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-65",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Sky",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HLZ87J/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Mandarin-X-Large/dp/B0B9HKR5SR/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-65",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Stylish Unisex Leather Strand Bracelet for Men and Women",
    asin: "B07CXQPQK7",
    countReview: 1152,
    imgUrl: "https://m.media-amazon.com/images/I/41g9kMis2UL._AC_UL320_.jpg",
    price: 189.0,
    retailPrice: 799.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Collection-Bracelet-YCFJBR-53BAND-BK/dp/B07CXQPQK7/ref=sr_1_66?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-66",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Men's Solid Mandarin Collar Slim Fit Half Sleeve Cotton T-Shirt",
    asin: "B08D7P19PW",
    countReview: 1369,
    imgUrl: "https://m.media-amazon.com/images/I/61J9oSA9SYL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1099,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7P19PW/ref=sr_1_67?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Dark Green",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7P19PW/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7NLW49/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mustard",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7NYM7W/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7M42R9/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "White",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B08D7NX7B9/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Bottle Green",
        dpUrl:
          "/Urbano-Fashion-Mandarin-T-Shirt-mandplctee-drgreen-m/dp/B09Z6WSD4H/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-67",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Casual Fashion Roman Sandal",
    asin: "B0BG2ZWL2Q",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71FSCi6DZFL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 1999,
    productRating: null,
    prime: false,
    dpUrl:
      "/Casual-Fashion-Roman-Sandal-numeric_7/dp/B0BG2ZWL2Q/ref=sr_1_68?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-68",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "BROWN",
        dpUrl:
          "/Casual-Fashion-Roman-Sandal-numeric_7/dp/B0BG2ZWL2Q/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-68",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BEIGE",
        dpUrl:
          "/Casual-Fashion-Roman-Sandal-numeric_7/dp/B0BG317TYJ/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-68",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "BLACK",
        dpUrl:
          "/Casual-Fashion-Roman-Sandal-numeric_7/dp/B0BG2XPF7S/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-68",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "TAN",
        dpUrl:
          "/Casual-Fashion-Roman-Sandal-numeric_7/dp/B0BG2YJHJJ/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-68",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Jeans",
    asin: "B074YXBTZF",
    countReview: 2546,
    imgUrl: "https://m.media-amazon.com/images/I/71KQOSp+5IL._AC_UL320_.jpg",
    price: 699.0,
    retailPrice: 1199,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/Urbano-Fashion-Mens-Jeans-eps-grey-34-fba/dp/B074YXBTZF/ref=sr_1_69?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-69",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Digital Sports Fashion Multi Functional Wrist Watch for Mens Boys -W45",
    asin: "B0B6WN4DY5",
    countReview: 17,
    imgUrl: "https://m.media-amazon.com/images/I/711FQPrt3OL._AC_UL320_.jpg",
    price: 497.0,
    retailPrice: 1999,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Shocknshop-Digital-Sports-Fashion-Functional/dp/B0B6WN4DY5/ref=sr_1_70?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-70",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Latest Men's Silver Ring For Boys Owl Face Ring Stainless Steel Captain Pirate Adjustable Silver Eagle Hat Pilot Skull Finger Ring For Men Boys Gift For Boyfriend Husband",
    asin: "B0BJ2KL6WQ",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71uM2zSZFtL._AC_UL320_.jpg",
    price: 349.0,
    retailPrice: 999.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Fashion-Frill-Stainless-Adjustable-Contemporary/dp/B0BJ2KL6WQ/ref=sr_1_71?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-71",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription: "Men's Regular Fit Button Down Shirt",
    asin: "B09XXLVNZC",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81vViU0pe5L._AC_UL320_.jpg",
    price: 964.85,
    retailPrice: 2299,
    productRating: null,
    prime: true,
    dpUrl:
      "/Association-Regular-Button-Shirt-UDSHTO0251_Dk/dp/B09XXLVNZC/ref=sr_1_72?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-72",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Men's Lycra Lining Digital Printed Stitched Half Sleeve Shirt Casual Shirts (Medium, New Lai)",
    asin: "B0B4VWXBKH",
    countReview: 23,
    imgUrl: "https://m.media-amazon.com/images/I/411n5QfG5sL._AC_UL320_.jpg",
    price: 433.0,
    retailPrice: 999.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/As-Fashion-Digital-Printed-Stitched/dp/B0B4VWXBKH/ref=sr_1_73?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-73",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Men's Cotton Regular Fit Casual Shirt",
    asin: "B091648P3K",
    countReview: 755,
    imgUrl: "https://m.media-amazon.com/images/I/61100SqRbcL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.6 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091648P3K/ref=sr_1_74?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Astronaut Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091648P3K/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091666M2K/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Cerulean Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B09167VQHJ/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091662NQX/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Red",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B091661JWX/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Turquoise",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B0B71WJ56T/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-74",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Analog Black Dial Unisex-Adult Watch-38024PP25",
    asin: "B099WNYHY2",
    countReview: 5484,
    imgUrl: "https://m.media-amazon.com/images/I/61FFBTzKiUL._AC_UL320_.jpg",
    price: 850.0,
    retailPrice: 0.0,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl: "/gp/bestsellers/watches/15723937031/ref=sr_bs_26_15723937031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Men's Cotton Casual Shirt",
    asin: "B08PDDN3XQ",
    countReview: 1685,
    imgUrl: "https://m.media-amazon.com/images/I/51mr7N7d0oL._AC_UL320_.jpg",
    price: 599.0,
    retailPrice: 2599,
    productRating: "3.7 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B08PDDN3XQ/ref=sr_1_76?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Wine",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B08PDDN3XQ/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Navy Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B06Y4QC4PV/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Aqua",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B085WXQXWS/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Astronout Blue",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B073FMGWWW/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Pink",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B07JNT7D9S/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/FINIVO-FASHION-Cotton-Casual-Shirt/dp/B071ZDMQBL/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-76",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Exclusive Black Silver Chain Men's Jewellery 3D Cuboid Bar Stainless Steel Silver Black Locket Chain Necklace For Men and Boys (Modern)",
    asin: "B0BDDYY1SW",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51P-ohUTdgL._AC_UL320_.jpg",
    price: 149.0,
    retailPrice: 999.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Fashion-Frill-Exclusive-Jewellery-Stainless/dp/B0BDDYY1SW/ref=sr_1_77?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-77",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription: "Men's Square Sunglasses",
    asin: "B07Y8S46XB",
    countReview: 4406,
    imgUrl: "https://m.media-amazon.com/images/I/51uYuk9R8pL._AC_UL320_.jpg",
    price: 321.0,
    retailPrice: 2299,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/elegante-Branded-Inspired-Sunglass-Silver-Black/dp/B07Y8S46XB/ref=sr_1_78?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-78",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Men's Dark Green Printed Slim Fit Cotton T-Shirt",
    asin: "B08JQLKQLG",
    countReview: 391,
    imgUrl: "https://m.media-amazon.com/images/I/61MOhOlUvdL._AC_UL320_.jpg",
    price: 379.0,
    retailPrice: 999.0,
    productRating: "3.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Urbano-Fashion-Printed-T-Shirt-aopleafhalf-drgreen-m/dp/B08JQLKQLG/ref=sr_1_79?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-79",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Men's Stylish Hooded Graphic Printed T-Shirt (Olive Green, Black)",
    asin: "B0BDVH9T4C",
    countReview: 6,
    imgUrl: "https://m.media-amazon.com/images/I/610jF2HhdNL._AC_UL320_.jpg",
    price: 449.0,
    retailPrice: 1999,
    productRating: "3.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/LEWEL-Stylish-Fashion-Graphic-Printed/dp/B0BDVH9T4C/ref=sr_1_80?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-80",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Leather Wallet for Men's Tan with Dashing Gift Box",
    asin: "B0965P1F96",
    countReview: 7,
    imgUrl: "https://m.media-amazon.com/images/I/71DizXvGIwL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 799.0,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/Fashion-Link-Leather-Wallet-Dashing/dp/B0965P1F96/ref=sr_1_81?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-81",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Saheli Fashion Men's Poly Cotton Digital Printed Stitched Half Sleeve Shirt Casual Shirt Hawaii Shirt for Men",
    asin: "B0BFRD4694",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/41T5cBaI4PL._AC_UL320_.jpg",
    price: 549.0,
    retailPrice: 1999,
    productRating: null,
    prime: true,
    dpUrl:
      "/Fashion-Digital-Printed-Stitched-Multicolor/dp/B0BFRD4694/ref=sr_1_82?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-82",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Multicolor 9",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-Multicolor/dp/B0BFRD4694/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Rainbow",
        dpUrl:
          "/Fashion-Digital-Printed-Stitched-Multicolor/dp/B0BFRCVQQZ/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-82",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Boys Silver Chain Accessories For Men Chains For Men Locket Pendant For Boys Girls Bar Pendant For Boys",
    asin: "B0BH7SNGHV",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61giPDptZOL._AC_UL320_.jpg",
    price: 139.0,
    retailPrice: 999.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Fashion-Frill-Silver-Accessories-Pendant/dp/B0BH7SNGHV/ref=sr_1_83?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-83",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [],
  },
  {
    productDescription:
      "Pendant for Men and Boys | Fashion Silver Pendants | Silver Tone Pendant | Stainless Steel Pendant Necklace Chain for Men| Accessories Jewellery for Men | Birthday Gift for Men and Boys Anniversary Gift for Husband",
    asin: "B08DXJZD5C",
    countReview: 208,
    imgUrl: "https://m.media-amazon.com/images/I/619jOTWiZRL._AC_UL320_.jpg",
    price: 399.0,
    retailPrice: 1328,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yellow-Chimes-Stainless-Locket-Pendant/dp/B08DXJZD5C/ref=sr_1_84?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-84",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [],
  },
  {
    productDescription:
      "Bracelets for Men and Boys | Fashion Gold Bracelet for Men | Gold Plated Big Bracelet for Men | Accessories Jewellery for Men | Birthday Gift for Men and Boys Anniversary Gift for Husband",
    asin: "B07XZBMKM7",
    countReview: 73,
    imgUrl: "https://m.media-amazon.com/images/I/618TD-OMmDL._AC_UL320_.jpg",
    price: 759.0,
    retailPrice: 3108,
    productRating: "4.0 out of 5 stars",
    prime: true,
    dpUrl:
      "/Yellow-Chimes-Trandy-Design-Bracelet/dp/B07XZBMKM7/ref=sr_1_85?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-85",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription: "Men's Regular Fit Button Down Shirt",
    asin: "B09XXLRJHN",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81w6flCZhJL._AC_UL320_.jpg",
    price: 1.379,
    retailPrice: 2299,
    productRating: null,
    prime: true,
    dpUrl:
      "/Association-Regular-Button-Shirt-UDSHTO0250_Navy/dp/B09XXLRJHN/ref=sr_1_86?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-86",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Street Samurai High top Flat high Street Fashion Sneakers for Men with Rubber Outsole",
    asin: "B0B4H8NZQ6",
    countReview: 24,
    imgUrl: "https://m.media-amazon.com/images/I/71BttLuQa7L._AC_UL320_.jpg",
    price: 1499,
    retailPrice: 3499,
    productRating: "4.2 out of 5 stars",
    prime: true,
    dpUrl:
      "/Bacca-Bucci-Samurai-Fashion-Sneakers/dp/B0B4H8NZQ6/ref=sr_1_87?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-87",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Black",
        dpUrl:
          "/Bacca-Bucci-Samurai-Fashion-Sneakers/dp/B0B4H8NZQ6/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/Bacca-Bucci-Samurai-Fashion-Sneakers/dp/B0B4H8QY6H/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Olive",
        dpUrl:
          "/Bacca-Bucci-Samurai-Fashion-Sneakers/dp/B0B856BXCM/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-87",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's Slim Fit Casual Shirt",
    asin: "B08R21T75D",
    countReview: 2859,
    imgUrl: "https://m.media-amazon.com/images/I/61kj0DfT6VL._AC_UL320_.jpg",
    price: 498,
    retailPrice: 1999,
    productRating: "3.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21T75D/ref=sr_1_88?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Peach",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21T75D/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Green",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R217KNH/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Grey",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21HM6J/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sea Blue",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R21D26K/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Sky Blue",
        dpUrl:
          "/IndoPrimo-Cotton-Casual-Stylish-Sleeve/dp/B08R219912/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-88",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Jewellery Stainless Steel Valentine Long Chain Platinum Necklace Silver Chain for Men & Boys Stylish -CN9136 (Silver White)",
    asin: "B079YWD4S2",
    countReview: 1661,
    imgUrl: "https://m.media-amazon.com/images/I/81peu8WSLoL._AC_UL320_.jpg",
    price: 220,
    retailPrice: 350,
    productRating: "3.8 out of 5 stars",
    prime: false,
    dpUrl:
      "/MEENAZ-Silver-Plated-Chain-Necklace/dp/B079YWD4S2/ref=sr_1_89?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-89",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription: "Analog Linear Designer Dial Men’s & Boy's Watch",
    asin: "B07KVZD6XM",
    countReview: 21585,
    imgUrl: "https://m.media-amazon.com/images/I/81BDpSIwu3L._AC_UL320_.jpg",
    price: 287.0,
    retailPrice: 1600,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Redux-Analogue-Blue-Watch-RWS0216S/dp/B07KVZD6XM/ref=sr_1_90?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-90",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [],
  },
  {
    productDescription:
      "Rings for Men and Boys | Fashion Black Ring Set for Men | Dragon Celtic Mens Ring | Stainless Steel Rings for Men Combo | Accessories Jewellery for Men | Birthday Gift for Men and Boys Anniversary Gift for Husband",
    asin: "B07YNJCLZJ",
    countReview: 1216,
    imgUrl: "https://m.media-amazon.com/images/I/61PGRfoJCZL._AC_UL320_.jpg",
    price: 175.0,
    retailPrice: 1745,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/jewelry/7124369031/ref=sr_bs_42_7124369031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
  {
    productDescription:
      "Men's Premium Digital Printed Unstitched Shirt Fabric for Men PolyBlend Material 2.25m Shirt Piece Cloth(IOU)",
    asin: "B09P6HYVC8",
    countReview: 3,
    imgUrl: "https://m.media-amazon.com/images/I/618IKre4cCL._AC_UL320_.jpg",
    price: 199.0,
    retailPrice: 999.0,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Niza-Fashion-Unstitched-Shirt-PolyBlend/dp/B09P6HYVC8/ref=sr_1_92?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-92",
    series: null,
    deliveryMessage: null,
    variations: [],
  },
  {
    productDescription:
      "Combo of Men's Regular Fit Cotton Casual Shirts (Pack of 2)",
    asin: "B0B5DYX5SP",
    countReview: 65,
    imgUrl: "https://m.media-amazon.com/images/I/61p6ZGKMq6L._AC_UL320_.jpg",
    price: 899.0,
    retailPrice: 3849,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DYX5SP/ref=sr_1_93?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Aqua & Pink",
        dpUrl:
          "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DYX5SP/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon & Black",
        dpUrl:
          "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DYF1HT/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Maroon & Light Blue",
        dpUrl:
          "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DYPHBX/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Blue & Light Blue",
        dpUrl:
          "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DWCTQX/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Dark Green & Light Blue",
        dpUrl:
          "/FINIVO-FASHION-Regular-Cotton-X-Large/dp/B0B5DXWN9S/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-93",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men's 6113 Fisherman Sandals",
    asin: "B08C39SQ79",
    countReview: 6835,
    imgUrl: "https://m.media-amazon.com/images/I/81YGP5ZLhPL._AC_UL320_.jpg",
    price: 799.0,
    retailPrice: 2499,
    productRating: "3.8 out of 5 stars",
    prime: true,
    dpUrl:
      "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08C39SQ79/ref=sr_1_94?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [
      {
        value: "Chiku",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08C39SQ79/ref=cs_sr_dp_1?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Beige",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08NZF9YWP/ref=cs_sr_dp_2?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Black",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08GGVFDPF/ref=cs_sr_dp_3?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Blue",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08KX8XJKQ/ref=cs_sr_dp_4?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Brown",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08NZ9RWXL/ref=cs_sr_dp_5?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Burgundy",
        dpUrl:
          "/Centrino-Chiku-Fisherman-Sandals-6113-01/dp/B08NZC9YSR/ref=cs_sr_dp_6?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-94",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Men Casual Shirt",
    asin: "B083X3G4VD",
    countReview: 5,
    imgUrl: "https://m.media-amazon.com/images/I/51IJVGKOICL._AC_UL320_.jpg",
    price: 1412,
    retailPrice: 1999,
    productRating: "3.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/Allen-Solly-Printed-Casual-ASSFWMOFM70201_Light/dp/B083X3G4VD/ref=sr_1_95?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-95",
    series: null,
    deliveryMessage: "FREE Delivery by Amazon",
    variations: [],
  },
  {
    productDescription:
      "Men's Stainless Steel Jewellery Set Combo Pack of 2 Cuboid Vertical Bar Pendant With 1 Rhodium Plated Rice Shape Link Neck Chain And 1 Black Finish Full Kada Bracelet For Boys and Men CO1000438",
    asin: "B0BJ6NMF64",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/616iKbzRtFL._AC_UL320_.jpg",
    price: 299.0,
    retailPrice: 1199,
    productRating: null,
    prime: false,
    dpUrl:
      "/Stainless-Jewellery-Vertical-Bracelet-CO1000438/dp/B0BJ6NMF64/ref=sr_1_96?keywords=mens+fashion&qid=1668703106&qu=eyJxc2MiOiI4LjI4IiwicXNhIjoiNy44MyIsInFzcCI6IjQuNjgifQ%3D%3D&sr=8-96",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [],
  },
];
