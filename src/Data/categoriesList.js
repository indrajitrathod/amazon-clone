import { booksProductData } from "./books";
import { computerProductData } from "./computers";
import { freshProductData } from "./freshProducts";
import { giftCardsProductData } from "./giftCards";
import { healthHouseholdAndPersonalCareProductData } from "./healthAndpersonalCare";
import { homeAndKitchenProductData } from "./homeAndKitchenProducts";
import { kidsFashionProductData } from "./kidsFashion";
import { mensFashionProductData } from "./mensFashion";
import { mobileProductData } from "./mobiles";
import { tvProductData } from "./tvs";
import { videoGamesProductData } from "./videoGames";
import { womensFashionProductData } from "./womensFashion";

export const categoryDetails = {
  fresh: [...freshProductData],
  mobiles: [...mobileProductData],
  computers: [...computerProductData],
  tvandelectronics: [...tvProductData],
  books: [...booksProductData],
  healthhouseholdandpersonalcare: [
    ...healthHouseholdAndPersonalCareProductData,
  ],
  mensfashion: [...mensFashionProductData],
  womensfashion: [...womensFashionProductData],
  kidsfashion: [...kidsFashionProductData],
  videogames: [...videoGamesProductData],
  homeandkitchenproducts: [...homeAndKitchenProductData],
  giftcards: [...giftCardsProductData],
};

export const categoryNames = {
  fresh: "Fresh",
  mobiles: "Mobile & Smart Phones",
  computers: "Computer & Accessaries",
  tvandelectronics: "Tv & Electronics",
  books: "Books",
  healthhouseholdandpersonalcare: "Health, Household & Personal Care",
  mensfashion: "Men's Fashion",
  womensfashion: "Women's Fashion",
  kidsfashion: "Kid's Fashion",
  videogames: "Video Games",
  homeandkitchenproducts: "Home & Kitchen Products",
  giftcards: "Gift Cards",
};

export const categoryDescription = {
  fresh: [
    `8 immunity nutrients - Vitamins (A, B12, C, D), Zinc, Iron, Copper and Selenium - contributes to the normal function of the immune system`,
    `It has Phosphorous (which is the building block for normal bone development) and Vitamin D (which helps in normal absorption of Calcium and Phosphorous)`,
    `It contains Protein (which helps in maintenance and growth of muscle mass), Vitamin B12/B2 (which is essential for maintaining a normal function of the nervous system) and Iodine/Iron (which support normal cognitive functions.`,
  ],
  mobiles: [
    `13+2MP dual rear AI camera with PDAF | 8MP front camera`,
    `15.7988 centimeters (6.22-inch) HD+ Dot notch display with 1520 x 720 pixels resolution and 19:9 aspect ratio | 2.5D curved glass`,
    `Memory, Storage & SIM: 2GB | 32GB internal memory expandable up to 512GB with dedicated memory card slot | Dual SIM (nano+nano) dual-standby (4G+4G)`,
    `Android Pie v9.0 operating system with 1.95GHz Snapdragon 439 octa core processor`,
    `5000mAH lithium-polymer battery`,
    `Box also includes: Power adapter, USB cable, SIM eject tool, warranty card and user guide. The box does not include earphones`,
  ],
  computers: [
    ` NEXT-GEN GRAPHICS – Experience incredible visuals to elevate all of your gameplay with an AMD Radeon RX 5500M graphics card that’s equipped with 4 GB of GDDR6 dedicated memory and PCI Express 4.0 support for speedier data-transfers.`,
    `IMMERSIVE DISPLAY – Enjoy vibrant, crystal-clear images with 178-degree wide-viewing angles on this FHD (1920 x 1080) IPS, micro-edge display; plus, this screen’s non-reflective and low-gloss design means you'll get less glare while outside in the sun or under bright lights.`,
    `UNCOMPROMISING PERFORMANCE – This thin and light laptop lacks nothing in power, packing the speedy 6-core AMD Ryzen 5 5600H mobile processor and up to 4.2 GHz max boost clock.`,
    `MEMORY AND STORAGE – With 8 GB of RAM, you’ll get a boost to system responsiveness and improved frame rates while gaming. Also, experience shorter load times and faster boot-ups thanks to your laptop computer’s 512 GB of PCIe NVMe M.2 SSD storage.`,
    `GAME UNINTERRUPTED – Keep playing as your HP laptop’s battery lasts up to 4 hours and can be recharged to 50% in approximately 30 minutes with HP Fast Charge.`,
    `WINDOWS 11 HOME AND WARRANTY – From a rejuvenated Start menu to new ways to connect, the Windows 11 OS is the place to think, express, and create in a natural way. Protect your purchase with the 1-year limited warranty. `,
  ],
  tvandelectronics: [
    `
  Resolution: 4K Ultra HD (3840x2160 ) | Refresh Rate: 60 hertz`,
    `Connectivity: 3 HDMI ports to connect set top box, Blu Ray players, gaming console | 2 USB ports to connect hard drives and other USB devices`,
    `Sound: 20 Watts Output`,
    `Display : Slim Bezel | Screen Mirroring : Yes`,
    `Smart TV Features : Certified Android | Google Play Store | Chromecast Enabled | YouTube | Google Voice Search | Bluetooth Remote | Pre-Installed Apps : Netflix | YouTube | Hotstar`,
    `Demo of this product once delivered, please directly call BPL support on 8880301111 and provide product's model name as well as seller's details mentioned on the invoice`,
    `Warranty Information: 1 year standard manufacturer warranty from BPL`,
    `Easy Returns: This product is eligible for replacement within 10 days of delivery in case of any product defects, damage or features not matching the description provided`,
  ],
  books: [
    "Reading books is a kind of enjoyment. Reading books is a good habit. We bring you a different kinds of books. You can carry this book where ever you want. It is easy to carry. It can be an ideal gift to yourself and to your loved ones. Care instruction keep away from fire.",
  ],
  healthhouseholdandpersonalcare: [
    `Durable`,
    `Exchangable`,
    `No side-effect`,
    `1 year Warranty`,
    `Quickly delivered`,
  ],
  mensfashion: [
    `Care Instructions: Read instructions from product label`,
    `Fit Type: Fitted`,
    `Man's Clothing fashion`,
    `Durable`,
    `Exchangable`,
    `Suited for all seasons`,
  ],
  womensfashion: [
    `Care Instructions: Read instructions from product label`,
    `Fit Type: Fitted`,
    `Woman's Clothing fashion`,
    `Durable`,
    `Exchangable`,
    `Suited for all seasons`,
  ],
  kidsfashion: [
    `Care Instructions: Read instructions from product label`,
    `Fit Type: Fitted`,
    `Kid's Clothing fashion`,
    `Durable`,
    `Exchangable`,
    `Suited for all seasons`,
  ],
  videogames: [
    ` Experience next-gen speed and performance with the velocity architecture, powered by a custom and integrated software`,
    `Play thousands of games from four generations with backward compatibility, including optimized titles at launch`,
    `Game Pass ultimate includes over 100 high-quality games, online multiplayer, and an EA play membership for one low monthly price (membership sold separately)`,
    `Smart delivery ensures you play the best available version of your game no matter which console you're playing on `,
  ],
  homeandkitchenproducts: [
    ` Durable`,
    `Returnable`,
    `Reliable & Safe: Our room heaters are dependable and they also come with a 2 Year warranty`,
  ],
  giftcards: [
    `Amazon Pay Gift Cards are valid for 365 days from the date of purchase and carry no fees`,
    `Gift cards have great designs for every occasion. Customers can write down their personal wishes for their loved ones`,
    `Customers can choose any denomination ranging from Rs.10-Rs.10000 as a gifting amount`,
    `Receiver can apply the 14 digit alpha-numeric code (for e.g. 8U95-Y3E8CQ-29MPQ) on amazon.in/addgiftcard and add the Amazon Pay balance in their account`,
    `Amazon Pay Gift Cards cannot be refunded or returned`,
    `Amazon Pay Gift cards are redeemable across all products on Amazon except apps, certain global store products and other Amazon Pay gift cards`,
  ],
};
