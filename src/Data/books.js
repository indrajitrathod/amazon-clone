export const booksProductData = [
  {
    productDescription: "Rich Dad Poor Dad",
    asin: "B0BKXWG93P",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/51ftpg5rBsL._AC_UY218_.jpg",
    price: 169.0,
    retailPrice: 499.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Rich-Dad-Poor-MANSOORI-BOOK/dp/B0BKXWG93P/ref=sr_1_1?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Rich-Dad-Poor-MANSOORI-BOOK/dp/B0BKXWG93P/ref=sr_1_1?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-1",
        price: 169.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Beginner's Photography Book: An introduction to digital photography and related terminology.",
    asin: "B017GUJC32",
    countReview: 8,
    imgUrl: "https://m.media-amazon.com/images/I/81W9g38fbiL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Beginners-Photography-Book-introduction-terminology-ebook/dp/B017GUJC32/ref=sr_1_omk_2?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Beginners-Photography-Book-introduction-terminology-ebook/dp/B017GUJC32/ref=sr_1_omk_2?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-2",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription: "Mental Maths - Mathematics Activity Book 2",
    asin: "9391924034",
    countReview: 23,
    imgUrl: "https://m.media-amazon.com/images/I/71dzq+sDy1L._AC_UY218_.jpg",
    price: 81.0,
    retailPrice: 95.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Mental-Maths-Mathematics-Activity-Book/dp/9391924034/ref=sr_1_3?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Mental-Maths-Mathematics-Activity-Book/dp/9391924034/ref=sr_1_3?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-3",
        price: 81.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Do It Today: Overcome Procrastination, Improve Productivity",
    asin: "0143452126",
    countReview: 3184,
    imgUrl: "https://m.media-amazon.com/images/I/61ZPDQOjw-L._AC_UY218_.jpg",
    price: 98.0,
    retailPrice: 199.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Do-Today-procrastination-productivity-meaningful/dp/0143452126/ref=sr_1_4?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/Do-Today-procrastination-productivity-meaningful/dp/0143452126/ref=sr_1_4?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-4",
        price: 98.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Do-Today-Procrastination-Productivity-Meaningful-ebook/dp/B07DRP1Q5W/ref=sr_1_4?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-4",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "The Simplest Book For Technical Analysis | Stock Market | Mukul Agrawal",
    asin: "9394200495",
    countReview: 115,
    imgUrl: "https://m.media-amazon.com/images/I/71CtcAltv+L._AC_UY218_.jpg",
    price: 240.0,
    retailPrice: 299.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Simplest-Technical-Analysis-Market-Agrawal/dp/9394200495/ref=sr_1_5?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/Simplest-Technical-Analysis-Market-Agrawal/dp/9394200495/ref=sr_1_5?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
        price: 240.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Simplest-Technical-Analysis-Market-Agrawal-ebook/dp/B0BKWQCLL4/ref=sr_1_5?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Spoken English Guru Practice Exercises Book (Basic to Advance Level - English Grammar & Spoken English Topics Practice)",
    asin: "8195757405",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/51JoR8vFa7L._AC_UY218_.jpg",
    price: 280.0,
    retailPrice: 399.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Spoken-English-Practice-Exercises-Advance/dp/8195757405/ref=sr_1_6?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it by Friday, November 25",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Spoken-English-Practice-Exercises-Advance/dp/8195757405/ref=sr_1_6?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-6",
        price: 280.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Life's Amazing Secrets: How to Find Balance and Purpose in Your Life | Inspirational Zen book on motivation, self-development & healthy living",
    asin: "0143442295",
    countReview: 18349,
    imgUrl: "https://m.media-amazon.com/images/I/81N7FmJhbhL._AC_UY218_.jpg",
    price: 160.0,
    retailPrice: 250.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Lifes-Amazing-Secrets-Balance-Purpose/dp/0143442295/ref=sr_1_7?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Hardcover",
        dpUrl:
          "/Lifes-Amazing-Secrets-Balance-Purpose/dp/0143442295/ref=sr_1_7?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
        price: 160.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Lifes-Amazing-Secrets-Balance-Purpose-ebook/dp/B07H9WSFQG/ref=sr_1_7?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
        price: 70.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Penguin-35-Collectors-Amazing-Secrets/dp/0670097667/ref=sr_1_7?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
        price: 320.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Think Straight: Change Your Thoughts, Change Your Life",
    asin: "0143452134",
    countReview: 4819,
    imgUrl: "https://m.media-amazon.com/images/I/71Yb9hJXocL._AC_UY218_.jpg",
    price: 98.0,
    retailPrice: 199.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Think-Straight-Change-your-thoughts/dp/0143452134/ref=sr_1_8?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/Think-Straight-Change-your-thoughts/dp/0143452134/ref=sr_1_8?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-8",
        price: 98.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/THINK-STRAIGHT-Change-Your-Thoughts-ebook/dp/B077NJWFR3/ref=sr_1_8?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-8",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Detective Alfa aani Ratnajadit Khanjirache Rahasya (Detective Alfa Series Book 1) (Marathi Edition)",
    asin: "B082SK78WM",
    countReview: 44,
    imgUrl: "https://m.media-amazon.com/images/I/61kVw4ao2EL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl:
      "/Detective-Ratnajadit-Khanjirache-Rahasya-Marathi-ebook/dp/B082SK78WM/ref=sr_1_9?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Detective-Ratnajadit-Khanjirache-Rahasya-Marathi-ebook/dp/B082SK78WM/ref=sr_1_9?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-9",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "इंग्रजी व्याकरण व शब्द संग्रह (English grammar by Sachin Jadhavar)",
    asin: "8194938325",
    countReview: 182,
    imgUrl: "https://m.media-amazon.com/images/I/81Y+kVnKUNL._AC_UY218_.jpg",
    price: 299.0,
    retailPrice: 510.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/%E0%A4%87%E0%A4%82%E0%A4%97%E0%A5%8D%E0%A4%B0%E0%A4%9C%E0%A5%80-%E0%A4%B5%E0%A5%8D%E0%A4%AF%E0%A4%BE%E0%A4%95%E0%A4%B0%E0%A4%A3-English-grammar-Jadhavar/dp/8194938325/ref=sr_1_10?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/%E0%A4%87%E0%A4%82%E0%A4%97%E0%A5%8D%E0%A4%B0%E0%A4%9C%E0%A5%80-%E0%A4%B5%E0%A5%8D%E0%A4%AF%E0%A4%BE%E0%A4%95%E0%A4%B0%E0%A4%A3-English-grammar-Jadhavar/dp/8194938325/ref=sr_1_10?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-10",
        price: 299.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "TT-POWER Magical Handwriting Reusable Magic Book for Kids (4 Books 2 Pen 2 Hand Grip 10 Refill) Self Deleting Practice Hand Writing With Pen Writing for kid age 3+ (SIZE- 19*13 cm)",
    asin: "B09ZDW7MPF",
    countReview: 189,
    imgUrl: "https://m.media-amazon.com/images/I/81wwMY+ezeL._AC_UY218_.jpg",
    price: 299.0,
    retailPrice: 499.0,
    productRating: "4.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/TT-POWER-Calligraphy-Practice-Deleting-Reusable/dp/B09ZDW7MPF/ref=sr_1_11?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Spiral-bound",
        dpUrl:
          "/TT-POWER-Calligraphy-Practice-Deleting-Reusable/dp/B09ZDW7MPF/ref=sr_1_11?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-11",
        price: 299.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Brain Activity Book for Kids - 200+ Activities for Age 3+",
    asin: "9391924298",
    countReview: 2129,
    imgUrl: "https://m.media-amazon.com/images/I/7175YpTSa7L._AC_UY218_.jpg",
    price: 95.0,
    retailprice: 999.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/1318081031/ref=sr_bs_11_1318081031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Brain-Activity-Book-Kids-Activities/dp/9391924298/ref=sr_1_12?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-12",
        price: 95.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Power of Your Subconscious Mind",
    asin: "8172345666",
    countReview: 62895,
    imgUrl: "https://m.media-amazon.com/images/I/71UwSHSZRnS._AC_UY218_.jpg",
    price: 86.0,
    retailPrice: 199.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/14267745031/ref=sr_bs_12_14267745031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Audible Audiobook",
        dpUrl:
          "/Power-Your-Subconscious-Mind-Success/dp/8172345666/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 86.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Power-Your-Subconscious-Mind-ebook/dp/B0773RQ55P/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Power-Your-Subconscious-Mind/dp/B08124QT1L/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Subconscious-Joseph-Hardcover-International-Bestseller/dp/9391626157/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mass Market Paperback",
        dpUrl:
          "/Power-Your-Subconscious-Mind/dp/0553233998/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Power-Your-Subconscious-Mind/dp/1469035928/ref=sr_1_13?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Brain Activity Book for Kids - 200+ Activities for Age 6+",
    asin: "9391924875",
    countReview: 40,
    imgUrl: "https://m.media-amazon.com/images/I/81r2zVlhx-L._AC_UY218_.jpg",
    price: 81.0,
    retailPrice: 95.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/1318121031/ref=sr_bs_13_1318121031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Brain-Activity-Book-Kids-Activities/dp/9391924875/ref=sr_1_14?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-14",
        price: 81.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "My First Animal Sticker Book: Exciting Sticker Book With 100 Stickers",
    asin: "9388144945",
    countReview: 1303,
    imgUrl: "https://m.media-amazon.com/images/I/817UNsZFh7L._AC_UY218_.jpg",
    price: 153.0,
    retailPrice: 225.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/My-First-Animal-Sticker-Book/dp/9388144945/ref=sr_1_15?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/My-First-Animal-Sticker-Book/dp/9388144945/ref=sr_1_15?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-15",
        price: 153.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Trading Chart Pattern [Breakout + Candlestick+Other] | Complete Book | Trading Book",
    asin: "B0B5S7XBVR",
    countReview: 38,
    imgUrl: "https://m.media-amazon.com/images/I/61jeOZ1ObXL._AC_UY218_.jpg",
    price: 189.0,
    retailPrice: 520.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/4149440031/ref=sr_bs_15_4149440031_1",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Trading-Pattern-Breakout-Candlestick-Complete/dp/B0B5S7XBVR/ref=sr_1_16?keywords=books&qid=1668701903&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-16",
        price: 189.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "How To Win Friends & Influence People, International Bestseller",
    asin: "8194899133",
    countReview: 77696,
    imgUrl: "https://m.media-amazon.com/images/I/71wrQ0bR8+S._AC_UY218_.jpg",
    price: 109.0,
    retailPrice: 150.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwMjA1NDk1NTQyOTg6OjA6Og&url=%2FFriends-Influence-People-International-Bestseller%2Fdp%2F8194899133%2Fref%3Dsr_1_33_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-33-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwMjA1NDk1NTQyOTg6OjA6Og&url=%2FFriends-Influence-People-International-Bestseller%2Fdp%2F8194899133%2Fref%3Dsr_1_33_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-33-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
        price: 109.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwMjA1NDk1NTQyOTg6OjA6Og&url=%2FFriends-Influence-People-International-Bestseller%2Fdp%2F8194899133%2Fref%3Dsr_1_33_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-33-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
        price: 109.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Complete Book of Yoga : Karma Yoga, Bhakti Yoga, Raja Yoga, Jnana Yoga",
    asin: "9389178789",
    countReview: 2218,
    imgUrl: "https://m.media-amazon.com/images/I/712qS6BINtS._AC_UY218_.jpg",
    price: 179.0,
    retailPrice: 275.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwNTA3MjQ3OTI2MDI6OjA6Og&url=%2FComplete-Book-Yoga-Karma-Bhakti%2Fdp%2F9389178789%2Fref%3Dsr_1_34_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-34-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwNTA3MjQ3OTI2MDI6OjA6Og&url=%2FComplete-Book-Yoga-Karma-Bhakti%2Fdp%2F9389178789%2Fref%3Dsr_1_34_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-34-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
        price: 179.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYXRmX25leHQ6MjAwNTA3MjQ3OTI2MDI6OjA6Og&url=%2FComplete-Book-Yoga-Karma-Bhakti%2Fdp%2F9389178789%2Fref%3Dsr_1_34_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-34-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9hdGZfbmV4dA%26psc%3D1",
        price: 179.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "101 Pencil Control Activity Book For Kids: Tracing Practise Book Age 2+",
    asin: "9354405118",
    countReview: 59,
    imgUrl: "https://m.media-amazon.com/images/I/71qsUS8qtiL._AC_UY218_.jpg",
    price: 199.0,
    retailPrice: 699.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Pencil-Control-Activity-Book-Kids/dp/9354405118/ref=sr_1_35?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-35",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Pencil-Control-Activity-Book-Kids/dp/9354405118/ref=sr_1_35?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-35",
        price: 199.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Pencil-Control-Activity-Book-Kids/dp/9354405118/ref=sr_1_35?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-35",
        price: 199.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "ADMINISTRATION & FINANCE: COMPETITIVE EXAM RESOURCE BOOK- (KVS / NVS/ EMRS) (Hindi Edition)",
    asin: "B087QGG5SD",
    countReview: 5,
    imgUrl: "https://m.media-amazon.com/images/I/71swu3jbXxL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "3.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/ADMINISTRATION-FINANCE-COMPETITIVE-RESOURCE-BOOK-ebook/dp/B087QGG5SD/ref=sr_1_36?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/ADMINISTRATION-FINANCE-COMPETITIVE-RESOURCE-BOOK-ebook/dp/B087QGG5SD/ref=sr_1_36?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-36",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/ADMINISTRATION-FINANCE-COMPETITIVE-RESOURCE-BOOK-ebook/dp/B087QGG5SD/ref=sr_1_36?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-36",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription: "General Knowledge 2023",
    asin: "9326190706",
    countReview: 615,
    imgUrl: "https://m.media-amazon.com/images/I/515RgBLaugL._AC_UY218_.jpg",
    price: 35.0,
    retailPrice: 31.5,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/9841198031/ref=sr_bs_4_9841198031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/General-Knowledge-2023-Manohar-Pandey/dp/9326190706/ref=sr_1_37?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-37",
        price: 35.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/General-Knowledge-2023-Manohar-Pandey/dp/9326190706/ref=sr_1_37?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-37",
        price: 35.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/General-Knowledge-2023-Manohar-Pandey-ebook/dp/B09V8CKSPX/ref=sr_1_37?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-37",
        price: 31.5,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Spoken English Guru Practice Exercises Book (Basic to Advance Level - English Grammar & Spoken English Topics Practice)",
    asin: "8195757405",
    countReview: 2,
    imgUrl: "https://m.media-amazon.com/images/I/51JoR8vFa7L._AC_UY218_.jpg",
    price: 280.0,
    retailPrice: 399.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Spoken-English-Practice-Exercises-Advance/dp/8195757405/ref=sr_1_38?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: "Get it by Wednesday, November 23",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Spoken-English-Practice-Exercises-Advance/dp/8195757405/ref=sr_1_38?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-38",
        price: 280.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Spoken-English-Practice-Exercises-Advance/dp/8195757405/ref=sr_1_38?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-38",
        price: 280.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Mindset - Hindi",
    asin: "938964724X",
    countReview: 454,
    imgUrl: "https://m.media-amazon.com/images/I/51xrB7WCoPL._AC_UY218_.jpg",
    price: 288.0,
    retailPrice: 399.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/MINDSET-HINDI-Carol-S-Dweck/dp/938964724X/ref=sr_1_39?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/MINDSET-HINDI-Carol-S-Dweck/dp/938964724X/ref=sr_1_39?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-39",
        price: 288.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Paperback",
        dpUrl:
          "/MINDSET-HINDI-Carol-S-Dweck/dp/938964724X/ref=sr_1_39?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-39",
        price: 288.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Mindset-Hindi-Carol-S-Dweck-ebook/dp/B083Z4C16J/ref=sr_1_39?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-39",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription: "Aap Bhi Ban Sakte Hain Intelligent Investor",
    asin: "9355211899",
    countReview: 99,
    imgUrl: "https://m.media-amazon.com/images/I/81Uu1PSLF-L._AC_UY218_.jpg",
    price: 195.0,
    retailPrice: 300.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Aap-Sakte-Hain-Intelligent-Investor/dp/9355211899/ref=sr_1_40?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-40",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/Aap-Sakte-Hain-Intelligent-Investor/dp/9355211899/ref=sr_1_40?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-40",
        price: 195.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Aap-Sakte-Hain-Intelligent-Investor/dp/9355211899/ref=sr_1_40?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-40",
        price: 195.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Sakte-Hain-Intelligent-Investor-Hindi-ebook/dp/B0BBMHH79S/ref=sr_1_40?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-40",
        price: 173.25,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "SSC GD constable 70 practice sets new pattern ( 80 MCQ per set) I latest TCS questions I Hindi medium",
    asin: "B0BMB9S9CK",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71XJM2pMHvL._AC_UY218_.jpg",
    price: 360.0,
    retailPrice: 530.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/constable-practice-pattern-latest-questions/dp/B0BMB9S9CK/ref=sr_1_41?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-41",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/constable-practice-pattern-latest-questions/dp/B0BMB9S9CK/ref=sr_1_41?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-41",
        price: 360.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/constable-practice-pattern-latest-questions/dp/B0BMB9S9CK/ref=sr_1_41?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-41",
        price: 360.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "You Can",
    asin: "9389931843",
    countReview: 2138,
    imgUrl: "https://m.media-amazon.com/images/I/813uPMOnskL._AC_UY218_.jpg",
    price: 99.0,
    retailPrice: 150.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/You-Can-George-Matthew-Adams/dp/9389931843/ref=sr_1_42?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-42",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition Hardcover",
        dpUrl:
          "/You-Can-George-Matthew-Adams/dp/9389931843/ref=sr_1_42?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-42",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/You-Can-George-Matthew-Adams/dp/9389931843/ref=sr_1_42?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-42",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/You-Can-George-Matthew-Adams-ebook/dp/B0B5YN5Y81/ref=sr_1_42?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-42",
        price: 29.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/You-Can-George-Matthew-Adams/dp/9354995608/ref=sr_1_42?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-42",
        price: 199.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "World’s Best Inspirational Books to Change Your Life (Box Set of 3 Books)",
    asin: "819489882X",
    countReview: 895,
    imgUrl: "https://m.media-amazon.com/images/I/81BfYDO0kSL._AC_UY218_.jpg",
    price: 279.0,
    retailPrice: 549.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/10533832031/ref=sr_bs_10_10533832031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfbXRmOjIwMDQ3MDQ3MjAzMDAzOjowOjo&url=%2FWorlds-Best-Inspirational-Books-Change%2Fdp%2F819489882X%2Fref%3Dsr_1_43_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-43-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9tdGY%26psc%3D1",
        price: 279.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfbXRmOjIwMDQ3MDQ3MjAzMDAzOjowOjo&url=%2FWorlds-Best-Inspirational-Books-Change%2Fdp%2F819489882X%2Fref%3Dsr_1_43_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-43-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9tdGY%26psc%3D1",
        price: 279.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Richest Man in Babylon (Deluxe Hardbound Edition)",
    asin: "9354402828",
    countReview: 32501,
    imgUrl: "https://m.media-amazon.com/images/I/81obwCFvhfL._AC_UY218_.jpg",
    price: 231.0,
    retailPrice: 499.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfbXRmOjIwMDk0NjY4NDE3MDk4OjowOjo&url=%2FRichest-Man-Babylon-Deluxe-Hardbound%2Fdp%2F9354402828%2Fref%3Dsr_1_44_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-44-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9tdGY%26psc%3D1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Hardcover",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfbXRmOjIwMDk0NjY4NDE3MDk4OjowOjo&url=%2FRichest-Man-Babylon-Deluxe-Hardbound%2Fdp%2F9354402828%2Fref%3Dsr_1_44_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-44-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9tdGY%26psc%3D1",
        price: 231.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfbXRmOjIwMDk0NjY4NDE3MDk4OjowOjo&url=%2FRichest-Man-Babylon-Deluxe-Hardbound%2Fdp%2F9354402828%2Fref%3Dsr_1_44_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-44-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9tdGY%26psc%3D1",
        price: 231.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Moral Story Books for Kids (Set of 10 Books) (Illustrated) - English Short Stories with Colourful Pictures - Bedtime Children Story Book - 3 Years to 6 Years Old Children - Read Aloud to Infants, Toddlers",
    asin: "9391129811",
    countReview: 795,
    imgUrl: "https://m.media-amazon.com/images/I/710GgrTea1L._AC_UY218_.jpg",
    price: 226.0,
    retailPrice: 350.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Moral-Story-Books-Kids-Set/dp/9391129811/ref=sr_1_45?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Moral-Story-Books-Kids-Set/dp/9391129811/ref=sr_1_45?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-45",
        price: 226.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Moral-Story-Books-Kids-Set/dp/9391129811/ref=sr_1_45?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-45",
        price: 226.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The 100+ Chanting Book FMGE/ NEXT",
    asin: "B0BJVF42N4",
    countReview: 45,
    imgUrl: "https://m.media-amazon.com/images/I/61z0GSa9DLL._AC_UY218_.jpg",
    price: 900.0,
    retailPrice: 1.1,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/100-Chanting-Book-FMGE-NEXT/dp/B0BJVF42N4/ref=sr_1_46?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-46",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Hardcover",
        dpUrl:
          "/100-Chanting-Book-FMGE-NEXT/dp/B0BJVF42N4/ref=sr_1_46?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-46",
        price: 900.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/100-Chanting-Book-FMGE-NEXT/dp/B0BJVF42N4/ref=sr_1_46?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-46",
        price: 900.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "MINDFUL ZEN HABITS: From Suffering to Happiness In 30 Days",
    asin: "8129144905",
    countReview: 205,
    imgUrl: "https://m.media-amazon.com/images/I/51cihp7YeQS._AC_UY218_.jpg",
    price: 134.0,
    retailPrice: 250.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/MINDFUL-ZEN-HABITS-Suffering-Happiness/dp/8129144905/ref=sr_1_47?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-47",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition",
        dpUrl:
          "/MINDFUL-ZEN-HABITS-Suffering-Happiness/dp/8129144905/ref=sr_1_47?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-47",
        price: 134.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/MINDFUL-ZEN-HABITS-Suffering-Happiness/dp/8129144905/ref=sr_1_47?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-47",
        price: 134.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Mindful-Zen-Habits-Mark-Reklau-ebook/dp/B0937BSM33/ref=sr_1_47?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-47",
        price: 127.3,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Animals Tales From Panchtantra: Timeless Stories for Children From Ancient India",
    asin: "9389178118",
    countReview: 3844,
    imgUrl: "https://m.media-amazon.com/images/I/81WCSu2RoJS._AC_UY218_.jpg",
    price: 163.0,
    retailPrice: 299.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/4149495031/ref=sr_bs_15_4149495031_1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Hardcover Kindle Edition",
        dpUrl:
          "/Animals-Tales-Panchtantra-Timeless-Children/dp/9389178118/ref=sr_1_48?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-48",
        price: 163.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Animals-Tales-Panchtantra-Timeless-Children/dp/9389178118/ref=sr_1_48?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-48",
        price: 163.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Animals-Tales-Panchtantra-Timeless-Children-ebook/dp/B08BKYZFG2/ref=sr_1_48?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-48",
        price: 154.85,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "SHIVAJI Maharaj CHARITRA book (Marathi Edition)",
    asin: "B09RMXCCKD",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71+PZy0ZoVL._AC_UY218_.jpg",
    price: 150.0,
    retailprice: 999.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/SHIVAJI-Maharaj-CHARITRA-book-Marathi-ebook/dp/B09RMXCCKD/ref=sr_1_49?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-49",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/SHIVAJI-Maharaj-CHARITRA-book-Marathi-ebook/dp/B09RMXCCKD/ref=sr_1_49?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-49",
        price: 150.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/SHIVAJI-Maharaj-CHARITRA-book-Marathi-ebook/dp/B09RMXCCKD/ref=sr_1_49?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-49",
        price: 150.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Hello, World! Solar System",
    asin: "0553521039",
    countReview: 8653,
    imgUrl: "https://m.media-amazon.com/images/I/91j1FZEYluL._AC_UY218_.jpg",
    price: 240.0,
    retailPrice: 399.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Hello-World-Solar-System-McDonald/dp/0553521039/ref=sr_1_50?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-50",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Board book Kindle Edition",
        dpUrl:
          "/Hello-World-Solar-System-McDonald/dp/0553521039/ref=sr_1_50?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-50",
        price: 240.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Board book",
        dpUrl:
          "/Hello-World-Solar-System-McDonald/dp/0553521039/ref=sr_1_50?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-50",
        price: 240.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Hello-World-Solar-System-ebook/dp/B00ZNE18JU/ref=sr_1_50?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-50",
        price: 228.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Little Artist Series Mandala: Copy Colour Books",
    asin: "9388144023",
    countReview: 395,
    imgUrl: "https://m.media-amazon.com/images/I/91YLx99VBUL._AC_UY218_.jpg",
    price: 57.0,
    retailPrice: 69.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl:
      "/Little-Artist-Mandala-Colour-Books/dp/9388144023/ref=sr_1_51?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-51",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Little-Artist-Mandala-Colour-Books/dp/9388144023/ref=sr_1_51?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-51",
        price: 57.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Little-Artist-Mandala-Colour-Books/dp/9388144023/ref=sr_1_51?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-51",
        price: 57.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "My First 4 in 1 Alphabet Numbers Colours Shapes: Padded Board Books",
    asin: "9387779564",
    countReview: 4056,
    imgUrl: "https://m.media-amazon.com/images/I/7124v3KPB6L._AC_UY218_.jpg",
    price: 154.0,
    retailPrice: 299.0,
    productRating: "4.3 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/15412184031/ref=sr_bs_19_15412184031_1",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Board book Kindle Edition",
        dpUrl:
          "/First-Alphabet-Numbers-Colours-Shapes/dp/9387779564/ref=sr_1_52?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-52",
        price: 154.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Board book",
        dpUrl:
          "/First-Alphabet-Numbers-Colours-Shapes/dp/9387779564/ref=sr_1_52?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-52",
        price: 154.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/First-Alphabet-Numbers-Colours-Shapes-ebook/dp/B07WZCYQTH/ref=sr_1_52?keywords=books&qid=1668701923&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-52",
        price: 146.3,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Shirdi Sai Baba: Life, Philosophy & Teachings",
    asin: "8175994711",
    countReview: 74,
    imgUrl: "https://m.media-amazon.com/images/I/81GSCCqm-EL._AC_UY218_.jpg",
    price: 139.0,
    retailPrice: 199.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDkwMzQyOTY2MDk4OjowOjo&url=%2FShirdi-Sai-Baba-Philosophy-Teachings%2Fdp%2F8175994711%2Fref%3Dsr_1_53_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-53-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDkwMzQyOTY2MDk4OjowOjo&url=%2FShirdi-Sai-Baba-Philosophy-Teachings%2Fdp%2F8175994711%2Fref%3Dsr_1_53_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-53-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
        price: 139.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDkwMzQyOTY2MDk4OjowOjo&url=%2FShirdi-Sai-Baba-Philosophy-Teachings%2Fdp%2F8175994711%2Fref%3Dsr_1_53_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-53-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
        price: 139.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Monk Who Sold His Ferrari",
    asin: "817992162X",
    countReview: 24920,
    imgUrl: "https://m.media-amazon.com/images/I/61Iz2yy2CKL._AC_UY218_.jpg",
    price: 165.0,
    retailPrice: 250.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDU0NjcwMTk2MjA0OjowOjo&url=%2FMonk-Who-Sold-His-Ferrari%2Fdp%2F817992162X%2Fref%3Dsr_1_54_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-54-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
    series: null,
    deliveryMessage: "Get it Wednesday, November 23 - Thursday, November 24",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDU0NjcwMTk2MjA0OjowOjo&url=%2FMonk-Who-Sold-His-Ferrari%2Fdp%2F817992162X%2Fref%3Dsr_1_54_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-54-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
        price: 165.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/sspa/click?ie=UTF8&spc=MTo1MDQzMzE5NTAxNzQyOTk1OjE2Njg3MDE5MjM6c3BfYnRmOjIwMDU0NjcwMTk2MjA0OjowOjo&url=%2FMonk-Who-Sold-His-Ferrari%2Fdp%2F817992162X%2Fref%3Dsr_1_54_sspa%3Fkeywords%3Dbooks%26qid%3D1668701923%26qu%3DeyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%253D%253D%26sr%3D8-54-spons%26sp_csd%3Dd2lkZ2V0TmFtZT1zcF9idGY%26psc%3D1",
        price: 165.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "5-Minute Balance Exercises for Seniors: The Illustrated Guide to Fall Prevention with Simple Home Exercises to Improve Balance and Posture & Never Fear Falling Again + 30-Day Workout Plan!",
    asin: "B0BKQX3QSN",
    countReview: 357,
    imgUrl: "https://m.media-amazon.com/images/I/716r5PGm0zL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/5-Minute-Balance-Exercises-Seniors-Illustrated-ebook/dp/B0BKQX3QSN/ref=sr_1_1?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-1",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/5-Minute-Balance-Exercises-Seniors-Illustrated-ebook/dp/B0BKQX3QSN/ref=sr_1_1?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-1",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "THE SECRETS OF ANTI-AGING & MUSCLE GROWTH: The Science behind Health and Longevity. Insight on SarmS & Peptide Protocols: Build Muscles by Losing Weight, ... Quality of Life through Healthy Habits",
    asin: "B0BHZVD52F",
    countReview: 352,
    imgUrl: "https://m.media-amazon.com/images/I/71XiZB4VymL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/SECRETS-ANTI-AGING-MUSCLE-GROWTH-Longevity-ebook/dp/B0BHZVD52F/ref=sr_1_2?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/SECRETS-ANTI-AGING-MUSCLE-GROWTH-Longevity-ebook/dp/B0BHZVD52F/ref=sr_1_2?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-2",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "The Expectant Father Survival Handbook: The Step-By-Step Pregnancy Guide for Men From the Pregnancy Announcement to the First Weeks After Birth to Become the Perfect Partner & First Time Dad",
    asin: "B0BLHX7SLP",
    countReview: 362,
    imgUrl: "https://m.media-amazon.com/images/I/718d7CBi5sL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Expectant-Father-Survival-Handbook-Step-ebook/dp/B0BLHX7SLP/ref=sr_1_3?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Expectant-Father-Survival-Handbook-Step-ebook/dp/B0BLHX7SLP/ref=sr_1_3?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-3",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Baby Tips For A New Dad: The Fool-Proof Handbook From Dad to First Time Dad That Answers the 117 Doubts Every Dad-To-Be Has From Pregnancy to Early Fatherhood | Don’t Panic: It Only Gets Better!",
    asin: "B0BK9YG5DB",
    countReview: 352,
    imgUrl: "https://m.media-amazon.com/images/I/81LA8sAjfRL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Baby-Tips-New-Dad-Dad-ebook/dp/B0BK9YG5DB/ref=sr_1_4?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Baby-Tips-New-Dad-Dad-ebook/dp/B0BK9YG5DB/ref=sr_1_4?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-4",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Tantric Sex Guide for Couples: Explore the Path of Sacred Sex to Reach the Ultimate Pleasure. Spice Up Your Sex Life Through Meditation, Breathing, and Illustrated Tantra Sex Positions",
    asin: "B09943RNCR",
    countReview: 317,
    imgUrl: "https://m.media-amazon.com/images/I/812is-ht8dL._AC_UY218_.jpg",
    price: 599.0,
    retailPrice: 2.264,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Tantric-Sex-Guide-Couples-Illustrated-ebook/dp/B09943RNCR/ref=sr_1_5?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it by Sunday, December 4",
    variations: [
      {
        value: "Kindle Edition Hardcover",
        dpUrl:
          "/Tantric-Sex-Guide-Couples-Illustrated-ebook/dp/B09943RNCR/ref=sr_1_5?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Tantric-Sex-Guide-Couples-Illustrated/dp/1803018755/ref=sr_1_5?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-5",
        price: 2.264,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Vending Machine Business: Build your Passive Income by Starting a Successful Vending Machine Business | Including Tax Guide, Hard-to-Find Secrets, and Helpful Tips from the Experts",
    asin: "B0BKR9N9L2",
    countReview: 309,
    imgUrl: "https://m.media-amazon.com/images/I/815TpMiwhRL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Vending-Machine-Business-Hard-Find-ebook/dp/B0BKR9N9L2/ref=sr_1_6?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Vending-Machine-Business-Hard-Find-ebook/dp/B0BKR9N9L2/ref=sr_1_6?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-6",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "ADHD Raising An Explosive Child: The Comprehensive Guide to Help Parents Understand, Discipline and Raise Better Children With Attention Deficit Hyperactivity Disorder Without Fighting Or Yelling.",
    asin: "B0B72FRL36",
    countReview: 253,
    imgUrl: "https://m.media-amazon.com/images/I/81tgDhSjhUL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/ADHD-Raising-Explosive-Child-Comprehensive-ebook/dp/B0B72FRL36/ref=sr_1_7?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/ADHD-Raising-Explosive-Child-Comprehensive-ebook/dp/B0B72FRL36/ref=sr_1_7?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-7",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Codependent No More: Your Complete & Practical Guidance to Stop Pleasing People and Start Loving Yourself | Fix Your Codependency and Heal Your Emotional Wounds",
    asin: "B0BJ9T6WX9",
    countReview: 245,
    imgUrl: "https://m.media-amazon.com/images/I/61Ti9RUKsYL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Codependent-More-Practical-Codependency-Emotional-ebook/dp/B0BJ9T6WX9/ref=sr_1_8?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Codependent-More-Practical-Codependency-Emotional-ebook/dp/B0BJ9T6WX9/ref=sr_1_8?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-8",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "DR. SEBI BIBLE: 12 BOOKS IN 1: Everything You Need to Know to Detox Your Body Through Dr. Sebi's Alkaline Diet, Herbal Medicine, Treatments and Cures, and Live a Long Disease-Free Life",
    asin: "B0BJ9DT6ZG",
    countReview: 247,
    imgUrl: "https://m.media-amazon.com/images/I/817EGPH8VLL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DR-SEBI-BIBLE-Everything-Disease-Free-ebook/dp/B0BJ9DT6ZG/ref=sr_1_9?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/DR-SEBI-BIBLE-Everything-Disease-Free-ebook/dp/B0BJ9DT6ZG/ref=sr_1_9?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-9",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Emotional Self Care for Black Women: A Simple Inspirational Guide To Boost Your Confidence And Start Loving Yourself. How to Overcome Anxiety, Negativity, and Self-Sabotage Behaviors, Starting Today",
    asin: "B09V387KTY",
    countReview: 218,
    imgUrl: "https://m.media-amazon.com/images/I/71+WESWZuFL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Emotional-Self-Care-Black-Women-ebook/dp/B09V387KTY/ref=sr_1_10?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Emotional-Self-Care-Black-Women-ebook/dp/B09V387KTY/ref=sr_1_10?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-10",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Low Cholesterol Cookbook: Quick & Easy Heart Healthy Recipes to Manage Blood Cholesterol Levels. Includes a 30-Day Meal Plan & Physical Exercises to Improve Your Health and Live Longer with No-Stress",
    asin: "B0BHXHH15N",
    countReview: 215,
    imgUrl: "https://m.media-amazon.com/images/I/91cYGz06NSL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Low-Cholesterol-Cookbook-Exercises-No-Stress-ebook/dp/B0BHXHH15N/ref=sr_1_11?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-11",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Low-Cholesterol-Cookbook-Exercises-No-Stress-ebook/dp/B0BHXHH15N/ref=sr_1_11?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-11",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "The Jar Spells Compendium: Discover the Power of Witch Bottles, Unleash your Magic, and Master the Witchcraft Art. Includes 120+ Spell Recipes for Love, Protection, Wealth, Healing, Luck, and More",
    asin: "B0BHZF8M5R",
    countReview: 211,
    imgUrl: "https://m.media-amazon.com/images/I/817ZGpDRy4L._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jar-Spells-Compendium-Witchcraft-Protection-ebook/dp/B0BHZF8M5R/ref=sr_1_12?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-12",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Jar-Spells-Compendium-Witchcraft-Protection-ebook/dp/B0BHZF8M5R/ref=sr_1_12?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-12",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "DARK PSYCHOLOGY AND GASLIGHTING MANIPULATION: 10 IN 1: Unmask Others and Stop Being Taken Advantage Of with Mind Control Techniques, Body Language, and Persuasion Secrets to Get What You Want",
    asin: "B0BL278WXK",
    countReview: 206,
    imgUrl: "https://m.media-amazon.com/images/I/81U5KzqUHHL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/DARK-PSYCHOLOGY-GASLIGHTING-MANIPULATION-Techniques-ebook/dp/B0BL278WXK/ref=sr_1_13?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/DARK-PSYCHOLOGY-GASLIGHTING-MANIPULATION-Techniques-ebook/dp/B0BL278WXK/ref=sr_1_13?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-13",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "The Jar Spells Compendium: Discharge Your Magic and Improve Your Art With 50+ Witch Bottles Recipes for Safety, Plenty, Love, Healing, and More",
    asin: "B0BL7Z3Y8R",
    countReview: 197,
    imgUrl: "https://m.media-amazon.com/images/I/9173fGvorgL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Jar-Spells-Compendium-Discharge-Improve-ebook/dp/B0BL7Z3Y8R/ref=sr_1_14?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-14",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Jar-Spells-Compendium-Discharge-Improve-ebook/dp/B0BL7Z3Y8R/ref=sr_1_14?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-14",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "Productive ME: Unlock the powerful secrets of productivity and eliminate procrastination",
    asin: "B0BBBLX32R",
    countReview: 193,
    imgUrl: "https://m.media-amazon.com/images/I/71Fc5+ZvQLL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Productive-ME-productivity-eliminate-procrastination-ebook/dp/B0BBBLX32R/ref=sr_1_15?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-15",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Productive-ME-productivity-eliminate-procrastination-ebook/dp/B0BBBLX32R/ref=sr_1_15?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-15",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
  {
    productDescription:
      "POSITIVE AFFIRMATIONS FOR BLACK WOMEN: DISCOVER AND REPLICATE THE STRATEGIES I USED TO ATTRACT LOVE, SUCCESS, HAPPINESS, WEALTH AND ABUNDANCE IN MY LIFE.",
    asin: "B0B65FC87K",
    countReview: 184,
    imgUrl: "https://m.media-amazon.com/images/I/816aUbVk2FL._AC_UY218_.jpg",
    price: 599.0,
    retailprice: 999.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/POSITIVE-AFFIRMATIONS-BLACK-WOMEN-STRATEGIES-ebook/dp/B0B65FC87K/ref=sr_1_16?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-16",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/POSITIVE-AFFIRMATIONS-BLACK-WOMEN-STRATEGIES-ebook/dp/B0B65FC87K/ref=sr_1_16?keywords=books&qid=1668701835&qu=eyJxc2MiOiI4LjUxIiwicXNhIjoiOC4zMSIsInFzcCI6IjguMzYifQ%3D%3D&sr=8-16",
        price: 599.0,
        prime: false,
        kindleUnlimited: true,
      },
    ],
  },
];
