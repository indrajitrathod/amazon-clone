const newReleasesProductData = [
  {
    productDescription: "American Gods Season 2 - International Release",
    asin: "B07Q6KMG5G",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81sk2YVNXXL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c6b4a8b5-207c-a3ef-a000-3dc967a5d94b%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q6KMG5G&ref=sr_1_1&keywords=new+releases&qid=1668703719&sr=8-1",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c6b4a8b5-207c-a3ef-a000-3dc967a5d94b%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q6KMG5G&ref=sr_1_1&keywords=new+releases&qid=1668703719&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c6b4a8b5-207c-a3ef-a000-3dc967a5d94b%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q6KMG5G&ref=sr_1_1&keywords=new+releases&qid=1668703719&sr=8-1",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Bhool Gaya Raja | Bankelal | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest",
    asin: "9354686125",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/61bJjhRPAKL._AC_UY218_.jpg",
    price: 500.0,
    retailPrice: 0.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Comics-Bankelal-Sanjay-Release-Latest/dp/9354686125/ref=sr_1_2?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-2",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Comics-Bankelal-Sanjay-Release-Latest/dp/9354686125/ref=sr_1_2?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-2",
        price: 500.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Comics-Bankelal-Sanjay-Release-Latest/dp/9354686125/ref=sr_1_2?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-2",
        price: 500.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Resistance Girl: An utterly gripping and heartbreaking new release from the bestselling author of world war 2 historical fiction novels for 2022",
    asin: "0008453411",
    countReview: 930,
    imgUrl: "https://m.media-amazon.com/images/I/81GqO0RloyL._AC_UY218_.jpg",
    price: 284.77,
    retailPrice: 399.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Resistance-Girl-Mandy-Robotham/dp/0008453411/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
    series: null,
    deliveryMessage: "Get it by Saturday, November 19",
    variations: [
      {
        value: "Paperback Kindle Edition Hardcover",
        dpUrl:
          "/Resistance-Girl-Mandy-Robotham/dp/0008453411/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
        price: 284.77,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Resistance-Girl-Mandy-Robotham/dp/0008453411/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
        price: 284.77,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Resistance-Girl-internationally-bestselling-historical-ebook/dp/B09F5Q1HY3/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
        price: 206.6,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Resistance-Girl-internationally-bestselling-historical/dp/0008523754/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
        price: 1968.71,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Resistance-Girl-Mandy-Robotham/dp/0008566143/ref=sr_1_3?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-3",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Nothing More to Tell: The new release from bestselling author Karen McManus",
    asin: "B09MVJM65H",
    countReview: 919,
    imgUrl: "https://m.media-amazon.com/images/I/71S2oUU69aL._AC_UY218_.jpg",
    price: 318.25,
    retailPrice: 0.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Nothing-More-Tell-Karen-McManus-ebook/dp/B09MVJM65H/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
    series: null,
    deliveryMessage: "Get it by Wednesday, November 30",
    variations: [
      {
        value: "Kindle Edition Audible Audiobook Hardcover",
        dpUrl:
          "/Nothing-More-Tell-Karen-McManus-ebook/dp/B09MVJM65H/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
        price: 318.25,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Nothing-More-Tell-Karen-McManus-ebook/dp/B09MVJM65H/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
        price: 318.25,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Audible-Nothing-More-to-Tell/dp/B09TQ2B9B6/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Nothing-More-Tell-release-bestselling/dp/0241621631/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
        price: 1207.5,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Nothing-More-Tell-Karen-McManus/dp/0241473683/ref=sr_1_4?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-4",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Yashasvi Bharat by Mohan Bhagwat Hindi New Release",
    asin: "9390366194",
    countReview: 122,
    imgUrl: "https://m.media-amazon.com/images/I/81u+nzPbo2L._AC_UY218_.jpg",
    price: 325.23,
    retailPrice: 600.0,
    productRating: "4.7 out of 5 stars",
    prime: false,
    dpUrl:
      "/Yashasvi-Bharat-Chief-Mohan-Bhagwat/dp/9390366194/ref=sr_1_5?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-5",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Hardcover Kindle Edition Paperback",
        dpUrl:
          "/Yashasvi-Bharat-Chief-Mohan-Bhagwat/dp/9390366194/ref=sr_1_5?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-5",
        price: 325.23,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Yashasvi-Bharat-Chief-Mohan-Bhagwat/dp/9390366194/ref=sr_1_5?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-5",
        price: 325.23,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Yashasvi-Bharat-%E0%A4%AA%E0%A5%8D%E0%A4%B0%E0%A4%BE%E0%A4%9A%E0%A5%80%E0%A4%A8%E0%A4%A4%E0%A4%AE%E0%A4%BE-%E0%A4%B0%E0%A4%BE%E0%A4%B7%E0%A5%8D%E0%A4%9F%E0%A5%8D%E0%A4%B0%E0%A5%80%E0%A4%AF%E0%A4%A4%E0%A4%BE-%E0%A4%B8%E0%A4%B6%E0%A4%95%E0%A5%8D%E0%A4%A4%E0%A5%80%E0%A4%95%E0%A4%B0%E0%A4%A3-ebook/dp/B08R72GWJK/ref=sr_1_5?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-5",
        price: 0.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Yashasvi-Bharat-Chief-Mohan-Bhagwat/dp/9390378214/ref=sr_1_5?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-5",
        price: 195.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Sarvsamar | Sarvnayak | Brahmand Rakshak | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest | Nagraj | Super Commando Dhruva | Doga | Tiranga | Bheriya | Parmanu",
    asin: "9354685889",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/612rD5bobKL._AC_UY218_.jpg",
    price: 480.0,
    retailPrice: 500.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sarvsamar-Sarvnayak-Brahmand-Rakshak-Commando/dp/9354685889/ref=sr_1_6?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-6",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Sarvsamar-Sarvnayak-Brahmand-Rakshak-Commando/dp/9354685889/ref=sr_1_6?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-6",
        price: 480.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Sarvsamar-Sarvnayak-Brahmand-Rakshak-Commando/dp/9354685889/ref=sr_1_6?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-6",
        price: 480.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Agnikhand | Bhokal | Agnipath | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest [Paperback] Nitin Mishra; Sanjay Gupta; Raj Comics By Sanjay Gupta and Raj Comics",
    asin: "9354682898",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61G4Nv9MOgL._AC_UY218_.jpg",
    price: 190.0,
    retailPrice: 200.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Comics-Agnikhand-Agnipath-Release-Paperback/dp/9354682898/ref=sr_1_7?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-7",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Comics-Agnikhand-Agnipath-Release-Paperback/dp/9354682898/ref=sr_1_7?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-7",
        price: 190.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Comics-Agnikhand-Agnipath-Release-Paperback/dp/9354682898/ref=sr_1_7?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-7",
        price: 190.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Sarvsamar Collector's Edition | Sarvnayak | Brahmand Rakshak | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest | Nagraj | Super Commando Dhruva | Doga | Tiranga",
    asin: "9354685315",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61CZQDXQ+WL._AC_UY218_.jpg",
    price: 680.0,
    retailPrice: 700.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sarvsamar-Collectors-Sarvnayak-Brahmand-Commando/dp/9354685315/ref=sr_1_8?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-8",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Hardcover Comic",
        dpUrl:
          "/Sarvsamar-Collectors-Sarvnayak-Brahmand-Commando/dp/9354685315/ref=sr_1_8?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-8",
        price: 680.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover Comic",
        dpUrl:
          "/Sarvsamar-Collectors-Sarvnayak-Brahmand-Commando/dp/9354685315/ref=sr_1_8?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-8",
        price: 680.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Prakoshth Ke Qaidi | Sarvnayak Vistar | Nagraj | Super Commando Dhruva | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest [Paperback] Nitin Mishra; Mandar Gangele; Anurag Singh; Raj Comics By Sanjay Gupta and Raj Comics",
    asin: "9354684424",
    countReview: 7,
    imgUrl: "https://m.media-amazon.com/images/I/61-DczHzQCL._AC_UY218_.jpg",
    price: 360.0,
    retailPrice: 400.0,
    productRating: "4.1 out of 5 stars",
    prime: false,
    dpUrl:
      "/Prakoshth-Sarvnayak-Commando-Release-Paperback/dp/9354684424/ref=sr_1_9?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-9",
    series: null,
    deliveryMessage: "Get it by Monday, November 21",
    variations: [
      {
        value: "Paperback Hardcover Comic",
        dpUrl:
          "/Prakoshth-Sarvnayak-Commando-Release-Paperback/dp/9354684424/ref=sr_1_9?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-9",
        price: 360.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Prakoshth-Sarvnayak-Commando-Release-Paperback/dp/9354684424/ref=sr_1_9?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-9",
        price: 360.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover Comic",
        dpUrl:
          "/Prakoshth-Collectors-Sarvnayak-Commando-Hardcover/dp/9354685560/ref=sr_1_9?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-9",
        price: 540.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Sarvnayak Vistaar Series Complete Set | Set of 5 Comics | New Releases | Nagraj, Doga, Dhruva, Parmanu, Tiranga, Bheriya, Bhokal | Raj Comics",
    asin: "B0BDZJM4QV",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81S6xVMzIwL._AC_UY218_.jpg",
    price: 710.0,
    retailPrice: 800.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/Sarvnayak-Vistaar-Complete-Releases-Parmanu/dp/B0BDZJM4QV/ref=sr_1_10?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-10",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Sarvnayak-Vistaar-Complete-Releases-Parmanu/dp/B0BDZJM4QV/ref=sr_1_10?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-10",
        price: 710.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Sarvnayak-Vistaar-Complete-Releases-Parmanu/dp/B0BDZJM4QV/ref=sr_1_10?keywords=new+releases&qid=1668703719&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-10",
        price: 710.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Legend of Tarzan",
    asin: "B07QCZHF8Q",
    countReview: 2940,
    imgUrl: "https://m.media-amazon.com/images/I/91eaJlPbMlS._AC_UY218_.jpg",
    price: 119.0,
    retailPrice: 0.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_11&keywords=new+releases&qid=1668703719&sr=8-11",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_11&keywords=new+releases&qid=1668703719&sr=8-11",
        price: 119.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_11&keywords=new+releases&qid=1668703719&sr=8-11",
        price: 119.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Missing",
    asin: "B07MPNMPKB",
    countReview: 17,
    imgUrl: "https://m.media-amazon.com/images/I/91fKMxbDj0L._AC_UY218_.jpg",
    price: 99.0,
    retailPrice: 0.0,
    productRating: "3.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c0b1a8bf-f45a-e03f-de8e-a51a438f9f9c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MPNMPKB&ref=sr_1_12&keywords=new+releases&qid=1668703719&sr=8-12",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c0b1a8bf-f45a-e03f-de8e-a51a438f9f9c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MPNMPKB&ref=sr_1_12&keywords=new+releases&qid=1668703719&sr=8-12",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.c0b1a8bf-f45a-e03f-de8e-a51a438f9f9c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MPNMPKB&ref=sr_1_12&keywords=new+releases&qid=1668703719&sr=8-12",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Sweetiee weds NRI",
    asin: "B07Q761ZHH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81O0YPQOrrL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_13&keywords=new+releases&qid=1668703719&sr=8-13",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_13&keywords=new+releases&qid=1668703719&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_13&keywords=new+releases&qid=1668703719&sr=8-13",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Madam Secretary - Season 2",
    asin: "B07MJVL1ST",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71g6gQAEZVL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_14&keywords=new+releases&qid=1668703719&sr=8-14",
    series: null,
    deliveryMessage: "This video is currently unavailable",
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_14&keywords=new+releases&qid=1668703719&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_14&keywords=new+releases&qid=1668703719&sr=8-14",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Roll No. 21 - Season 2",
    asin: "B07MJTP28N",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81ZtO-tO59L._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "0.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_15&keywords=new+releases&qid=1668703719&sr=8-15",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_15&keywords=new+releases&qid=1668703719&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_15&keywords=new+releases&qid=1668703719&sr=8-15",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Driving Licence",
    asin: "B08427CQQK",
    countReview: 1,
    imgUrl: "https://m.media-amazon.com/images/I/91xMNII44HL._AC_UY218_.jpg",
    price: 79.0,
    retailPrice: 0.0,
    productRating: "5.0 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.0ab7b5ab-48fc-8341-1f88-f6492e973c76%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B08427CQQK&ref=sr_1_16&keywords=new+releases&qid=1668703719&sr=8-16",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.0ab7b5ab-48fc-8341-1f88-f6492e973c76%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B08427CQQK&ref=sr_1_16&keywords=new+releases&qid=1668703719&sr=8-16",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.0ab7b5ab-48fc-8341-1f88-f6492e973c76%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B08427CQQK&ref=sr_1_16&keywords=new+releases&qid=1668703719&sr=8-16",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Legend of Tarzan",
    asin: "B07QCZHF8Q",
    countReview: 2940,
    imgUrl: "https://m.media-amazon.com/images/I/91eaJlPbMlS._AC_UY218_.jpg",
    price: 119.0,
    retailPrice: 0.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_17&keywords=new+releases&qid=1668703751&sr=8-17",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_17&keywords=new+releases&qid=1668703751&sr=8-17",
        price: 119.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.e2ad0da0-b8c9-f4d5-c82a-2430682d78c1%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QCZHF8Q&ref=sr_1_17&keywords=new+releases&qid=1668703751&sr=8-17",
        price: 119.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Mikhael",
    asin: "B07Q4WFFNF",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/91V-YRDDOiL._AC_UY218_.jpg",
    price: 79.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.82b4808c-0eee-12e8-1ff4-be7af92d37cf%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q4WFFNF&ref=sr_1_18&keywords=new+releases&qid=1668703751&sr=8-18",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.82b4808c-0eee-12e8-1ff4-be7af92d37cf%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q4WFFNF&ref=sr_1_18&keywords=new+releases&qid=1668703751&sr=8-18",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.82b4808c-0eee-12e8-1ff4-be7af92d37cf%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q4WFFNF&ref=sr_1_18&keywords=new+releases&qid=1668703751&sr=8-18",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Shylock",
    asin: "B084R99ZGP",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81C9qMGceTL._AC_UY218_.jpg",
    price: 79.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.feb819dd-7971-60f8-24a2-9a7c5b155a05%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B084R99ZGP&ref=sr_1_19&keywords=new+releases&qid=1668703751&sr=8-19",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.feb819dd-7971-60f8-24a2-9a7c5b155a05%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B084R99ZGP&ref=sr_1_19&keywords=new+releases&qid=1668703751&sr=8-19",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.feb819dd-7971-60f8-24a2-9a7c5b155a05%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B084R99ZGP&ref=sr_1_19&keywords=new+releases&qid=1668703751&sr=8-19",
        price: 79.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Mayabazar 2016",
    asin: "B086JK2T62",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/91yHdscFA3L._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.f6b87403-50c7-83f5-678a-4f5be55b763c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B086JK2T62&ref=sr_1_20&keywords=new+releases&qid=1668703751&sr=8-20",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.f6b87403-50c7-83f5-678a-4f5be55b763c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B086JK2T62&ref=sr_1_20&keywords=new+releases&qid=1668703751&sr=8-20",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.f6b87403-50c7-83f5-678a-4f5be55b763c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B086JK2T62&ref=sr_1_20&keywords=new+releases&qid=1668703751&sr=8-20",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Sweetiee weds NRI",
    asin: "B07Q761ZHH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81O0YPQOrrL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_21&keywords=new+releases&qid=1668703751&sr=8-21",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_21&keywords=new+releases&qid=1668703751&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.8cb40311-db9e-d527-80d4-cf097fc809ba%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07Q761ZHH&ref=sr_1_21&keywords=new+releases&qid=1668703751&sr=8-21",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Villain",
    asin: "B07QD35QPT",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71WQcv3K1sL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.84b44323-900f-35fa-bdbd-8cddcf59660f%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QD35QPT&ref=sr_1_22&keywords=new+releases&qid=1668703751&sr=8-22",
    series: null,
    deliveryMessage: "This video is currently unavailable",
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.84b44323-900f-35fa-bdbd-8cddcf59660f%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QD35QPT&ref=sr_1_22&keywords=new+releases&qid=1668703751&sr=8-22",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.84b44323-900f-35fa-bdbd-8cddcf59660f%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07QD35QPT&ref=sr_1_22&keywords=new+releases&qid=1668703751&sr=8-22",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Steve Jobs",
    asin: "B09W2W11FF",
    countReview: 186,
    imgUrl: "https://m.media-amazon.com/images/I/81XP0fzJdXL._AC_UY218_.jpg",
    price: 99.0,
    retailPrice: 0.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d2b67c0c-4cfe-1ed9-add4-bf68520767c6%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09W2W11FF&ref=sr_1_23&keywords=new+releases&qid=1668703751&sr=8-23",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d2b67c0c-4cfe-1ed9-add4-bf68520767c6%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09W2W11FF&ref=sr_1_23&keywords=new+releases&qid=1668703751&sr=8-23",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d2b67c0c-4cfe-1ed9-add4-bf68520767c6%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09W2W11FF&ref=sr_1_23&keywords=new+releases&qid=1668703751&sr=8-23",
        price: 99.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Madam Secretary - Season 2",
    asin: "B07MJVL1ST",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/71g6gQAEZVL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_24&keywords=new+releases&qid=1668703751&sr=8-24",
    series: null,
    deliveryMessage: "This video is currently unavailable",
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_24&keywords=new+releases&qid=1668703751&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.24ae9e85-a199-f731-5654-29bc5360417c%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJVL1ST&ref=sr_1_24&keywords=new+releases&qid=1668703751&sr=8-24",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Keeper of Stories: NEW for 2022, the most charming and uplifting novel you will read this year!",
    asin: "B08NCBM2ZF",
    countReview: 24763,
    imgUrl: "https://m.media-amazon.com/images/I/81yhsgDafPL._AC_UY218_.jpg",
    price: 163.95,
    retailPrice: 725.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Dusting-Hope-Sally-Page-ebook/dp/B08NCBM2ZF/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
    series: null,
    deliveryMessage: "Currently unavailable.",
    variations: [
      {
        value: "Kindle Edition Hardcover Paperback",
        dpUrl:
          "/Dusting-Hope-Sally-Page-ebook/dp/B08NCBM2ZF/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
        price: 163.95,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Dusting-Hope-Sally-Page-ebook/dp/B08NCBM2ZF/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
        price: 163.95,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Keeper-Stories-Sally-Page/dp/B0B92LF1WR/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Dusting-Hope-Sally-Page/dp/0008453519/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
        price: 725.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Keeper-Stories-Sally-Page/dp/B0BGSW6S2X/ref=sr_1_25?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-25",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Super Wings",
    asin: "B087NVW6P5",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81FqJZTI0lL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d6b7ed23-e7bb-4731-d731-9822bb7cc316%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B087NVW6P5&ref=sr_1_26&keywords=new+releases&qid=1668703751&sr=8-26",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d6b7ed23-e7bb-4731-d731-9822bb7cc316%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B087NVW6P5&ref=sr_1_26&keywords=new+releases&qid=1668703751&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.d6b7ed23-e7bb-4731-d731-9822bb7cc316%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B087NVW6P5&ref=sr_1_26&keywords=new+releases&qid=1668703751&sr=8-26",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Roll No. 21 - Season 2",
    asin: "B07MJTP28N",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81ZtO-tO59L._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_27&keywords=new+releases&qid=1668703751&sr=8-27",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_27&keywords=new+releases&qid=1668703751&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.3eaf03b3-f69a-7834-942a-c95fa6409a82%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJTP28N&ref=sr_1_27&keywords=new+releases&qid=1668703751&sr=8-27",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Over My Dead Body: The Next Thriller from the Sunday Times Bestselling Author, the Latest Must-Read New Book of 2021 (William Warwick Novels)",
    asin: "B08T122L5D",
    countReview: 23241,
    imgUrl: "https://m.media-amazon.com/images/I/81tzfMYmVEL._AC_UY218_.jpg",
    price: 286.9,
    retailPrice: 0.0,
    productRating: "4.3 out of 5 stars",
    prime: true,
    dpUrl:
      "/Over-Dead-Body-Bestselling-Must-Read-ebook/dp/B08T122L5D/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
    series: null,
    deliveryMessage: "Get it by Sunday, November 20",
    variations: [
      {
        value: "Kindle Edition Audible Audiobook Hardcover",
        dpUrl:
          "/Over-Dead-Body-Bestselling-Must-Read-ebook/dp/B08T122L5D/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 286.9,
        prime: true,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Over-Dead-Body-Bestselling-Must-Read-ebook/dp/B08T122L5D/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 286.9,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Over-Dead-William-Warwick-Novels/dp/B08T1MCG1X/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Over-Dead-Body-Bestselling-Must-Read/dp/0008474273/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 1098.77,
        prime: true,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Over-Dead-Body-Bestselling-Must-Read/dp/0008476403/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Over-Dead-Body-Bestselling-Must-Read/dp/000848452X/ref=sr_1_28?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-28",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Evolve: Conscious Leadership For A New Era",
    asin: "B0BMJGGFF9",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/716Qc1Wz0KL._AC_UY218_.jpg",
    price: 449.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Evolve-Conscious-Leadership-New-Era-ebook/dp/B0BMJGGFF9/ref=sr_1_29?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-29",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Kindle Edition",
        dpUrl:
          "/Evolve-Conscious-Leadership-New-Era-ebook/dp/B0BMJGGFF9/ref=sr_1_29?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-29",
        price: 449.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Evolve-Conscious-Leadership-New-Era-ebook/dp/B0BMJGGFF9/ref=sr_1_29?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-29",
        price: 449.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Brave New World: Aldous Huxley's Most Popular Dystopian Classic Novel: Aldous Huxley's Most Popular Classic Novel",
    asin: "B085GH8Q5P",
    countReview: 22383,
    imgUrl: "https://m.media-amazon.com/images/I/81cRxeSSWJL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Brave-New-World-Huxleys-Popular-ebook/dp/B085GH8Q5P/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
    series: null,
    deliveryMessage: "Get it by Wednesday, November 23",
    variations: [
      {
        value: "Kindle Edition Audible Audiobook Hardcover",
        dpUrl:
          "/Brave-New-World-Huxleys-Popular-ebook/dp/B085GH8Q5P/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Brave-New-World-Huxleys-Popular-ebook/dp/B085GH8Q5P/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: true,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Brave-New-World/dp/B01MR8495P/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Brave-New-World-Aldous-Huxley/dp/9356610916/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 273.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Brave-New-World-Aldous-Huxley/dp/9355711751/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mass Market Paperback",
        dpUrl:
          "/Brave-New-World-Modern-Classics/dp/0140010521/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Brave-New-World-Audiobooks-America/dp/1602833362/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio, Cassette",
        dpUrl:
          "/Brave-New-World-Cover-Classics/dp/1572700645/ref=sr_1_30?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-30",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Monk Who Sold His Ferrari",
    asin: "817992162X",
    countReview: 24920,
    imgUrl: "https://m.media-amazon.com/images/I/61Iz2yy2CKL._AC_UY218_.jpg",
    price: 228.0,
    retailPrice: 360.0,
    productRating: "4.5 out of 5 stars",
    prime: false,
    dpUrl:
      "/Monk-Who-Sold-His-Ferrari/dp/817992162X/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
    series: null,
    deliveryMessage: "Get it Tuesday, November 22 - Thursday, November 24",
    variations: [
      {
        value: "Paperback Paperback Bunko Kindle Edition",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/817992162X/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 228.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/817992162X/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 228.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback Bunko",
        dpUrl:
          "/Monk-Who-Sold-His-Ferari/dp/B07QSR63XN/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 131.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari-ebook/dp/B009FTAH3W/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 49.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/B072K5XLHC/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/8179929663/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mass Market Paperback",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/006112589X/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "MP3 CD",
        dpUrl:
          "/Monk-Who-Sold-His-Ferrari/dp/1489382593/ref=sr_1_31?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-31",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "The Alchemist",
    asin: "8172234988",
    countReview: 94537,
    imgUrl: "https://m.media-amazon.com/images/I/71aFt4+OTOL._AC_UY218_.jpg",
    price: 228.0,
    retailPrice: 350.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Alchemist-Paulo-Coelho/dp/8172234988/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Audible Audiobook",
        dpUrl:
          "/Alchemist-Paulo-Coelho/dp/8172234988/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 228.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/Alchemist-Paulo-Coelho/dp/8172234988/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 228.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Alchemist-Paulo-Coelho-ebook/dp/B00U6SFUSS/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 75.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Alchemist-Fable-About-Following-Dream/dp/B06X99L7W7/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Novelty Book",
        dpUrl:
          "/Alchemist-Paulo-Coelho/dp/0008144222/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Mass Market Paperback",
        dpUrl:
          "/Alchemist-25th-Anniversary-Fable-Following/dp/0062355309/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio, Cassette",
        dpUrl:
          "/Alchemist-Paulo-Coelho/dp/0553476580/ref=sr_1_32?keywords=new+releases&qid=1668703751&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-32",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Sarvsamar Collector's Edition | Sarvnayak | Brahmand Rakshak | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest | Nagraj | Super Commando Dhruva | Doga | Tiranga",
    asin: "9354685315",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61CZQDXQ+WL._AC_UY218_.jpg",
    price: 680.0,
    retailPrice: 700.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Sarvsamar-Collectors-Sarvnayak-Brahmand-Commando/dp/9354685315/ref=sr_1_33?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-33",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Hardcover Comic",
        dpUrl:
          "/Sarvsamar-Collectors-Sarvnayak-Brahmand-Commando/dp/9354685315/ref=sr_1_33?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-33",
        price: 680.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Raj Comics | Agnikhand | Bhokal | Agnipath | New Comics | Raj Comics By Sanjay Gupta | New Release | Latest [Paperback] Nitin Mishra; Sanjay Gupta; Raj Comics By Sanjay Gupta and Raj Comics",
    asin: "9354682898",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61G4Nv9MOgL._AC_UY218_.jpg",
    price: 190.0,
    retailPrice: 200.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Comics-Agnikhand-Agnipath-Release-Paperback/dp/9354682898/ref=sr_1_34?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-34",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Comics-Agnikhand-Agnipath-Release-Paperback/dp/9354682898/ref=sr_1_34?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-34",
        price: 190.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Rojulu Marayi (New)",
    asin: "B07YKG5T16",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81xYDXHGSRL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.5eb6bc07-84e2-9f7b-3037-d431f56e87c8%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07YKG5T16&ref=sr_1_35&keywords=new+releases&qid=1668703767&sr=8-35",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.5eb6bc07-84e2-9f7b-3037-d431f56e87c8%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07YKG5T16&ref=sr_1_35&keywords=new+releases&qid=1668703767&sr=8-35",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Sarvnayak Series Complete Set | Set of 15 Comics | New Releases | Nagraj, Doga, Dhruva, Parmanu, Bhokal | Raj Comics",
    asin: "B0BDZH5D9F",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/91n93XvRyoL._AC_UY218_.jpg",
    price: 2.299,
    retailPrice: 2.81,
    productRating: null,
    prime: false,
    dpUrl:
      "/Sarvnayak-Complete-Comics-Releases-Parmanu/dp/B0BDZH5D9F/ref=sr_1_36?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-36",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Sarvnayak-Complete-Comics-Releases-Parmanu/dp/B0BDZH5D9F/ref=sr_1_36?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-36",
        price: 2.299,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "Sarvnayak Vistaar Series Complete Set | Set of 5 Comics | New Releases | Nagraj, Doga, Dhruva, Parmanu, Tiranga, Bheriya, Bhokal | Raj Comics",
    asin: "B0BDZJM4QV",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81S6xVMzIwL._AC_UY218_.jpg",
    price: 710.0,
    retailPrice: 800.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/Sarvnayak-Vistaar-Complete-Releases-Parmanu/dp/B0BDZJM4QV/ref=sr_1_37?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-37",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/Sarvnayak-Vistaar-Complete-Releases-Parmanu/dp/B0BDZJM4QV/ref=sr_1_37?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-37",
        price: 710.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The New Rules of Marketing and PR: How to Use News Releases, Blogs, Podcasting, Viral Marketing and Online Media to Reach Buyers Directly",
    asin: "0470113456",
    countReview: 209,
    imgUrl: "https://m.media-amazon.com/images/I/51NN8Ip0gmL._AC_UY218_.jpg",
    price: 1.1,
    retailPrice: 2.2,
    productRating: "4.4 out of 5 stars",
    prime: true,
    dpUrl:
      "/New-Rules-Marketing-PR-Podcasting/dp/0470113456/ref=sr_1_38?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-38",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Hardcover Audible Audiobook Paperback",
        dpUrl:
          "/New-Rules-Marketing-PR-Podcasting/dp/0470113456/ref=sr_1_38?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-38",
        price: 1.1,
        prime: true,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/New-Rules-Marketing-PR-Applications/dp/B079B8WKY3/ref=sr_1_38?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-38",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/New-Rules-Marketing-PR-Podcasting/dp/0470379286/ref=sr_1_38?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-38",
        price: 1.755,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Ram Chandra Series: Boxset of 4 Books (Ram - Scion of Ikshvaku, Sita : Warrior of Mithila, Raavan : Enemy of Aryavarta, War of Lanka)",
    asin: "B0BH6FTMPT",
    countReview: 11,
    imgUrl: "https://m.media-amazon.com/images/I/91G-UhnewSL._AC_UY218_.jpg",
    price: 678.0,
    retailPrice: 899.0,
    productRating: "4.6 out of 5 stars",
    prime: false,
    dpUrl:
      "/Ram-Chandra-Ikshvaku-Warrior-Aryavarta-ebook/dp/B0BH6FTMPT/ref=sr_1_39?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-39",
    series: null,
    deliveryMessage: "Get it by Tuesday, November 22",
    variations: [
      {
        value: "Kindle Edition Paperback",
        dpUrl:
          "/Ram-Chandra-Ikshvaku-Warrior-Aryavarta-ebook/dp/B0BH6FTMPT/ref=sr_1_39?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-39",
        price: 678.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Paperback",
        dpUrl:
          "/MIM-Best-Festival-Warrior-Mithila/dp/B0BK4CHZQD/ref=sr_1_39?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-39",
        price: 899.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "The Power of A Positive Attitude: Your Road To Success",
    asin: "9389432642",
    countReview: 2727,
    imgUrl: "https://m.media-amazon.com/images/I/81JByBEqw+S._AC_UY218_.jpg",
    price: 96.0,
    retailPrice: 150.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl:
      "/Power-Positive-Attitude-Your-Success/dp/9389432642/ref=sr_1_40?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-40",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Audible Audiobook",
        dpUrl:
          "/Power-Positive-Attitude-Your-Success/dp/9389432642/ref=sr_1_40?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-40",
        price: 96.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Power-Positive-Attitude-Discovering-Success-ebook/dp/B001BY5AFE/ref=sr_1_40?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-40",
        price: 91.2,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/Power-Positive-Attitude-Discovering-Success/dp/B07BCQKSXC/ref=sr_1_40?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "MP3 CD",
        dpUrl:
          "/Power-Positive-Attitude-Discovering-Success/dp/1536627658/ref=sr_1_40?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-40",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "War of Lanka (Ram Chandra Series Book 4)",
    asin: "9356291527",
    countReview: 1579,
    imgUrl: "https://m.media-amazon.com/images/I/91+T1TAxkKL._AC_UY218_.jpg",
    price: 361.0,
    retailPrice: 499.0,
    productRating: "4.2 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/1318164031/ref=sr_bs_8_1318164031_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Audible Audiobook",
        dpUrl:
          "/War-Lanka-Ram-Chandra-Book/dp/9356291527/ref=sr_1_41?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-41",
        price: 361.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/War-Lanka-Ram-Chandra-Book-ebook/dp/B0B7J9THC7/ref=sr_1_41?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-41",
        price: 261.25,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audible Audiobook",
        dpUrl:
          "/War-Lanka-Ram-Chandra-Book/dp/B0BGYM67YR/ref=sr_1_41?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-41",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "It Starts With Us",
    asin: "1398518174",
    countReview: 36487,
    imgUrl: "https://m.media-amazon.com/images/I/81FummIc2eL._AC_UY218_.jpg",
    price: 225.0,
    retailPrice: 699.0,
    productRating: "4.4 out of 5 stars",
    prime: false,
    dpUrl: "/gp/bestsellers/books/ref=sr_bs_9_books_1",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback Kindle Edition Hardcover",
        dpUrl:
          "/Starts-Us-Colleen-Hoover/dp/1398518174/ref=sr_1_42?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-42",
        price: 225.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Kindle Edition",
        dpUrl:
          "/Starts-Us-Colleen-Hoover-ebook/dp/B09RX42621/ref=sr_1_42?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-42",
        price: 203.3,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Hardcover",
        dpUrl:
          "/Starts-Us-Colleen-Hoover/dp/1398518166/ref=sr_1_42?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-42",
        price: 399.0,
        prime: false,
        kindleUnlimited: false,
      },
      {
        value: "Audio CD",
        dpUrl:
          "/Starts-Us-Ends/dp/179714510X/ref=sr_1_42?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-42",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "American Gods Season 1 - International Release",
    asin: "B07MM9RC92",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/91bMrqB5AXL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.4aadd604-87b7-4f5e-9d0c-93c321305b0b%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MM9RC92&ref=sr_1_43&keywords=new+releases&qid=1668703767&sr=8-43",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.4aadd604-87b7-4f5e-9d0c-93c321305b0b%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MM9RC92&ref=sr_1_43&keywords=new+releases&qid=1668703767&sr=8-43",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Lizzo's Watch Out For The Big Grrrls",
    asin: "B09KFQDM1S",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81u4iq2rmZL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.5dcee1dc-f259-41be-8802-35fd24a5427e%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09KFQDM1S&ref=sr_1_44&keywords=new+releases&qid=1668703767&sr=8-44",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.5dcee1dc-f259-41be-8802-35fd24a5427e%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09KFQDM1S&ref=sr_1_44&keywords=new+releases&qid=1668703767&sr=8-44",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription:
      "SSC Maths Chapter - Wise 8000+ TCS - MCQ (Bilingual) by Gagan Pratap Sir Champion Publication",
    asin: "B0BDRDR66V",
    countReview: 301,
    imgUrl: "https://m.media-amazon.com/images/I/61KhJoNovML._AC_UY218_.jpg",
    price: 584.0,
    retailPrice: 790.0,
    productRating: "4.1 out of 5 stars",
    prime: true,
    dpUrl:
      "/SSC-Maths-Chapter-Bilingual-Publication/dp/B0BDRDR66V/ref=sr_1_45?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-45",
    series: null,
    deliveryMessage: "Get it by tomorrow, November 18",
    variations: [
      {
        value: "Paperback",
        dpUrl:
          "/SSC-Maths-Chapter-Bilingual-Publication/dp/B0BDRDR66V/ref=sr_1_45?keywords=new+releases&qid=1668703767&qu=eyJxc2MiOiIwLjAwIiwicXNhIjoiMC4wMCIsInFzcCI6IjAuMDAifQ%3D%3D&sr=8-45",
        price: 584.0,
        prime: true,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Specials Season 1",
    asin: "B09BZPQNJZ",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/61U3vwMWM8L._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.32bbc089-ae46-788b-aa05-b9e15547c447%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09BZPQNJZ&ref=sr_1_46&keywords=new+releases&qid=1668703767&sr=8-46",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.32bbc089-ae46-788b-aa05-b9e15547c447%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B09BZPQNJZ&ref=sr_1_46&keywords=new+releases&qid=1668703767&sr=8-46",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Detective Byomkesh Bakshy",
    asin: "B07MLD6QQH",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/81zWUXsW+RL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.50abea87-7a43-d3a9-cd57-4a718ab8477d%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MLD6QQH&ref=sr_1_47&keywords=new+releases&qid=1668703767&sr=8-47",
    series: null,
    deliveryMessage: null,
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.50abea87-7a43-d3a9-cd57-4a718ab8477d%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MLD6QQH&ref=sr_1_47&keywords=new+releases&qid=1668703767&sr=8-47",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
  {
    productDescription: "Mad Men - Season 6",
    asin: "B07MJQQBVK",
    countReview: 0,
    imgUrl: "https://m.media-amazon.com/images/I/91bNlCbyxGL._AC_UY218_.jpg",
    price: 0.0,
    retailPrice: 0.0,
    productRating: null,
    prime: false,
    dpUrl:
      "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.a6abed27-09c5-3a1b-66da-29653830695f%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJQQBVK&ref=sr_1_48&keywords=new+releases&qid=1668703767&sr=8-48",
    series: null,
    deliveryMessage: "This video is currently unavailable",
    variations: [
      {
        value: "Prime Video",
        dpUrl:
          "/gp/video/ssoredirect/?ru=https%3A%2F%2Fapp.primevideo.com%2Fdetail%3Fgti%3Damzn1.dv.gti.a6abed27-09c5-3a1b-66da-29653830695f%26ref_%3Ddvm_src_ret_in_xx_s&page-type-id=B07MJQQBVK&ref=sr_1_48&keywords=new+releases&qid=1668703767&sr=8-48",
        price: 0.0,
        prime: false,
        kindleUnlimited: false,
      },
    ],
  },
];
