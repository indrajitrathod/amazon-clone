import { Route, Switch } from "react-router-dom";
import Header from "./Modules/Header";
import Footer from "./Modules/Footer";
import Cart from "./Modules/Cart";
import HomePage from "./Modules/HomePage";
import Login from "./Modules/Login";
import ProductDetails from "./Modules/ProductDetails";
import SignUp from "./Modules/SignUp";
import React from "react";
import Checkout from "./Modules/Checkout";
import Category from "./Modules/Category";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isUserLoggedIn: false,
      loggedInUser: {},
      users: [],
      cart: [],
      category: undefined,
      filterSet: false,
      priceFilters: {
        min: undefined,
        max: undefined,
      },
      discountFilters: undefined,
      reviewFilters: undefined,
    };
  }

  insertUserInfo = (userDetails) => {
    const updatedUsers = [...this.state.users, userDetails];
    this.setState({
      users: updatedUsers,
    });
  };

  setUser = (userInfo) => {
    this.setState({
      isUserLoggedIn: true,
      loggedInUser: { ...userInfo },
    });
  };

  manageReviewFilters = (reviewFilters) => {
    this.setState(() => ({
      reviewFilters: reviewFilters,
    }));
  };

  manageDiscountFilters = (discountFilters) => {
    this.setState(() => ({
      discountFilters: discountFilters,
    }));
  };

  managePriceFilters = (priceFilters) => {
    this.setState(() => ({
      priceFilters: priceFilters,
    }));
  };

  filterCategory = (category) => {
    this.setState({
      filterSet: true,
      category: category,
    });
  };

  manageCart = (items) => {
    this.setState(() => ({
      cart: items,
    }));
  };

  render() {
    return (
      <>
        <Header
          isUserLoggedIn={this.state.isUserLoggedIn}
          userInfo={this.state.loggedInUser}
          cart={this.state.cart}
          manageCart={this.manageCart}
          filterCategory={this.filterCategory}
          category={this.state.category}
          filterSet={this.state.filterSet}
          priceFilters={this.state.priceFilters}
          managePriceFilters={this.managePriceFilters}
          discountFilters={this.state.discountFilters}
          manageDiscountFilters={this.manageDiscountFilters}
          reviewFilters={this.state.reviewFilters}
          manageReviewFilters={this.manageReviewFilters}
        />

        <Switch>
          <Route exact path="/">
            <HomePage filterCategory={this.filterCategory} />
          </Route>
          <Route extac path="/category/:category">
            <Category
              category={this.state.category}
              filterSet={this.state.filterSet}
              filterCategory={this.filterCategory}
              priceFilters={this.state.priceFilters}
              managePriceFilters={this.managePriceFilters}
              discountFilters={this.state.discountFilters}
              manageDiscountFilters={this.manageDiscountFilters}
              reviewFilters={this.state.reviewFilters}
              manageReviewFilters={this.manageReviewFilters}
            />
          </Route>
          <Route exact path="/cart">
            <Cart cart={this.state.cart} manageCart={this.manageCart} />
          </Route>
          <Route exact path="/checkout">
            <Checkout />
          </Route>
          <Route exact path="/productdetails/:category/:id">
            <ProductDetails
              cart={this.state.cart}
              manageCart={this.manageCart}
            />
          </Route>
          <Route exact path="/signup">
            <SignUp users={this.state.users} signUpUser={this.insertUserInfo} />
          </Route>
          <Route exact path="/login">
            <Login
              setUser={this.setUser}
              users={this.state.users}
              loggedInUser={this.state.loggedInUser}
            />
          </Route>
        </Switch>
        <Footer />
      </>
    );
  }
}

export default App;
