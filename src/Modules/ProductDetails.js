import React from "react";
import Rating from "./Rating";
import {
  categoryDescription,
  categoryDetails,
  categoryNames,
} from "../Data/categoriesList";
import { Link, withRouter } from "react-router-dom";

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      itemCount: 1,
    };
  }
  componentDidMount() {
    window.scrollTo({
      top: 0,
      behavior: "instant",
    });
  }
  manageItemCart = (event) => {
    this.setState(() => ({
      itemCount: event.target.value,
    }));
  };
  render() {
    const category = this.props.match.params.category;
    const id = this.props.match.params.id;
    const product = categoryDetails[category][id];
    const categoryName = categoryNames[category];
    const randomNumberForDeliveryDate = Math.floor(Math.random() * 9 - 3) + 3;
    const deliveryDate = new Date();
    deliveryDate.setDate(deliveryDate.getDate() + randomNumberForDeliveryDate);
    const deliveryDateInString = deliveryDate.toLocaleDateString("en-IN", {
      weekday: "long",
      day: "numeric",
      month: "long",
    });
    const rating =
      product.productRating !== null
        ? Number(product.productRating.slice(0, 3))
        : 2.1;
    const productDescriptionDetails = categoryDescription[category];
    return (
      <>
        <main>
          <div className="container my-5">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">All Categories</li>
                <span></span>
                <Link to={`/${category}`} style={{ textDecoration: "none" }}>
                  <li className="breadcrumb-item active">{categoryName}</li>
                </Link>
                <span></span>
                <li
                  style={{ width: "51ch" }}
                  className="breadcrumb-item active max-1-lines"
                  aria-current="page"
                >
                  {product.productDescription}
                </li>
              </ol>
            </nav>
            <div className="row d-flex d-lg-none">
              <div>
                <p className="col-12 text-end">
                  <Rating
                    productId={`${product.asin}_${id}`}
                    rating={{ rate: rating, count: product.countReview }}
                  />
                </p>
                <div className="text-center">
                  <h6 className="col-12 font-weight-bold text-start">
                    {product.productDescription}
                  </h6>

                  <div>
                    <img src={product.imgUrl} alt="main product detail" />
                  </div>

                  <div className="dropdown-divider mt-3"></div>

                  <div className="mb-0">
                    <h4 className="card-price">
                      <span className="sign">
                        <i className="fa fa-inr"></i>
                      </span>
                      <span className="price">{product.price}</span>
                    </h4>
                    <h6
                      className="card-price-mrp"
                      style={{ fontSize: "0.8rem" }}
                    >
                      <span>M.R.P.: </span>
                      <span className="sign">
                        <i className="fa fa-inr"></i>
                      </span>
                      <del className="price-mrp">{product.retailPrice}</del>
                    </h6>
                  </div>
                  <p>Inclusive of all taxes</p>
                </div>
                <br />
                <div
                  className="row text-center py-2"
                  style={{
                    fontSize: "12px",
                    fontWeight: "bold",
                    borderBlock: "2px solid #EDEDED",
                  }}
                >
                  <div className="col-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-cod._CB485937110_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>Pay on delivery</span>
                  </div>
                  <div className="col-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-returns._CB484059092_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>7 Day Replacement</span>
                  </div>
                  <div className="col-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-amazon-delivered._CB485933725_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>Amazon Delivered</span>
                  </div>
                  <div className="col-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-warranty._CB485935626_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>1 Year Waranty</span>
                  </div>
                </div>

                <div className="col-12 mt-3 text-start">
                  <div className="cart-box rounded float-md-right">
                    <div className="card-body">
                      <div className="form-group">
                        <h4 className="card-price">
                          <span className="sign">
                            <i className="fa fa-inr"></i>
                          </span>
                          <span className="price">{product.price}</span>
                        </h4>
                        <div className="mb-3" style={{ fontSize: "0.9rem" }}>
                          FREE delivery
                          <br />{" "}
                          <span style={{ fontWeight: "bold" }}>
                            {deliveryDateInString}.
                          </span>
                        </div>
                        <div
                          className="mb-1"
                          style={{ fontSize: "0.9rem", color: "#B12704" }}
                        >
                          Only few left in stock
                        </div>
                        <div className="mb-3" style={{ fontSize: "0.9rem" }}>
                          Delivered by Amazon.
                        </div>
                        <label htmlFor="quantity">Quantity: </label>
                        <select
                          className="ms-1"
                          id="quantity"
                          defaultValue="1"
                          onChange={this.manageItemCart}
                        >
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </div>

                      <div
                        className="mt-3 add-to-cart-button"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasBottom"
                        aria-controls="offcanvasBottom"
                        onClick={() => {
                          console.log("category:", category);
                          console.log("id", id);
                          console.log(this.props.cart);
                          const isThisItemInCart = this.props.cart.findIndex(
                            (cartItem) => {
                              return (
                                cartItem.category === category &&
                                cartItem.id === id
                              );
                            }
                          );
                          const itemIntoCart = {
                            id: id,
                            title: product.productDescription,
                            price: product.price,
                            imgUrl: product.imgUrl,
                            category: category,
                            deliveryDate: deliveryDateInString,
                            count: Number(this.state.itemCount),
                          };
                          itemIntoCart.count +=
                            isThisItemInCart >= 0
                              ? Number(this.props.cart[isThisItemInCart].count)
                              : 0;

                          const updatedCartItems =
                            isThisItemInCart >= 0
                              ? [
                                  ...this.props.cart.slice(0, isThisItemInCart),
                                  itemIntoCart,
                                  ...this.props.cart.slice(
                                    isThisItemInCart + 1,
                                    this.props.cart.length
                                  ),
                                ]
                              : [...this.props.cart, itemIntoCart];
                          this.props.manageCart(updatedCartItems);
                        }}
                      >
                        Add to cart
                      </div>
                    </div>
                  </div>
                </div>

                <div className="dropdown-divider mt-3"></div>

                <p className="text-center" style={{ fontWeight: "bold" }}>
                  Details
                </p>

                <ul className="ml-n3">
                  {productDescriptionDetails.map((productDetail) => {
                    return <li>{productDetail}</li>;
                  })}
                </ul>
              </div>
            </div>
            <div className="row d-none d-lg-flex">
              <div className="col-md-4">
                <div>
                  
                    <img src={product.imgUrl} alt="product" />
                  
                </div>
              </div>

              <div className="col-md-4">
                <h4 className="font-weight-bold">
                  {product.productDescription}
                </h4>
                <p>
                  <Rating
                    productId={`${product.asin}_${id}`}
                    rating={{ rate: rating, count: product.countReview }}
                  />
                </p>

                <div className="dropdown-divider mt-3"></div>

                <div className="mb-0">
                  <h4 className="card-price">
                    <span className="sign">
                      <i className="fa fa-inr"></i>
                    </span>
                    <span className="price">{product.price}</span>
                  </h4>
                  <h6 className="card-price-mrp" style={{ fontSize: "0.8rem" }}>
                    <span>M.R.P.: </span>
                    <span className="sign">
                      <i className="fa fa-inr"></i>
                    </span>
                    <del className="price-mrp">{product.retailPrice}</del>
                  </h6>
                </div>
                <p>Inclusive of all taxes</p>

                <div
                  className="row"
                  style={{ fontSize: "12px", fontWeight: "bold" }}
                >
                  <div className="col-md-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-cod._CB485937110_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>Pay on delivery</span>
                  </div>
                  <div className="col-md-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-returns._CB484059092_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>7 Day Replacement</span>
                  </div>
                  <div className="col-md-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-amazon-delivered._CB485933725_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>Amazon Delivered</span>
                  </div>
                  <div className="col-md-3">
                    <img
                      src="https://m.media-amazon.com/images/G/31/A2I-Convert/mobile/IconFarm/icon-warranty._CB485935626_.png"
                      width="35"
                      height="35"
                      className="rounded"
                      alt=""
                      srcSet=""
                    />
                    <br />
                    <span>1 Year Waranty</span>
                  </div>
                </div>

                <div className="dropdown-divider mt-3"></div>

                <p className="text-success font-weight-bold">In stock.</p>
                <p>
                  Sold by:
                  <span className="ms-1">ABC Seller</span>
                </p>

                <ul className="ml-n3">
                  {productDescriptionDetails.map((productDetail) => {
                    return <li>{productDetail}</li>;
                  })}
                </ul>
              </div>

              <div className="col-3 ms-3 text-start">
                <div className="cart-box rounded float-md-right">
                  <div className="card-body">
                    <div className="form-group">
                      <h4 className="card-price">
                        <span className="sign">
                          <i className="fa fa-inr"></i>
                        </span>
                        <span className="price">{product.price}</span>
                      </h4>
                      <div className="mb-3" style={{ fontSize: "0.9rem" }}>
                        FREE delivery
                        <br />{" "}
                        <span style={{ fontWeight: "bold" }}>
                          {deliveryDateInString}.
                        </span>
                      </div>
                      <div
                        className="mb-1"
                        style={{ fontSize: "0.9rem", color: "#B12704" }}
                      >
                        Only few left in stock
                      </div>
                      <div className="mb-3" style={{ fontSize: "0.9rem" }}>
                        Delivered by Amazon.
                      </div>
                      <label htmlFor="quantity">Quantity: </label>
                      <select
                        className="ms-1"
                        
                        id="quantity"
                        defaultValue="1"
                        onChange={this.manageItemCart}
                      >
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                      </select>
                    </div>

                    <div
                      className="mt-3 add-to-cart-button"
                      data-bs-toggle="offcanvas"
                      data-bs-target="#offcanvasRight"
                      aria-controls="offcanvasRight"
                      onClick={() => {
                        console.log("category:", category);
                        console.log("id", id);
                        console.log(this.props.cart);
                        const isThisItemInCart = this.props.cart.findIndex(
                          (cartItem) => {
                            return (
                              cartItem.category === category &&
                              cartItem.id === id
                            );
                          }
                        );
                        const itemIntoCart = {
                          id: id,
                          title: product.productDescription,
                          price: product.price,
                          imgUrl: product.imgUrl,
                          category: category,
                          deliveryDate: deliveryDateInString,
                          count: Number(this.state.itemCount),
                        };
                        itemIntoCart.count +=
                          isThisItemInCart >= 0
                            ? Number(this.props.cart[isThisItemInCart].count)
                            : 0;
                        
                        const updatedCartItems =
                          isThisItemInCart >= 0
                            ? [
                                ...this.props.cart.slice(0, isThisItemInCart),
                                itemIntoCart,
                                ...this.props.cart.slice(
                                  isThisItemInCart + 1,
                                  this.props.cart.length
                                ),
                              ]
                            : [...this.props.cart, itemIntoCart];
                        this.props.manageCart(updatedCartItems);
                        
                      }}
                    >
                      Add to cart
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </>
    );
  }
}

export default withRouter(ProductDetails);
