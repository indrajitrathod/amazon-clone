import React from "react";
import { Link } from "react-router-dom";
import Address from "./Address";
import Payment from "./payment";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      selectedAddress: null,
      isCheckoutSuccess: false,
    };
  }

  saveAddress = (address) => {
    let addressString = `${address.fullName},\n${address.address}\n${address.city},${address.state} ${address.pincode}`;
    this.setState({
      address: addressString,
    });
  };

  selectThisAddress = (address) => {
    this.setState({
      selectedAddress: address,
    });
  };

  makePayment = () => {
    this.setState({
      isCheckoutSuccess: true,
    });
  };

  render() {
    return (
      <>
        <header>
          <nav className="navbar bg-light mt-2">
            <div className="container">
              <h3>Checkout</h3>
            </div>
          </nav>
        </header>
        {this.state.isCheckoutSuccess && (
          <div>
            <main>
              <div className="container text-center mt-3">
                <h5>Your order is placed.</h5>
                <Link to="/">
                  <button type="button" className="btn btn-warning">
                    Go to Main Page
                  </button>
                </Link>
              </div>
            </main>
          </div>
        )}
        {!this.state.isCheckoutSuccess && (
          <div
            className="container d-flex flex-md-row flex-column "
            style={{ minHeight: "65vh" }}
          >
            <div className="col-12 col-md-8  mt-2">
              <div className="address border m-1 p-2">
                <Address
                  saveAddress={this.saveAddress}
                  selectThisAddress={this.selectThisAddress}
                  address={this.state.address}
                />
              </div>
              <div className="payment border m-1 p-2">
                <Payment
                  address={this.state.selectedAddress}
                  makePayment={this.makePayment}
                />
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Checkout;
