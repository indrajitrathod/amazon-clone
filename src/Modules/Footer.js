function Footer() {
  return (
    <div className="container-fluid bg-dark px-0 pt-3">
      <div className="dropdown-divider mt-3"></div>

      <div className="row mt-3 mx-0 px-0">
        <div className="col-md-12 text-center">
          <img
            src="amazon_logo_white.png"
            height="30"
            width="100"
            alt="amazon logo white footer"
          />
        </div>
      </div>

      <div
        className="py-3 mt-3 shadow-lg"
        style={{ backgroundColor: "#131a22", fontSize: "12px" }}
      >
        <div className="container">
          <div className="row text-white">
            <div className="col-md-3">
              <p>Get to know us</p>
              <span className="text-white">About</span> <br />
              <span className="text-white">Careers</span> <br />
              <span className="text-white">Press Release</span> <br />
              <span className="text-white">Investment</span> <br />
              <span className="text-white">Offers</span> <br />
            </div>

            <div className="col-md-3">
              <p>Connect With Us</p>
              <span className="text-white">Facebook</span> <br />
              <span className="text-white">Twitter</span> <br />
              <span className="text-white">Instagram</span> <br />
            </div>

            <div className="col-md-3">
              <p>Make Money With Us</p>
              <span className="text-white">Sell on Amazon</span> <br />
              <span className="text-white">Affiliate Marketing</span> <br />
              <span className="text-white">Fullfilment Center</span> <br />
              <span className="text-white">Advertise Your Product</span> <br />
              <span className="text-white">Amazon Pay</span> <br />
            </div>

            <div className="col-md-3">
              <p>Let Us Help You</p>
              <span className="text-white">COVID 19</span> <br />
              <span className="text-white">Your Account</span> <br />
              <span className="text-white">Returns</span> <br />
              <span className="text-white">Funding</span> <br />
              <span className="text-white">Help</span> <br />
            </div>
          </div>

          <div className="row my-5">
            <div className="col-md-12 text-center text-white">
              <p>
                <i className="far fa-copyright"></i> 1996-2020, Amazon.com Inc.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
