import React from "react";
import PaymentModal from "./paymentModal";

class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submit: false,
      cardName: "",
    };
  }
  onSubmit = (name) => {
    console.log("entered submit");
    this.setState({
      submit: true,
      cardName: name,
    });
  };

  render() {
    const address = this.props.address;
    return (
      <div className="container ">
        <h5 className={address ? "mt-2" : "mt-2 text-muted"}>Payment</h5>
        <div className={address ? "d-block" : "d-none"}>
          <p className="lead fs-6">Enter Your card details</p>
          {!this.state.submit ? (
            <button
              className="btn btn-warning"
              type="button"
              data-bs-toggle="modal"
              data-bs-target="#carddetails"
            >
              Enter card details
            </button>
          ) : (
            <div>
              <div className="row">
                <p className="col-8">Name on Card:</p>
                <span className="col-4">{this.state.cardName}</span>
              </div>
              <button
                className="btn btn-warning"
                type="button"
                onClick={() => {
                  this.props.makePayment();
                }}
              >
                Make payment
              </button>
            </div>
          )}
          <PaymentModal onSubmit={this.onSubmit} />
        </div>
      </div>
    );
  }
}

export default Payment;
