import MainResult from "./MainResult"

function HomePage() {
  return (
    <>
      <main>
        <MainResult />
      </main>
    </>
  );
}

export default HomePage;
