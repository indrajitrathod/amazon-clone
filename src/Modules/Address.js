import React from "react";
import AddressModal from "./AddressModal";

class Address extends React.Component {
  selectThisAddress = () => {
    this.props.selectThisAddress(this.props.address);
  };

  render() {
    return (
      <div className="container">
        <h5 className="mt-2">Delivery Address</h5>
        <div>
          {this.props.address ? (
            <div className="addresses row">
              <p className="lead fs-6 col-10">{this.props.address}</p>
              <button
                className="btn btn-warning col-2 p-1 "
                type="button"
                onClick={(event) => {
                  this.selectThisAddress();
                }}
              >
                Use this
              </button>
            </div>
          ) : (
            <p>No addresses</p>
          )}
          <button
            className="btn btn-warning"
            type="button"
            
            data-bs-toggle="modal"
            data-bs-target="#AddressModal"
          >
            + Add new address
          </button>
        </div>
        <AddressModal saveAddress={this.props.saveAddress} />
      </div>
    );
  }
}

export default Address;
