import React from "react";
import Rating from "./Rating";

class Product extends React.Component {
  render() {
    const { productId, title, image, price, rating, description } = this.props;
    return (
      <div className="product-card">
        <div className="product-image">
          <img src={image} alt="" />
        </div>
        <div className="product-details">
          <h3>{title}</h3>
          <Rating rating={rating} productId={productId}/>
          <h4>
            <small>$</small>
            <span className="price">{Number(price).toFixed(2)}</span>
          </h4>
          <p>{description}</p>
        </div>
      </div>
    );
  }
}

export default Product;
