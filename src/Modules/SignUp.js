import React from "react";
import { Link } from "react-router-dom";
import validator from "validator";

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isStageOneDone: false,
      isStageTwoDone: false,
      isOTPValid: false,
      isOTPSet: false,
      isUserNameValid: false,
      isPhoneNumberValid: false,
      isEmailValid: false,
      isPasswordValid: false,
      isUserNameSet: false,
      isPhoneNumberSet: false,
      isEmailSet: false,
      isPasswordSet: false,
      userName: "",
      phoneNumber: "",
      email: "",
      password: "",
      otpGenerated: "",
      otp: "",
      userAlreadyExist: false,
    };
  }

  handleOTP = (event) => {
    this.setState(() => ({
      otp: event.target.value,
      isOTPSet: true,
    }));
  };

  handleUserName = (event) => {
    this.setState(() => ({
      isUserNameValid:
        !validator.isEmpty(event.target.value) &&
        validator.isAlpha(event.target.value)
          ? true
          : false,
    }));
    this.setState(() => ({
      userName: event.target.value,
      isUserNameSet: true,
    }));
  };

  handlePhone = (event) => {
    const isPhoneValid =
      validator.isNumeric(event.target.value) &&
      validator.isLength(event.target.value, { min: 10, max: 10 });

    this.setState({
      isPhoneNumberSet: true,
      isPhoneNumberValid: isPhoneValid,
      phoneNumber: event.target.value,
    });
  };

  handleEmail = (event) => {
    const isEmailValid = validator.isEmail(event.target.value);
    this.setState({
      isEmailValid: isEmailValid,
      email: event.target.value,
      isEmailSet: true,
    });
  };

  signUpUser = () => {
    const userInfo = {
      name: this.state.userName,
      phone: this.state.phoneNumber,
      email: this.state.email,
      password: this.state.password,
    };
    this.props.signUpUser(userInfo);
  };

  handlePassword = (event) => {
    const isPasswordValid = validator.isLength(event.target.value, {
      min: 6,
    });

    this.setState({
      isPasswordSet: true,
      isPasswordValid: isPasswordValid,
      password: event.target.value,
    });
  };

  handleFormSubmit = (event) => {
    event.preventDefault();
    const userInUsersIndex = this.props.users.findIndex((user) => {
      return (
        user.phone === this.state.phoneNumber || user.email === this.state.email
      );
    });

    if (userInUsersIndex >= 0) {
    } else {
      const otp = Math.floor(Math.random() * 1000000 + 100000);
      console.log("OTP: ", otp);
      this.setState({
        isStageOneDone: true,
        otpGenerated: otp,
      });
    }
  };

  handleOTPSubmit = (event) => {
    event.preventDefault();
    const isOTPEnteredValid =
      this.state.otpGenerated === Number(this.state.otp);
    const isStageTwoDone = isOTPEnteredValid ? true : false;

    this.setState({
      isOTPValid: isOTPEnteredValid,
      isStageTwoDone: isStageTwoDone,
    });
  };

  render() {
    return (
      <>
        <div
          className="container center"
          style={{ width: "25rem", height: "80vh" }}
        >
          <div className="logo-center my-3">
            <img
              src="amazon_logo_black.png"
              height="30"
              width="100"
              alt="amazon logo white"
            />
          </div>
          {!this.state.isStageOneDone && (
            <div className="card" style={{ height: "auto" }}>
              <div className="card-body">
                <h4 className="card-title">Create Account</h4>
                {this.state.userAlreadyExist && (
                  <h6 style={{ color: "red" }}>User already exists</h6>
                )}
                <form
                  className="needs-validation"
                  autoComplete="off"
                  noValidate
                  onSubmit={this.handleFormSubmit}
                >
                  <div className="form-group my-2">
                    <label htmlFor="userName">Your Name: </label>
                    <input
                      type="text"
                      name="userName"
                      value={this.state.userName}
                      className="form-control"
                      placeholder="Enter your name"
                      id="userName"
                      aria-describedby="userName"
                      onChange={this.handleUserName}
                    />
                    {!this.state.isUserNameSet && (
                      <small id="emailHelp" className="form-text text-muted">
                        <i className="fas text-primary fa-info"></i> Enter Valid
                        Name
                      </small>
                    )}

                    {this.state.isUserNameSet && this.state.isUserNameValid && (
                      <div
                        className="valid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="far text-success fa-thumbs-up"></i> OK
                      </div>
                    )}

                    {this.state.isUserNameSet && !this.state.isUserNameValid && (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                        Enter a Valid User name
                      </div>
                    )}
                  </div>
                  <div className="form-group my-2">
                    <label htmlFor="userMobile">Mobile No: </label>
                    <div className="d-flex flex-row mt-1">
                      <select
                        className="clickable me-1"
                        aria-label="Default select example"
                      >
                        <option defaultValue="91">IN: +91</option>
                      </select>
                      <div>
                        <input
                          type="text"
                          value={this.state.phoneNumber}
                          className="form-control"
                          name="userMobile"
                          id="userMobile"
                          aria-describedby="userEmailHelp"
                          onChange={this.handlePhone}
                        />
                        {!this.state.isPhoneNumberSet && (
                          <small
                            id="passwordHelp"
                            className="form-text text-muted"
                          >
                            <i className="fas text-primary fa-info"></i> Enter a
                            valid Mobile No
                          </small>
                        )}

                        {this.state.isPhoneNumberSet &&
                          this.state.isPhoneNumberValid && (
                            <div
                              className="valid-feedback"
                              style={{ display: "block" }}
                            >
                              <i className="far text-success fa-thumbs-up"></i>{" "}
                              OK
                            </div>
                          )}

                        {this.state.isPhoneNumberSet &&
                          !this.state.isPhoneNumberValid && (
                            <div
                              className="invalid-feedback"
                              style={{ display: "block" }}
                            >
                              <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                              Enter a Valid 10 digit Phone No
                            </div>
                          )}
                      </div>
                    </div>
                  </div>
                  <div className="form-group my-2">
                    <label htmlFor="userEmail">Email: </label>
                    <input
                      type="email"
                      className="form-control"
                      value={this.state.email}
                      name="userEmail"
                      id="userEmail"
                      aria-describedby="userEmailHelp"
                      onChange={this.handleEmail}
                    />
                    {!this.state.isEmailSet && (
                      <small id="passwordHelp" className="form-text text-muted">
                        <i className="fas text-primary fa-info"></i> Enter a
                        valid Email
                      </small>
                    )}
                    {this.state.isEmailSet && this.state.isEmailValid && (
                      <div
                        className="valid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="far text-success fa-thumbs-up"></i> OK
                      </div>
                    )}

                    {this.state.isEmailSet && !this.state.isEmailValid && (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                        Enter a Valid Email Id
                      </div>
                    )}
                  </div>
                  <div className="form-group my-2">
                    <label htmlFor="userPassword">Password: </label>
                    <input
                      type="password"
                      className="form-control"
                      placeholder="******"
                      value={this.state.password}
                      name="password"
                      id="userPassword"
                      aria-describedby="passwordHelp"
                      onChange={this.handlePassword}
                    />
                    {!this.state.isPasswordSet && (
                      <small id="passwordHelp" className="form-text text-muted">
                        <i className="fas text-primary fa-info"></i> Password
                        must be at least 6 characters.
                      </small>
                    )}

                    {this.state.isPasswordSet && this.state.isPasswordValid && (
                      <div
                        className="valid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="far text-success fa-thumbs-up"></i> OK
                      </div>
                    )}

                    {this.state.isPasswordSet && !this.state.isPasswordValid && (
                      <div
                        className="invalid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                        Password Should be Minimum 6 Characters
                      </div>
                    )}
                  </div>
                  {this.state.isUserNameValid &&
                    this.state.isPhoneNumberValid &&
                    this.state.isEmailValid &&
                    this.state.isPasswordValid && (
                      <button
                        type="submit"
                        className="btn btn-warning form-control"
                      >
                        Continue
                      </button>
                    )}
                  {(!this.state.isUserNameValid ||
                    !this.state.isPhoneNumberValid ||
                    !this.state.isEmailValid ||
                    !this.state.isPasswordValid) && (
                    <button
                      type="submit"
                      className="btn btn-warning form-control"
                      disabled
                    >
                      Continue
                    </button>
                  )}
                </form>

                <hr className="hr-text" data-bs-content="New to Amazon?" />
                <Link to="/login">
                  <button className="btn btn-dark btn-sm w-100 mt-2">
                    Already have an account? Sign in
                  </button>
                </Link>
              </div>
            </div>
          )}
          {this.state.isStageOneDone && !this.state.isStageTwoDone && (
            <div className="card" style={{ height: "auto" }}>
              <div className="card-body">
                <h4 className="card-title">Create Account</h4>
                <form
                  className="needs-validation"
                  autoComplete="off"
                  noValidate
                  onSubmit={this.handleOTPSubmit}
                >
                  <div className="form-group my-2">
                    <label htmlFor="otp">OTP: </label>
                    <input
                      type="text"
                      name="otp"
                      value={this.state.otp}
                      className="form-control"
                      placeholder="Enter 6 Digit OTP"
                      id="otp"
                      aria-describedby="otp"
                      onChange={this.handleOTP}
                    />
                  </div>

                  {this.state.isOTPValid && (
                    <Link
                      to="/"
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      <button
                        className="btn btn-warning form-control"
                        onClick={this.signUpUser}
                      >
                        Create Account
                      </button>
                    </Link>
                  )}
                  {!this.state.isOTPValid && (
                    <button
                      type="submit"
                      className="btn btn-warning form-control"
                      onClick={this.signUpUser}
                    >
                      Create Account
                    </button>
                  )}
                </form>

                <hr className="hr-text" data-bs-content="New to Amazon?" />
                <Link to="/login">
                  <button className="btn btn-dark btn-sm w-100 mt-2">
                    Already have an account? Sign in
                  </button>
                </Link>
              </div>
            </div>
          )}
          {this.state.isStageTwoDone && (
            <div className="card" style={{ height: "auto" }}>
              <div className="card-body">
                <h4 className="card-title">Account Created</h4>
                <Link to="/login">
                  <button className="btn btn-warning form-control">
                    Sign in to your account
                  </button>
                </Link>

                <hr className="hr-text" data-bs-content="New to Amazon?" />
                <Link to="/">
                  <button className="btn btn-warning form-control">
                    Go to home page
                  </button>
                </Link>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default SignUp;
