// import { ProductData } from "../newProduct";
import React from "react";
import { Link } from "react-router-dom";
import { categoryDetails } from "../Data/categoriesList";
import LeftSideFilters from "./LeftSideFilters";
import Rating from "./Rating";

class ProductList extends React.Component {
  render() {
    const allProducts = this.props.products;
    const category = this.props.category;

    const products = allProducts
      .filter((product) => {
        return product.productRating !== null &&
          this.props.reviewFilters !== undefined
          ? Number(product.productRating.slice(0, 3)) >=
              Number(this.props.reviewFilters)
          : true;
      })
      .filter((product) => {
        return this.props.discountFilters !== undefined
          ? (Number(product.retailPrice) - Number(product.price))/Number(product.retailPrice)*100 >=
              Number(this.props.discountFilters)
          : true;
      })
      .filter((product) => {
        return this.props.priceFilters.min !== undefined &&
          this.props.priceFilters.max !== undefined
          ? Number(product.price) >= this.props.priceFilters.min &&
              Number(product.price) <= this.props.priceFilters.max
          : true;
      });

    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="d-none d-lg-block col mx-3">
            <LeftSideFilters
              category={this.props.category}
              filterCategory={this.props.filterCategory}
              priceFilters={this.props.priceFilters}
              managePriceFilters={this.props.managePriceFilters}
              discountFilters={this.props.discountFilters}
              manageDiscountFilters={this.props.manageDiscountFilters}
              reviewFilters={this.props.reviewFilters}
              manageReviewFilters={this.props.manageReviewFilters}
            />
          </div>
          <div className="d-none d-lg-block col-10">
            <div>
              {products.length !== 0 && (
                <h4
                  className="text-start"
                  style={{ textTransform: "uppercase", fontWeight: "normal" }}
                >
                  Results{" "}
                  <span style={{ fontSize: "18px" }}>
                    ({products.length} products found)
                  </span>
                </h4>
              )}
              {products.length === 0 && (
                <h4
                  className="text-start"
                  style={{ textTransform: "uppercase", fontWeight: "normal" }}
                >
                  No Products Found
                </h4>
              )}
            </div>
            <div className="row">
              {products.map((product, index) => {
                const id = categoryDetails[category].findIndex(
                  (productData) => {
                    return (
                      productData.productDescription ===
                      product.productDescription
                    );
                  }
                );
                const rating =
                  product.productRating !== null
                    ? Number(product.productRating.slice(0, 3))
                    : 2.1;
                return (
                  <div className="col" key={`${product.asin}_${index}`}>
                    <Link
                      to={`/productdetails/${category}/${id}`}
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      <div className="card mb-3 p-3" style={{ width: "18rem" }}>
                        <div className="card-img-container">
                          <img
                            src={product.imgUrl}
                            className="card-img-top card-img"
                            alt="..."
                          />
                        </div>
                        <div className="card-body">
                          <div className="card-text">
                            <h4 className="card-price">
                              <span className="sign">
                                <i className="fa fa-inr"></i>
                              </span>
                              <span className="price">{product.price}</span>
                            </h4>
                            <h6 className="card-price-mrp">
                              <span className="sign">
                                <i className="fa fa-inr"></i>
                              </span>
                              <del className="price-mrp">
                                {product.retailPrice}
                              </del>
                            </h6>
                            <p className="max-2-lines">
                              {product.productDescription}
                            </p>
                            <Rating
                              productId={`${product.asin}_${index}`}
                              rating={{
                                rate: rating,
                                count: product.countReview,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="d-block d-lg-none col-12  text-center">
            <div>
              <h4 style={{ textTransform: "uppercase", fontWeight: "normal" }}>
                Results
              </h4>
            </div>
            <div className="row">
              {products.map((product, index) => {
                const id = categoryDetails[category].findIndex(
                  (productData) => {
                    return (
                      productData.productDescription ===
                      product.productDescription
                    );
                  }
                );
                const rating =
                  product.productRating !== null
                    ? Number(product.productRating.slice(0, 3))
                    : 2.1;
                return (
                  <div
                    className="col"
                    align="center"
                    key={`${product.asin}_${index}`}
                  >
                    <Link
                      to={`/productdetails/${category}/${id}`}
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      <div className="card mb-3 p-3" style={{ width: "18rem" }}>
                        <div className="card-img-container">
                          <img
                            src={product.imgUrl}
                            className="card-img-top card-img"
                            alt="..."
                          />
                        </div>
                        <div className="card-body">
                          <div className="card-text">
                            <h4 className="card-price">
                              <span className="sign">
                                <i className="fa fa-inr"></i>
                              </span>
                              <span className="price">{product.price}</span>
                            </h4>
                            <h6 className="card-price-mrp">
                              <span className="sign">
                                <i className="fa fa-inr"></i>
                              </span>
                              <del className="price-mrp">
                                {product.retailPrice}
                              </del>
                            </h6>
                            <p className="max-2-lines">
                              {product.productDescription}
                            </p>
                            <Rating
                              productId={`${product.asin}_${index}`}
                              rating={{
                                rate: rating,
                                count: product.countReview,
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductList;
