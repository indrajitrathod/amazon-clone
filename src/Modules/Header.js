import React from "react";
import { Link } from "react-router-dom";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOffCanvasOpen: false,
    };
  }
  closeOffCanvas = () => {
    this.manageCategory();
    this.setState(() => ({
      isOffCanvasOpen: false,
    }));
  };

  manageCategory = () => {
    this.props.filterCategory(undefined);
    this.props.managePriceFilters({
      min: undefined,
      max: undefined,
    });
    this.props.manageDiscountFilters(undefined);
    this.props.manageReviewFilters(undefined);
  };

  render() {
    const total = this.props.cart.reduce(
      (totalObject, item) => {
        totalObject.count += Number(item.count);
        totalObject.price += Number(item.price) * Number(item.count);
        return totalObject;
      },
      { count: 0, price: 0 }
    );
    return (
      <div className="container-fluid bg-dark py-1 sticky-top">
        <div className="row align-center">
          <div className="col">
            <div className="d-lg-none d-inline-flex" style={{ color: "#fff" }}>
              <button
                className="btn-nostyle me-2"
                data-bs-target="#offcanvasExample"
                aria-controls="offcanvasExample"
                onClick={() => {
                  this.setState(() => ({
                    isOffCanvasOpen: true,
                  }));
                }}
              >
                <i className="fa fa-bars"></i>
              </button>
            </div>
            <img
              src="amazon_logo_white.png"
              height="30"
              width="100"
              alt="amazon logo white"
            />
          </div>
          <div className="col order-lg-2">
            <ul className="navbar-nav float-end navbar-cart">
              {!this.props.isUserLoggedIn && (
                <li
                  className="px-2"
                  style={{ color: "white", fontSize: "15px" }}
                >
                  <Link
                    to="/login"
                    style={{ textDecoration: "none", color: "inherit" }}
                  >
                    <span
                      className="nav-link navbar-cart no-break"
                      aria-disabled="true"
                    >
                      Sign In
                      <i
                        className="fa-solid fa-circle-user mx-2"
                        style={{ fontSize: "21px" }}
                      ></i>
                    </span>
                  </Link>
                </li>
              )}
              {this.props.isUserLoggedIn && (
                <li
                  className="px-2"
                  style={{ color: "white", fontSize: "15px" }}
                >
                  
                    <span
                      className="nav-link navbar-cart no-break"
                      aria-disabled="true"
                    >
                      Hi, {this.props.userInfo.name}
                      <i
                        className="fa-solid fa-circle-user mx-2"
                        style={{ fontSize: "21px" }}
                      ></i>
                    </span>
                  
                </li>
              )}

              <li className="px-2">
                <span className="nav-link" aria-disabled="true">
                  <button
                    className="btn-nostyle"
                    type="button"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasRight"
                    aria-controls="offcanvasRight"
                  >
                    <i className="fas fa-2x text-light fa-shopping-cart"></i>
                  </button>
                </span>
              </li>
            </ul>
          </div>
          <div className="col col-12 col-lg-9 order-lg-1">
            <form className="form-inline px-lg-5" noValidate method="get">
              <div className="input-group">
                <div className="input-group-prepend">
                  <div className="dropdown">
                    <button
                      className="btn btn-secondary dropdown-toggle"
                      name="btnCategory"
                      type="button"
                      id="btnCategoryDropdownMenu"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      All
                    </button>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="btnCategoryDropdownMenu"
                    >
                      <span className="dropdown-item">All</span>
                      <span className="dropdown-item">Smartphone</span>
                      <span className="dropdown-item">Kitchen Hardware</span>
                      <span className="dropdown-item">Prime Deals</span>
                      <span className="dropdown-item">Book</span>
                    </div>
                  </div>
                </div>
                <input
                  type="text"
                  className="form-control"
                  size="50"
                  name="query"
                  id="query"
                />
                
                <input type="text" name="category" id="category" hidden />
                <div className="input-group-append">
                  <button type="submit" className="btn btn-warning">
                    <i className="fas fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="row category-header pt-2 pb-2 d-none d-lg-flex">
          <div className="col ps-0 ms-1">
            <button
              className="btn-nostyle ms-3"
              data-bs-target="#offcanvasExample"
              aria-controls="offcanvasExample"
              onClick={() => {
                this.setState(() => ({
                  isOffCanvasOpen: true,
                }));
              }}
            >
              <i className="fa fa-bars"></i>
            </button>
          </div>

          <div className="col no-break">
            <Link
              to="/category/fresh"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Fresh
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/mobiles"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Mobiles
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/computers"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Computers
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/tvandelectronics"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Tv & Electronics
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/mensfashion"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Fashion
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/books"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Books
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/healthhouseholdandpersonalcare"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Household
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/videogames"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Video Games
            </Link>
          </div>
          <div className="col no-break">
            <Link
              to="/category/giftcards"
              className="top-menu-link px-2 py-1"
              style={{ textDecoration: "none", color: "inherit" }}
              onClick={this.manageCategory}
            >
              Gift Cards
            </Link>
          </div>
        </div>

        <div
          className={
            this.state.isOffCanvasOpen
              ? `offcanvas offcanvas-start show`
              : `offcanvas offcanvas-start`
          }
          tabIndex="-1"
          id="offcanvasExample"
          aria-labelledby="offcanvasExampleLabel"
        >
          <div className="offcanvas-header blue-bg">
            <h5 className="offcanvas-title" id="offcanvasExampleLabel">
              <i className="fa fa-user-circle me-2"></i>
              Hello, {this.props.isUserLoggedIn && (<span>{this.props.userInfo.name}</span>)}
              {!this.props.isUserLoggedIn && (<span>User</span>)}
            </h5>
            <button
              type="button"
              className="btn-close"
              onClick={this.closeOffCanvas}
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body text-start">
            <div className="list-group">
              <h5>Trending</h5>
              <button className="list-group-item list-group-item-action no-border ps-0">
                Best Sellers
              </button>
              <button className="list-group-item list-group-item-action no-border ps-0">
                New Releases
              </button>
            </div>
            <hr />
            <div className="list-group">
              <h5>Shop By Category</h5>
              <Link
                to="/category/fresh"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Fresh
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/mobiles"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Mobiles, Smartphones
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/computers"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Computers
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/tvandelectronics"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  TV and Electronics
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/books"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Books
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/healthhouseholdandpersonalcare"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Health, Household and Personal Care
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/mensfashion"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  men's Fashion
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/womensfashion"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Women's Fashion
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/kidsfashion"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Kid's Fashion
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/videogames"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Video Games
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/homeandkitchenproducts"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Home & Kitchen Products
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
              <Link
                to="/category/giftcards"
                style={{ textDecoration: "none", color: "inherit" }}
              >
                <button
                  className="list-group-item list-group-item-action no-border ps-0"
                  onClick={this.closeOffCanvas}
                >
                  Gift Cards
                  <i className="fa fa-angle-right float-end"></i>
                </button>
              </Link>
            </div>
            <hr />
            <div className="list-group">
              <h5>Help & Settings</h5>
              <button
                className="list-group-item list-group-item-action no-border ps-0"
                onClick={this.closeOffCanvas}
              >
                Your Account
              </button>
              <button
                className="list-group-item list-group-item-action no-border ps-0"
                onClick={this.closeOffCanvas}
              >
                Customer Service
              </button>
              <button
                className="list-group-item list-group-item-action no-border ps-0"
                onClick={this.closeOffCanvas}
              >
                Sign In
              </button>
            </div>
          </div>
        </div>
        {/* OffCanvas Manu Ends */}
        <div
          className="offcanvas offcanvas-bottom"
          tabIndex="-1"
          id="offcanvasBottom"
          aria-labelledby="offcanvasBottomLabel"
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasBottomLabel">
              Your Shopping Cart
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body">
            <div
              className="cart-side-item mb-1 pb-2 text-center"
              style={{
                borderBottom: "2px solid #EDEDED",
                borderTop: "2px solid #EDEDED",
              }}
            >
              Subtotal ({total.count} Items):{" "}
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "1.2rem",
                  color: "#B12704",
                }}
              >
                <i className="fa fa-inr ms-2"></i> {total.price}
              </span>
              <div>
                <Link to="/cart" style={{ all: "inherit" }}>
                  <button
                    className="btn-nostyle go-to-cart col-9"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasBottom"
                    aria-controls="offcanvasBottom"
                  >
                    Go to Cart
                  </button>
                </Link>
              </div>
            </div>
            {this.props.cart.map((item, index) => {
              return (
                <div
                  className="cart-side-item mb-1 pb-2"
                  style={{ borderBottom: "2px solid #EDEDED" }}
                  key={`${item.category}_${item.id}_${index}`}
                >
                  <div className="row">
                    <div className="col-6" style={{ height: "9rem" }}>
                      <img
                        style={{ height: "100%" }}
                        src={item.imgUrl}
                        alt="cart item"
                      />
                    </div>
                    <div className="col-6">
                      <div
                        className="max-2-lines"
                        style={{ fontSize: "0.9rem", fontWeight: "bold" }}
                      >
                        {item.title}
                      </div>

                      <div className="my-1">
                        <i className="fa fa-inr"></i> {item.price}
                      </div>
                      <div className="row">
                        <div className="col-6">
                          <select
                            className="cart-count-btn text-center"
                            defaultValue={item.count}
                          >
                            <option value="0">0 (Delete)</option>
                            {new Array(9).fill(0).map((eachOption, index) => {
                              return (
                                <option
                                  key={`${item.category}_${item.id}_${eachOption}_${index}`}
                                  value={`${(index + 1).toString()}`}
                                >
                                  {index + 1}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                        <div className="col-3 ms-3">
                          <button
                            className="btn-nostyle cart-del-btn"
                            onClick={() => {
                              const updatedCartItems = this.props.cart.filter(
                                (cartItem) => {
                                  return (
                                    item.category !== cartItem.category &&
                                    item.id !== cartItem.id
                                  );
                                }
                              );
                              this.props.manageCart(updatedCartItems);
                            }}
                          >
                            <i className="fa fa-trash-o"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div
          className="offcanvas offcanvas-end"
          tabIndex="-1"
          id="offcanvasRight"
          aria-labelledby="offcanvasRightLabel"
        >
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasRightLabel">
              Your Shopping Cart
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body">
            <div
              className="cart-side-item mb-1 pb-2 text-center"
              style={{
                borderBottom: "2px solid #EDEDED",
                borderTop: "2px solid #EDEDED",
              }}
            >
              Subtotal ({total.count} Items):{" "}
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "1.2rem",
                  color: "#B12704",
                }}
              >
                <i className="fa fa-inr ms-2"></i> {total.price}
              </span>
              <div>
                <i className="fa fa-shopping-cart me-3"></i>
                <Link to="/cart" style={{ all: "inherit" }}>
                  <button
                    className="btn-nostyle go-to-cart"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasRight"
                    aria-controls="offcanvasRight"
                  >
                    Go to Cart
                  </button>
                </Link>
              </div>
            </div>
            {this.props.cart.map((item, index) => {
              return (
                <div
                  className="cart-side-item mb-1 pb-2"
                  style={{ borderBottom: "2px solid #EDEDED" }}
                  key={`${item.category}_${item.id}_${index}`}
                >
                  <div className="row">
                    <div className="col-6" style={{ height: "9rem" }}>
                      <img
                        style={{ height: "100%" }}
                        src={item.imgUrl}
                        alt="cart item"
                      />
                    </div>
                    <div className="col-6">
                      <div
                        className="max-2-lines"
                        style={{ fontSize: "0.9rem", fontWeight: "bold" }}
                      >
                        {item.title}
                      </div>

                      <div className="my-1">
                        <i className="fa fa-inr"></i> {item.price}
                      </div>
                      <div className="row">
                        <div className="col-6">
                          <div
                            class="btn-group"
                            role="group"
                            aria-label="Basic example"
                          >
                            <button
                              type="button"
                              class="btn cart-count-btn py-1"
                              style={{ width: "auto" }}
                              onClick={() => {
                                const updatedCount = item.count - 1;
                                const presentItemIndex=this.props.cart.findIndex((cartItem)=>{
                                  return cartItem.category===item.category && cartItem.id===item.id;
                                });
                                const {
                                  id,
                                  title,
                                  price,
                                  imgUrl,
                                  category,
                                  deliveryDate,
                                } = this.props.cart[presentItemIndex];

                                const presentItem = {
                                  id,
                                  title,
                                  price,
                                  imgUrl,
                                  category,
                                  deliveryDate,
                                  count: updatedCount,
                                };

                                const updatedCart =
                                  updatedCount === 0
                                    ? [
                                        ...this.props.cart.slice(0, presentItemIndex - 1),
                                        ...this.props.cart.slice(
                                          index + 1,
                                          this.props.cart.length
                                        ),
                                      ]
                                    : [
                                        ...this.props.cart.slice(0, presentItemIndex - 1),
                                        presentItem,
                                        ...this.props.cart.slice(
                                          index + 1,
                                          this.props.cart.length
                                        ),
                                      ];
                                this.props.manageCart(updatedCart);
                              }}
                            >
                              -
                            </button>
                            <div className="p-2">{item.count}</div>
                            <button
                              type="button"
                              class="btn cart-count-btn py-1"
                              style={{ width: "auto" }}
                              onClick={() => {
                                const updatedCount = item.count + 1;
                                const presentItemIndex=this.props.cart.findIndex((cartItem)=>{
                                  return cartItem.category===item.category && cartItem.id===item.id;
                                });
                                const {
                                  id,
                                  title,
                                  price,
                                  imgUrl,
                                  category,
                                  deliveryDate,
                                } = this.props.cart[presentItemIndex];

                                const presentItem = {
                                  id,
                                  title,
                                  price,
                                  imgUrl,
                                  category,
                                  deliveryDate,
                                  count: updatedCount,
                                };

                                const updatedCart = [
                                  ...this.props.cart.slice(0, presentItemIndex - 1),
                                  presentItem,
                                  ...this.props.cart.slice(
                                    index + 1,
                                    this.props.cart.length
                                  ),
                                ];
                                this.props.manageCart(updatedCart);
                              }}
                            >
                              +
                            </button>
                          </div>
                        </div>
                        <div className="col-3 ms-3">
                          <button
                            className="btn-nostyle cart-del-btn p-1"
                            onClick={() => {
                              const updatedCartItems = this.props.cart.filter(
                                (cartItem) => {
                                  return (
                                    item.category !== cartItem.category &&
                                    item.id !== cartItem.id
                                  );
                                }
                              );
                              this.props.manageCart(updatedCartItems);
                            }}
                          >
                            <i className="fa fa-trash-o"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
