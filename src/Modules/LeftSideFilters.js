import React from "react";

class LeftSideFilters extends React.Component {
  handleCategoryFilter = (event) => {
    this.props.filterCategory(event.target.value);
  };
  filterPrice = (event) => {
    let min, max;
    if (event.target.value === "1000") {
      min = 0;
      max = 1000;
    }
    if (event.target.value === "1000-5000") {
      min = 1000;
      max = 5000;
    }
    if (event.target.value === "5000-10000") {
      min = 5000;
      max = 10000;
    }
    if (event.target.value === "10000-20000") {
      min = 10000;
      max = 20000;
    }
    if (event.target.value === "20000") {
      min = 20000;
      max = Infinity;
    }
    const priceFilters = {
      min: min,
      max: max,
    };

    this.props.managePriceFilters(priceFilters);
  };
  filterDiscount = (event) => {
    this.props.manageDiscountFilters(event.target.value);
  };

  filterReviews = (event) => {
    this.props.manageReviewFilters(event.target.value);
  };
  render() {
    return (
      <div style={{ borderRight: "2px solid #D3D3D3" }}>
        <p
          className="text-capitalize font-weight-bold"
          style={{ fontWeight: "bold" }}
        >
          category
        </p>
        <ul style={{ listStyle: "none", paddingLeft: "0px" }}>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="all-category"
              value="allCategory"
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="all-category">
              All Category
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="fresh-category"
              value="fresh"
              checked={this.props.category === "fresh"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="fresh-category">
              Fresh
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="mobiles-category"
              value="mobiles"
              checked={this.props.category === "mobiles"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="mobiles-category">
              Mobiles
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="computers-category"
              value="computers"
              checked={this.props.category === "computers"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="computers-category">
              Computers
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="tv-category"
              value="tvandelectronics"
              checked={this.props.category === "tvandelectronics"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="tv-category">
              Tv & Electronics
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="books-category"
              value="books"
              checked={this.props.category === "books"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="books-category">
              Books
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="household-category"
              value="healthhouseholdandpersonalcare"
              checked={this.props.category === "healthhouseholdandpersonalcare"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="household-category">
              Health & Household
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="mensfashion-category"
              value="mensfashion"
              checked={this.props.category === "mensfashion"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="mensfashion-category">
              Men's Fashion
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="womensfashion-category"
              value="womensfashion"
              checked={this.props.category === "womensfashion"}
              onChange={this.handleCategoryFilter}
            />
            <label
              className="form-check-label"
              htmlFor="womensfashion-category"
            >
              Women's Fashion
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="kidsfashion-category"
              value="kidsfashion"
              checked={this.props.category === "kidsfashion"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="kidsfashion-category">
              Kid's Fashion
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="videogames-category"
              value="videogames"
              checked={this.props.category === "videogames"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="videogames-category">
              Video Games
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="homeandkitchenproducts-category"
              value="homeandkitchenproducts"
              checked={this.props.category === "homeandkitchenproducts"}
              onChange={this.handleCategoryFilter}
            />
            <label
              className="form-check-label"
              htmlFor="homeandkitchenproducts-category"
            >
              Home & Kitchen
            </label>
          </div>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="filter-category"
              id="giftcards-category"
              value="giftcards"
              checked={this.props.category === "giftcards"}
              onChange={this.handleCategoryFilter}
            />
            <label className="form-check-label" htmlFor="giftcards-category">
              Gift Cards
            </label>
          </div>
        </ul>
        <div style={{ listStyle: "none" }}>
          <p
            className="text-capitalize font-weight-bold"
            style={{ fontWeight: "bold" }}
          >
            Price
          </p>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="all-price"
                value="all"
                checked={this.props.priceFilters.min === undefined}
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="all-price">
                all
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="1000-price"
                value="1000"
                checked={
                  this.props.priceFilters.min === 0 &&
                  this.props.priceFilters.max === 1000
                }
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="1000-price">
                Under ₹1,000
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="1000-5000-price"
                value="1000-5000"
                checked={
                  this.props.priceFilters.min === 1000 &&
                  this.props.priceFilters.max === 5000
                }
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="1000-5000-price">
                ₹1,000 - ₹5,000
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="5000-10000-price"
                value="5000-10000"
                checked={
                  this.props.priceFilters.min === 5000 &&
                  this.props.priceFilters.max === 10000
                }
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="5000-10000-price">
                ₹5,000 - ₹10,000
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="10000-20000-price"
                value="10000-20000"
                checked={
                  this.props.priceFilters.min === 10000 &&
                  this.props.priceFilters.max === 20000
                }
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="10000-20000-price">
                ₹10,000 - ₹20,000
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="price-category"
                id="20000-price"
                value="20000"
                checked={this.props.priceFilters.min === 20000}
                onChange={this.filterPrice}
              />
              <label className="form-check-label" htmlFor="20000-price">
                Over ₹20,000
              </label>
            </div>
          </li>
        </div>

        <div style={{ listStyle: "none", marginTop: "20px" }}>
          <p
            className="text-capitalize font-weight-bold"
            style={{ fontWeight: "bold" }}
          >
            Discount
          </p>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="any-discount"
                value={undefined}
                defaultChecked
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="any-discount">
                all
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="10-discount"
                value="10"
                checked={this.props.discountFilters === "10"}
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="10-discount">
                10% off or more
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="25-discount"
                value="25"
                checked={this.props.discountFilters === "25"}
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="25-discount">
                25% off or more
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="35-discount"
                value="35"
                checked={this.props.discountFilters === "35"}
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="35-discount">
                35% off or more
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="50-discount"
                value="50"
                checked={this.props.discountFilters === "50"}
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="50-discount">
                50% off or more
              </label>
            </div>
          </li>
          <li>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="discount-category"
                id="70-discount"
                value="70"
                checked={this.props.discountFilters === "70"}
                onChange={this.filterDiscount}
              />
              <label className="form-check-label" htmlFor="70-discount">
                70% off or more
              </label>
            </div>
          </li>
        </div>
        <p
          className="text-capitalize font-weight-bold"
          style={{ marginTop: "20px", fontWeight: "bold" }}
        >
          Customer reviews
        </p>
        <button className="btn btn-sm bg-transparent">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="review-category"
              id="any-review"
              value={undefined}
              // checked={this.props.reviewFilters === undefined}
              onChange={this.filterReviews}
              defaultChecked
            />
            <label className="form-check-label" htmlFor="any-review">
              <span className="rating">all</span>
            </label>
          </div>
        </button>
        <br />
        <button className="btn btn-sm bg-transparent">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="review-category"
              id="4-review"
              value="4"
              checked={this.props.reviewFilters === "4"}
              onChange={this.filterReviews}
            />
            <label className="form-check-label" htmlFor="4-review">
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <span className="rating">& Up</span>
            </label>
          </div>
        </button>
        <br />
        <button className="btn btn-sm bg-transparent">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="review-category"
              id="3-review"
              value="3"
              checked={this.props.reviewFilters === "3"}
              onChange={this.filterReviews}
            />
            <label className="form-check-label" htmlFor="3-review">
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <span className="rating">& Up</span>
            </label>
          </div>
        </button>
        <br />
        <button className="btn btn-sm bg-transparent">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="review-category"
              id="2-review"
              value="2"
              checked={this.props.reviewFilters === "2"}
              onChange={this.filterReviews}
            />
            <label className="form-check-label" htmlFor="2-review">
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <span className="rating">& Up</span>
            </label>
          </div>
        </button>
        <br />
        <button className="btn btn-sm bg-transparent">
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="review-category"
              id="1-review"
              value="1"
              checked={this.props.reviewFilters === "1"}
              onChange={this.filterReviews}
            />
            <label className="form-check-label" htmlFor="1-review">
              <i className="fas fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <i className="far fa-star" style={{ color: "#ffa41c" }}></i>
              <span className="rating">& Up</span>
            </label>
          </div>
        </button>
        <br />
      </div>
    );
  }
}

export default LeftSideFilters;
