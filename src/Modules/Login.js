import React from "react";
import { Link } from "react-router-dom";
import validator from "validator";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isPhoneOrEmailEntered: false,
      isPasswordEntered: false,
      isPhoneOrEmailValid: false,
      phoneOrEmail: "",
      password: "",
      wrongCredentials: false,
      isSignedIn: false,
    };
  }

  handleUserId = (event) => {
    const enteredValue = event.target.value;
    const phoneOrEmailValidation =
      validator.isEmail(enteredValue) ||
      (validator.isNumeric(enteredValue) &&
        validator.isLength(enteredValue, { min: 10, max: 10 }));

    this.setState({
      isPhoneOrEmailEntered: true,
      phoneOrEmail: enteredValue,
      isPhoneOrEmailValid: phoneOrEmailValidation,
    });
  };

  handlePassword = (event) => {
    const enteredValue = event.target.value;
    this.setState({
      isPasswordEntered: true,
      password: enteredValue,
    });
  };

  setUser = (event) => {
    event.preventDefault();
    const isIdEmailOrPhone = validator.isEmail(this.state.phoneOrEmail)
      ? "email"
      : "phone";

    const userInUsersIndex = this.props.users.findIndex((user) => {
      return isIdEmailOrPhone === "email"
        ? user.email === this.state.phoneOrEmail
        : user.phone === this.state.phoneOrEmail;
    });

    const isCredentialCorrect =
      userInUsersIndex >= 0
        ? this.props.users[userInUsersIndex].password === this.state.password
          ? true
          : false
        : false;

    if (!isCredentialCorrect) {
      this.setState({
        wrongCredentials: true,
        isSignedIn: false,
      });
    } else {
      const userInfo = this.props.users[userInUsersIndex];
      this.props.setUser(userInfo);
      this.setState({
        wrongCredentials: false,
        isSignedIn: true,
      });
    }

  };

  render() {
    return (
      <>
        <div
          className="container center"
          style={{ width: "25rem", height: "80vh" }}
        >
          <div className="logo-center my-3">
            <img
              src="amazon_logo_black.png"
              height="30"
              width="100"
              alt="amazon logo white"
            />
          </div>
          {this.state.isSignedIn && (
            <div className="card" style={{ height: "auto" }}>
              <div className="card-body">
                <h4 className="card-title">Signed In</h4>

                <Link to="/">
                  <button className="btn btn-dark btn-sm w-100 mt-2">
                    Go to home page
                  </button>
                </Link>
              </div>
            </div>
          )}

          {!this.state.isSignedIn && (
            <div className="card" style={{ height: "auto" }}>
              <div className="card-body">
                <h4 className="card-title">Sign In</h4>
                <form
                  className="needs-validation"
                  autoComplete="off"
                  noValidate
                  onSubmit={this.setUser}
                >
                  {this.state.wrongCredentials && (
                    <h6 style={{ color: "red" }}>Wrong Credentials</h6>
                  )}

                  <div className="form-group my-2">
                    <label htmlFor="userPhoneOrEmail">
                      Phone No or Email:{" "}
                    </label>
                    <input
                      type="text"
                      value={this.state.phoneOrEmail}
                      name="userPhoneOrEmail"
                      className="form-control"
                      id="userPhoneOrEmail"
                      aria-describedby="userPhoneOrEmail"
                      onChange={this.handleUserId}
                    />
                    {!this.state.isPhoneOrEmailEntered && (
                      <small id="emailHelp" className="form-text text-muted">
                        <i className="fas text-primary fa-info"></i> Enter
                        registered Phone or Email
                      </small>
                    )}

                    {this.state.isPhoneOrEmailValid && (
                      <div
                        className="valid-feedback"
                        style={{ display: "block" }}
                      >
                        <i className="far text-success fa-thumbs-up"></i> OK
                      </div>
                    )}
                    {this.state.isPhoneOrEmailEntered &&
                      !this.state.isPhoneOrEmailValid && (
                        <div
                          className="invalid-feedback"
                          style={{ display: "block" }}
                        >
                          <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                          Enter a valid Phone No or Email
                        </div>
                      )}
                  </div>

                  <div className="form-group my-2">
                    <label htmlFor="userPassword">Password: </label>
                    <input
                      type="password"
                      className="form-control"
                      placeholder="******"
                      name="password"
                      id="userPassword"
                      aria-describedby="passwordHelp"
                      onChange={this.handlePassword}
                    />
                    <div className="valid-feedback">
                      <i className="far text-success fa-thumbs-up"></i> OK
                    </div>
                    <div className="invalid-feedback">
                      <i className="fas text-danger fa-exclamation-triangle"></i>{" "}
                      Some error in password.
                    </div>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-warning form-control"
                    disabled={
                      !this.state.isPhoneOrEmailValid ||
                      !this.state.isPasswordEntered
                    }
                  >
                    Sign In
                  </button>
                </form>

                <hr className="hr-text" data-bs-content="New to Amazon?" />
                <Link to="/signup">
                  <button className="btn btn-dark btn-sm w-100 mt-2">
                    New to Amazon? Sign Up
                  </button>
                </Link>
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default Login;
