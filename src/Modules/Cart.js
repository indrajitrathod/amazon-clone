import React from "react";
import { Link } from "react-router-dom";

class Cart extends React.Component {
  render() {
    const total = this.props.cart.reduce(
      (totalObject, item) => {
        totalObject.count += Number(item.count);
        totalObject.price += Number(item.price) * Number(item.count);
        return totalObject;
      },
      { count: 0, price: 0 }
    );
    return (
      <>
        <main>
          <div className="container my-5">
            <div className="row">
              <div className="col-md-8">
                <h3>Shopping Cart</h3>
                <p className="text-muted float-end mt-n3">Price</p>
              </div>
            </div>

            <div className="row">
              <div className="col-md-8">
                {this.props.cart.map((item) => {
                  return (
                    <div className="row border-top py-3">
                      <div
                        className="col-3 text-center"
                        key={`${item.catogory}_${item.id}`}
                      >
                        <img
                          src={item.imgUrl}
                          className="img-fluid"
                          style={{ height: "180px" }}
                          alt="product 01"
                        />
                      </div>
                      <div className="col-md-9">
                        <button
                          className="btn-nostyle"
                          style={{ maxWidth: "36ch", fontWeight: "bold" }}
                        >
                          {item.title}
                        </button>
                        <p className="text-uppercase font-weight-bold my-0 float-end">
                          <i className="fa fa-inr mx-1"></i>
                          <span>{item.price * item.count}</span>
                        </p>
                        <p className="text-muted my-2">
                          Delivery By :{" "}
                          <span style={{ color: "#000" }}>
                            {item.deliveryDate}
                          </span>
                        </p>
                        <div className="row mt-3">
                          <div className="col-1"></div>
                          <div className="col-1 ms-3">
                            <button
                              className="btn-nostyle cart-del-btn"
                              onClick={() => {
                                const updatedCartItems = this.props.cart.filter(
                                  (cartItem) => {
                                    return (
                                      item.category !== cartItem.category &&
                                      item.id !== cartItem.id
                                    );
                                  }
                                );
                                this.props.manageCart(updatedCartItems);
                              }}
                            >
                              <i className="fa fa-trash-o"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}

                <div className="row border-top py-3">
                  <div className="col-md-12">
                    <p className="float-end font-weight-bold">
                      Subtotal ({total.count} items):
                      <i className="fa fa-inr mx-1"></i> {total.price}
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-md-4 text-center pl-md-5 pl-0">
                <img
                  src="img/shopping_cart_image.svg"
                  className="img-fluid mt-md-n5 mt-0"
                  alt="amazon"
                />

                <div className="cart-box my-3">
                  <div className="card-header bg-transparent">
                    <p className="text-center text-success my-0">
                      <i className="fas fa-check"></i>
                      Your order is eligible for FREE Delivery.
                    </p>
                  </div>
                  <div className="card-body text-center">
                    <p className="font-weight-bold">
                      Subtotal ({total.count} items):
                      <i className="fa fa-inr mx-1"></i> {total.price}
                    </p>
                    <Link to="/checkout" style={{ all: "inherit" }}>
                      <button className="btn-nostyle add-to-cart-button px-3">
                        Proceed to Checkout
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </>
    );
  }
}

export default Cart;
