import { Link } from "react-router-dom";

const MainResult = () => {
  return (
    <>
      <div className="container px-0 d-none d-lg-block">
        <div
          id="amazonImageSlider"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="slider/slide01.png"
                className="d-block w-100"
                height="500"
                alt="01"
              />
            </div>
            <div className="carousel-item">
              <img
                src="slider/slide02.png"
                className="d-block w-100"
                height="500"
                alt="02"
              />
            </div>
            <div className="carousel-item">
              <img
                src="slider/slide03.png"
                className="d-block w-100"
                height="500"
                alt="03"
              />
            </div>
            <div className="carousel-item ">
              <img
                src="slider/slide04.png"
                className="d-block w-100"
                height="500"
                alt="04"
              />
            </div>
            <div className="carousel-item">
              <img
                src="slider/slide05.png"
                className="d-block w-100"
                height="500"
                alt="05"
              />
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#amazonImageSlider"
            role="button"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#amazonImageSlider"
            role="button"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Next</span>
          </a>
        </div>

        <div className="container check my-3">
          <div
            id="carouselExampleControls"
            className="carousel slide"
            data-bs-interval="false"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="cards-wrapper">
                  <Link to="/category/mobiles">
                    <div className="card">
                      <img
                        src="https://m.media-amazon.com/images/G/31/img22/Wireless/simonti/ShopByCategory/New/BAU/D40385862_IN_WL_Category_Page_Revamp_1Sprite._CB605984115_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Smart Phones</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/computers">
                    <div className="card d-none d-md-block">
                      <img
                        src="https://m.media-amazon.com/images/G/31/img21/CEPC/Electronics/Revamp/SBC/xcm_banners_01_sbc_v1_564x564_in-en._CB657839327_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Computers</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/books">
                    <div className="card card-home d-none d-md-block">
                      <img
                        src="img/books.png"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Books</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/homeandkitchenproducts">
                    <div className="card d-none d-md-block">
                      <img
                        src="https://m.media-amazon.com/images/I/419N2-QRjkL._AC_UL320_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Home & Kitchen</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/tvandelectronics">
                    <div className="card d-none d-md-block">
                      <img
                        src="img/tv.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Tv & Appliances</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/mensfashion">
                  <div className="card d-none d-md-block">
                    <img
                      src="https://m.media-amazon.com/images/G/31/Symbol/2022/WinterFlip/MenClothing/1._SY530_QL85_.jpg"
                      className="card-img-top"
                      alt="..."
                    />
                    <div className="card-body">
                      <p className="card-text">Fashion</p>
                    </div>
                  </div>
                  </Link>
                </div>
              </div>
              <div className="carousel-item">
              <div className="cards-wrapper">
                  <Link to="/category/mobiles">
                    <div className="card">
                      <img
                        src="https://m.media-amazon.com/images/G/31/img22/Wireless/simonti/ShopByCategory/New/BAU/D40385862_IN_WL_Category_Page_Revamp_1Sprite._CB605984115_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Smart Phones</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/computers">
                    <div className="card d-none d-md-block">
                      <img
                        src="https://m.media-amazon.com/images/G/31/img21/CEPC/Electronics/Revamp/SBC/xcm_banners_01_sbc_v1_564x564_in-en._CB657839327_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Computers</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/books">
                    <div className="card card-home d-none d-md-block">
                      <img
                        src="img/books.png"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Books</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/homeandkitchenproducts">
                    <div className="card d-none d-md-block">
                      <img
                        src="https://m.media-amazon.com/images/I/419N2-QRjkL._AC_UL320_.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Home & Kitchen</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/tvandelectronics">
                    <div className="card d-none d-md-block">
                      <img
                        src="img/tv.jpg"
                        className="card-img-top"
                        alt="..."
                      />
                      <div className="card-body">
                        <p className="card-text">Tv & Appliances</p>
                      </div>
                    </div>
                  </Link>
                  <Link to="/category/mensfashion">
                  <div className="card d-none d-md-block">
                    <img
                      src="https://m.media-amazon.com/images/G/31/Symbol/2022/WinterFlip/MenClothing/1._SY530_QL85_.jpg"
                      className="card-img-top"
                      alt="..."
                    />
                    <div className="card-body">
                      <p className="card-text">Fashion</p>
                    </div>
                  </div>
                  </Link>
                </div>
              </div>
            </div>
            <button
              className="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <span
                className="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Previous</span>
            </button>
            <button
              className="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <span
                className="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span className="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainResult;
