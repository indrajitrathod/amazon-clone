import React from "react";
import validator from "validator";

class AddressModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      mobileNumber: "",
      address: "",
      pincode: "",
      landMark: "",
      city: "",
      state: "",
      nameError: "",
      mobileNumberError: "",
      pincodeError: "",
      addressError: "",
      cityError: "",
    };
  }

  validateName = (value) => {
    if (value.trim().length === 0) {
      this.setState({ nameError: true });
    } else {
      this.setState({ nameError: false, name: value });
    }
  };

  validateMobile = (value) => {
    if (!validator.isLength(value, {min: 10, max:10}) || !validator.isNumeric(value)) {
      this.setState({ mobileNumberError: true });
    } else {
      this.setState({ mobileNumberError: false, mobileNumber: value });
    }
  };

  validatePincode = (value) => {
    const pinRegex = RegExp("^[1-9][0-9]{5}$");
    if (!pinRegex.test(value)) {
      this.setState({ pincodeError: true });
    } else {
      this.setState({ pincodeError: false, pincode: value });
    }
  };

  validateAddress = (value) => {
    if (value.trim().length === 0) {
      this.setState({ addressError: true });
    } else {
      this.setState({ addressError: false, address: value });
    }
  };

  validateCity = (value) => {
    if (value.trim().length === 0) {
      this.setState({ cityError: true });
    } else {
      this.setState({ cityError: false, city: value });
    }
  };

  setValue = (value, property) => {
    this.setState({ [property]: value });
  };

  saveDetails = () => {
    console.log(this.state);
    this.props.saveAddress(this.state);
  };
  render() {
    const validity = [
      this.state.nameError,
      this.state.mobileNumberError,
      this.state.pincodeError,
      this.state.cityError,
      this.state.addressError,
    ];
    return (
      <div
        className="modal fade"
        id="AddressModal"
        tabIndex="-1"
        aria-labelledby="AddressModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={{ color: "black", maxWidth: "500px" }}
        >
          <div className="modal-content">
            <div className="modal-header">
              <legend className="modal-title fs-5 lead" id="AddressModalLabel">
                Add a new Address
              </legend>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body d-flex flex-column align-items-center">
              <form
                className="d-flex flex-column align-items-center"
                style={{ color: "black" }}
              >
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="fullname"
                    className="col-sm-12 col-form-label lead"
                  >
                    Your Name*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control shadow-none"
                      placeholder="Name"
                      id="fullname"
                      onChange={(event) => {
                        this.validateName(event.target.value);
                      }}
                    />
                    {this.state.nameError ? (
                      <div style={{ color: "red", fontSize: "15px" }}>
                        Name can not be empty
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="mobile-number"
                    className="col-sm-12 col-form-label lead"
                  >
                    Mobile Number*
                  </label>
                  <div className="col">
                    <input
                      inputMode="numeric"
                      className="form-control shadow-none "
                      placeholder="Mobile Number"
                      id="mobile-number"
                      onChange={(event) => {
                        this.validateMobile(event.target.value);
                      }}
                    />
                    {this.state.mobileNumberError ? (
                      <div style={{ color: "red", fontSize: "15px" }}>
                        Please enter valid Mobile number
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="pincode"
                    className="col-sm-12 col-form-label lead"
                  >
                    Pincode*
                  </label>
                  <div className="col  form-div">
                    <input
                      className="form-control shadow-none"
                      placeholder="Pincode"
                      id="pincode"
                      onChange={(event) => {
                        this.validatePincode(event.target.value);
                      }}
                    />
                    {this.state.pincodeError ? (
                      <div style={{ color: "red", fontSize: "15px" }}>
                        Please enter valid pincode
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="address-1"
                    className="col-sm-12 col-form-label lead"
                  >
                    Complete address*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control shadow-none"
                      id="address-1"
                      placeholder="Complete address"
                      onChange={(event) => {
                        this.validateAddress(event.target.value);
                      }}
                    />
                    {this.state.addressError ? (
                      <div style={{ color: "red", fontSize: "15px" }}>
                        Address cannot be empty
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="landmark"
                    className="col-sm-12 col-form-label lead"
                  >
                    Landmark
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control shadow-none"
                      id="landmark"
                      placeholder="Landmark"
                      onChange={(event) => {
                        this.setValue(event.target.value, "landMark");
                      }}
                    />
                  </div>
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="city"
                    className="col-sm-12 col-form-label lead"
                  >
                    City*
                  </label>
                  <div className="col">
                    <input
                      type="text"
                      className="form-control shadow-none"
                      placeholder="City"
                      id="city"
                      onChange={(event) => {
                        this.validateCity(event.target.value);
                      }}
                    />
                  </div>
                  {this.state.cityError ? (
                    <div style={{ color: "red", fontSize: "15px" }}>
                      City cannot be empty
                    </div>
                  ) : null}
                </div>
                <div className="row mb-3 w-100">
                  <label
                    htmlFor="city"
                    className="col-sm-12 col-form-label lead"
                  >
                    State*
                  </label>
                  <div className="col">
                    <select
                      name="state"
                      id="state"
                      className="form-control"
                      value={this.state.state}
                      onChange={(event) => {
                        this.setValue(event.target.value, "state");
                      }}
                    >
                      <option disabled={true} value="">
                        Select a state
                      </option>
                      <option value="Andhra Pradesh">Andhra Pradesh</option>
                      <option value="Andaman and Nicobar Islands">
                        Andaman and Nicobar Islands
                      </option>
                      <option value="Arunachal Pradesh">
                        Arunachal Pradesh
                      </option>
                      <option value="Assam">Assam</option>
                      <option value="Bihar">Bihar</option>
                      <option value="Chandigarh">Chandigarh</option>
                      <option value="Chhattisgarh">Chhattisgarh</option>
                      <option value="Dadar and Nagar Haveli">
                        Dadar and Nagar Haveli
                      </option>
                      <option value="Daman and Diu">Daman and Diu</option>
                      <option value="Delhi">Delhi</option>
                      <option value="Lakshadweep">Lakshadweep</option>
                      <option value="Puducherry">Puducherry</option>
                      <option value="Goa">Goa</option>
                      <option value="Gujarat">Gujarat</option>
                      <option value="Haryana">Haryana</option>
                      <option value="Himachal Pradesh">Himachal Pradesh</option>
                      <option value="Jammu and Kashmir">
                        Jammu and Kashmir
                      </option>
                      <option value="Jharkhand">Jharkhand</option>
                      <option value="Karnataka">Karnataka</option>
                      <option value="Kerala">Kerala</option>
                      <option value="Madhya Pradesh">Madhya Pradesh</option>
                      <option value="Maharashtra">Maharashtra</option>
                      <option value="Manipur">Manipur</option>
                      <option value="Meghalaya">Meghalaya</option>
                      <option value="Mizoram">Mizoram</option>
                      <option value="Nagaland">Nagaland</option>
                      <option value="Odisha">Odisha</option>
                      <option value="Punjab">Punjab</option>
                      <option value="Rajasthan">Rajasthan</option>
                      <option value="Sikkim">Sikkim</option>
                      <option value="Tamil Nadu">Tamil Nadu</option>
                      <option value="Telangana">Telangana</option>
                      <option value="Tripura">Tripura</option>
                      <option value="Uttar Pradesh">Uttar Pradesh</option>
                      <option value="Uttarakhand">Uttarakhand</option>
                      <option value="West Bengal">West Bengal</option>
                    </select>
                  </div>
                </div>
              </form>
              <div className="d-grid w-100 mt-3">
                <button
                  className="btn btn-warning"
                  type="button"
                  disabled={
                    validity.includes(true) || validity.includes("")
                      ? true
                      : false
                  }
                  style={{
                    alignSelf: "center",
                  }}
                  data-bs-dismiss="modal"
                  onClick={(event) => {
                    this.saveDetails(event);
                  }}
                >
                  Save Address
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AddressModal;
