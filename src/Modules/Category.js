import React from "react";
import { categoryDetails } from "../Data/categoriesList";
import { withRouter } from "react-router-dom";
import ProductList from "./ProductList";

class Category extends React.Component {
  render() {
    const category = this.props.category
      ? this.props.category
      : this.props.match.params.category;
    const products = categoryDetails[category];
    return (
      <>
        <ProductList
          products={products}
          category={category}
          filterCategory={this.props.filterCategory}
          priceFilters={this.props.priceFilters}
          managePriceFilters={this.props.managePriceFilters}
          discountFilters={this.props.discountFilters}
          manageDiscountFilters={this.props.manageDiscountFilters}
          reviewFilters={this.props.reviewFilters}
          manageReviewFilters={this.props.manageReviewFilters}
        />
      </>
    );
  }
}

export default withRouter(Category);
